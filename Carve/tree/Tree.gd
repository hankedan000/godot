extends Node2D

signal score

var speed = 0
var origPointOffset = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	origPointOffset = $pointLabel.rect_position - position
	
	# connect all the scorebox callbacks
	for child in get_children():
		if child.is_in_group("scorebox"):
			child.connect("scored",self,"_scored")

func set_speed(s):
	speed = s
	
func _reset():
	# reset the point label
	$pointLabel.rect_position = position + origPointOffset
	$pointLabel.visible = false
	
	# turn on scorebox collision again
	for child in get_children():
		if child.is_in_group("scorebox"):
			child.enableCollision(true)
		

func spawn():
	_reset()
	var w = get_viewport_rect().size.x
	var h = get_viewport_rect().size.y
	
	position.x = GameGlobals.random.randi_range(0,w)
	position.y = GameGlobals.random.randi_range(0,h) + h / 2

func respawn():
	_reset()
	var w = get_viewport_rect().size.x
	var h = get_viewport_rect().size.y
	
	position.x = GameGlobals.random.randi_range(0,w)
	position.y = h + tree_height()

func tree_height():
	return ($Sprite).get_rect().size.y
	
func _scored(pointReward: int, scoreColor: Color):
	# once a scorebox is collected, turn off all the rest
	for child in get_children():
		if child.is_in_group("scorebox"):
			child.enableCollision(false)
			
	$pointLabel.set("custom_colors/font_color",scoreColor)
	$pointLabel.text = str(pointReward)
	$pointLabel.visible = true
	
	emit_signal("score",pointReward)
	
	# begin animation that slides score label up
	var TWEEN_DURATION = 1.0
	$tween.interpolate_method(self,"_on_score_slide_up",Vector2(),Vector2(0,-50),TWEEN_DURATION,Tween.TRANS_LINEAR,Tween.EASE_OUT)
	$tween.interpolate_method(self,"_on_score_alpha_fade",1.0,0.0,TWEEN_DURATION/2.0,Tween.TRANS_LINEAR,Tween.EASE_OUT,TWEEN_DURATION/2.0)
	$tween.start()
	
# ----------------------------------------------------------

# Tween callbacks used to animate the point label

# slides the score label up slightly after being collected 
func _on_score_slide_up(offset: Vector2):
	$pointLabel.rect_position = origPointOffset + offset
	
# makes the score slightly fade away after being collected
func _on_score_alpha_fade(alpha: float):
	var newColor = Color(1,1,1,alpha)
	$pointLabel.modulate = newColor
	
# ----------------------------------------------------------

func _process(delta):
	position += Vector2(0,speed * -1)
	
	if position.y < 0:
		respawn()
