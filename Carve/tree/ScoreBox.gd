extends StaticBody2D
class_name ScoreBox

export var pointReward = 10
export var scoreColor = Color()

var origCollisionLayer = 0

signal scored(pointReward,scoreColor)

func _ready():
	origCollisionLayer = collision_layer
	add_to_group("scorebox")
	
func enableCollision(enabled):
	if enabled:
		collision_layer = origCollisionLayer
	else:
		collision_layer = 0

func hit():
	emit_signal("scored",pointReward,scoreColor)
