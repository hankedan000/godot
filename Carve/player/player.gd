extends KinematicBody2D

# signal that's emitted when player collides with something
signal collision_event

var a = 0.0
var v = 0.0
var initY = 0
var startCarveV = 0.0
var inShred = false
var distance = 0
var speed = 0
var points = 0
var playerID = 1
var shredParticleNode = null
var origCollisionMask = 0

var MAX_TAIL = 100
var SIZE_SCALE = 7.0/300.0
var TAIL_SCALE = 2.0/300.0
var ACC_SCALE = 0.03/300.0
var MAX_VEL_SCALE = 1.0/300.0

var ShredParticle = preload("res://player/ShredParticle.tscn")
var player1_tex = preload('res://player/player1.png')
var player2_tex = preload('res://player/player2.png')

# Called when the node enters the scene tree for the first time.
func _ready():
	initY = global_position.y
	shredParticleNode = get_parent()
	origCollisionMask = collision_mask
	respawn()
	
# allows the parent to specify which Node the shred particles
# are spawned into. by default, player will spawn particles in
# the parent Node.
func setShredNode(node):
	shredParticleNode = node

func respawn():
#	tail.clear();
	var w = get_viewport_rect().size.x
	var h = get_viewport_rect().size.y
	position.x = w / 2
	position.y = initY
	a = 0.0
	v = 0.0
	startCarveV = 0
	inShred = false
	distance = 0
	points = 0
	collision_mask = origCollisionMask
		
func carve():
	($carve_short).play(0.0)
		
	if a < 0.001 and a > -0.001:
		# if standing still, kick off in the right direction
		var w = get_viewport_rect().size.x
		a = w * ACC_SCALE
	else:
		startCarveV = v
		inShred = true
		a *= -1
		
func set_player(player_num):
	playerID = player_num
	match(player_num):
		1:
			($Sprite).set_texture(player1_tex)
		2:
			($Sprite).set_texture(player2_tex)
		
func set_speed(s):
	if s < 0.001 and s > -0.001:
		a = 0.0
		v = 0.0
		
	speed = s
		
func get_size():
	return ($Sprite).get_rect().size.x
	
func add_points(p):
	points += p
	
func get_score():
	return points + distance
		
func is_moving():
	return inShred

func _max_velocity():
	var w = get_viewport_rect().size.x
	return w * MAX_VEL_SCALE

func _physics_process(delta):
	var maxVel = _max_velocity()
	v += a
	if abs(v) > maxVel:
		v = (abs(v)/v) * maxVel
	var collision_info = move_and_collide(Vector2(v,0))
	
	# reset y-position
	# move_and_collide was causing slow drift over time
	global_position.y = initY
	if collision_info:
		var collider = collision_info.collider
		if collider.is_in_group("scorebox"):
			# trigger score on the tree
			collider.hit()
		else:
			# turn off all futher collisions
			collision_mask = 0
			emit_signal("collision_event")
	
	if inShred:
		var new_shred = ShredParticle.instance()
		new_shred.init(position,speed)
		shredParticleNode.add_child(new_shred)
		if sign(startCarveV) != sign(v):
			inShred = false;

	if abs(v) > 0:
		distance += speed
#		pushTail();
#		for tail_point in tail:
#			tail_point.position.y -= speed;
