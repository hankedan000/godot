extends Object
class_name HighscoreTable

export var max_highscores = 10

signal new_score_added

var _highscores = []

func _ready():
	pass # Replace with function body.
	
func _getInsertIndex(highscore: Highscore):
	return _highscores.bsearch_custom(highscore,Highscore,"highestAndOldest",false)

func getHighscores():
	return _highscores
	
func isHighscore(score: int):
	# make a temporary highscore so we can compare
	var tempHighscore = Highscore.new("temp_player",score)
	
	# find where the score would be inserted if it were a new highscore
	var index = _getInsertIndex(tempHighscore)
	
	return index < max_highscores
	
func addHighscore(highscore: Highscore):
	# insert the new score to the list
	_highscores.append(highscore)
	_highscores.sort_custom(Highscore,"highestAndOldest")
	
	# remove the lowest score
	if _highscores.size() > max_highscores:
		_highscores.pop_back()
	
	emit_signal("new_score_added")
	
func save_game():
	var highscoreData = []
	for highscore in _highscores:
		highscoreData.append(highscore.save_game())
		
	var data = {
		highscores = highscoreData
		}
	return data
	
func load_game(data: Dictionary):
	for scoreData in data["highscores"]:
		var hs = Highscore.new("temp",0)
		hs.load_game(scoreData)
		addHighscore(hs)