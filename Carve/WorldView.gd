extends MarginContainer

var MAX_TREES = 40
var DOWNHILL_SPEED_SCALE = 1.5/300.0
var SAVE_GAME_PATH = "user://carve.save"

enum GameState {
	eRespawn,eMenu,eStaged,ePlay,ePostgame
}

var Tree = preload("res://tree/Tree.tscn")
var Utils = preload("res://Utils.gd")
var rng = RandomNumberGenerator.new()
var ready = false
var roundSeed = 0
var numPlayers = 0
var currPlayer = 0
var p1Lives = 0
var p2Lives = 0
var P1_COLOR = Color('#9696FF')
var P2_COLOR = Color('#FFAA3C')

onready var p = $player
onready var camera = $Camera2D
onready var score_label = $UI/topbar/score
onready var highscore_label = $UI/topbar/highscore

var arcadeMode = false
var androidMode = false
var pressedKey = false
var gs = GameState.eMenu
var prevDrawStart_ms
var rotate = false
# multipliers against tree widths
var proxBins = [1.5,1.0,0.5]
var scoreBins = [25,50,100]
var colorBins = [Color('#F79B0F'),Color('#0FAFF7'),Color('#9D0FF7')]

# time to live (used for off course)
var MAX_TTL_MS = 3000
var ttl_ms = MAX_TTL_MS

func _ready():
	rng.randomize()
	roundSeed = rng.randi()
	if OS.has_feature("arcade_mode"):
		arcadeMode = true
		# hide the mouse when hovering over game
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	if OS.has_feature("Android"):
		androidMode = true

	# setup
	load_game()
	respawn()
	($player).connect('collision_event',self,'on_player_collision')
	($player).setShredNode(($shred_particles))
	
	ready = true

# Note: This can be called from anywhere inside the tree. This function is
# path independent.
# Go through everything in the persist category and ask them to return a
# dict of relevant variables
func save_game():
	var save_game = File.new()
	save_game.open(SAVE_GAME_PATH, File.WRITE)
	var save_data = ($game_data).call("save");
	save_game.store_line(to_json(save_data))
	save_game.close()

# Note: This can be called from anywhere inside the tree. This function
# is path independent.
func load_game():
	var save_game = File.new()
	if not save_game.file_exists(SAVE_GAME_PATH):
		return # Error! We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(SAVE_GAME_PATH, File.READ)
	var save_data = parse_json(save_game.get_line())
	($game_data).load(save_data)
	save_game.close()

func respawn():
	# set the random seed back so p2 can have same game as p1
	GameGlobals.random.seed = roundSeed
	
	for shred in $shred_particles.get_children():
		shred.queue_free()

	for tree in $trees.get_children():
		tree.queue_free()

	for tt in range(MAX_TREES):
		var tree = Tree.instance()
		tree.connect("score",self,"on_tree_score")
		# trees is YSort'd, so draw order is from highest to lowest
		$trees.add_child(tree)
		tree.spawn()

	$player.visible = false
	$player.respawn()

func updateHighscore(score):
	var game_data = ($game_data)
	if (score > game_data.highscore):
		game_data.highscore = score

func incrementPlaycount():
	($game_data).playcount += 1

func _input(event):
	# quit the game when escape is pressed
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()

	if gs == GameState.eMenu:
		if Input.is_action_pressed("p1_start"):
			numPlayers = 1
			pressedKey = true
		elif Input.is_action_just_pressed("p2_start"):
			numPlayers = 2
			pressedKey = true
		elif numPlayers > 0 and Input.is_action_just_pressed("p1_button"):
			# continue game with current player count
			pressedKey = true
	elif gs == GameState.eStaged or gs == GameState.ePostgame:
		if Input.is_action_pressed(("p%d_button" % currPlayer)):
			pressedKey = true
	elif gs == GameState.ePlay:
		if Input.is_action_pressed(("p%d_button" % currPlayer)):
			p.carve()

func _setSpeed(speed):
	$player.set_speed(speed)
	for tree in $trees.get_children():
		tree.set_speed(speed)

func _process(delta):
	var dt_ms = delta * 1000.0
	var h = ($snow).get_rect().size.y
	var w = ($snow).get_rect().size.x
	
	var offCourse = false
	var showMenu = false
	var showStaging = false

	match gs:
		GameState.eRespawn:
			gs = GameState.eStaged
			if p1Lives == 0 and p2Lives == 0:
				roundSeed = rng.randi()
				gs = GameState.eMenu
			else:
				currPlayer += 1
				if currPlayer == 1:
					$Camera2D.rotation_degrees = 0
				else:
					$Camera2D.rotation_degrees = 180
					
			_setSpeed(0)
			ttl_ms = MAX_TTL_MS
			pressedKey = false
			respawn()
					
		GameState.eMenu:
			$Camera2D.rotation_degrees = 0
			p.visible = false
			showMenu = true
			if numPlayers > 0 and pressedKey:
				pressedKey = false
				p1Lives = 1
				p2Lives = 0
				if numPlayers > 1:
					p2Lives = 1
				currPlayer = 1
				gs = GameState.eStaged
			elif not arcadeMode:
				p1Lives = 1
				numPlayers = 1
				currPlayer = 1
				gs = GameState.eStaged
		GameState.eStaged:
			p.set_player(currPlayer)
			p.visible = true
			showStaging = true
			if pressedKey:
				gs = GameState.ePlay
				if currPlayer == 1:
					p1Lives -= 1
				else:
					p2Lives -= 1
				p.carve()
				pressedKey = false
		GameState.ePlay:
			_setSpeed(DOWNHILL_SPEED_SCALE * w)

			# see if player is off screen
			if p.position.x < 0 or p.position.x > w:
				offCourse = true
		GameState.ePostgame:
			# update game data and save
			updateHighscore(p.get_score())
			incrementPlaycount()
			save_game()

			# stop game play
			_setSpeed(0)

			var pgp = $UI/postgame_panel
			pgp.setHighscore(($game_data).highscore)
			pgp.setPlayerScore(p.get_score())
			pgp.visible = true

			if pressedKey:
				pressedKey = false
				pgp.visible = false
				gs = GameState.eRespawn

	# draw score texts
	score_label.set_text('%d' % p.get_score())
	highscore_label.set_text('%d' % ($game_data).highscore)

	$UI/menu_text.visible = showMenu or showStaging
	$"UI/1or2player".visible = showMenu and arcadeMode
	if showMenu:
		var menuString = '1 or 2 Players?'
		if numPlayers > 0:
			menuString += '\nOr continue?'
		$UI/menu_text.set_text(menuString)

	if showStaging:
		$UI/menu_text.set_text('Player %d\nPress button to carve!' % currPlayer)

	# draw OFF COURSE warning
	($blood).visible = offCourse
	if offCourse:
		# decrement time to live when off course
		ttl_ms -= dt_ms
		if ttl_ms < 0:
			$Sounds/wilhelm.play(0.0)
			gs = GameState.ePostgame;

		# blood fill
		var alpha = Utils.map(ttl_ms,MAX_TTL_MS,0,0,1.0)
		($blood).modulate = Color(1,1,1,alpha)

func on_player_collision():
	$Sounds/grunt.play(0.0)
	gs = GameState.ePostgame

func on_tree_score(points):
	$Sounds/score.play(0.0)
	$player.add_points(points)

func _on_TouchScreenButton_pressed():
	pressedKey = true
	if gs == GameState.ePlay:
		p.carve()

func _on_WorldView_resized():
	if not ready:
		yield(self,"tree_entered")
	
	# keep the camera centered when screen resizes
	camera.offset = rect_size / 2

func _on_WorldView_ready():
	# keep the camera centered when screen resizes
	camera.offset = rect_size / 2
