extends Node2D

onready var _proc_sw := StopwatchFactory.create(name,10,true)

func _process(_delta):
	_proc_sw.start()
	
	for _i in range(100):
		OS.get_ticks_usec()
	
	_proc_sw.stop()
