#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <termios.h>
#include <time.h>

char getch() {
    struct termios tty_opts_backup, tty_opts_raw;

    if (!isatty(STDIN_FILENO)) {
      printf("Error: stdin is not a TTY\n");
      exit(1);
    }

    // Back up current TTY settings
    tcgetattr(STDIN_FILENO, &tty_opts_backup);

    // Change TTY settings to raw mode
    cfmakeraw(&tty_opts_raw);
    tcsetattr(STDIN_FILENO, TCSANOW, &tty_opts_raw);

    // Read a char from stdin
    char c = getchar();

    // Restore previous TTY settings
    tcsetattr(STDIN_FILENO, TCSANOW, &tty_opts_backup);

    return c;
}

unsigned long long time_us() {
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
}

struct CellState {
    char color;
    bool flooded;
    bool explored;
};

class Board {
public:
    Board(size_t width, size_t height, const std::string &colors)
     : cells_()
     , colors_(colors)
     , width_(width)
     , height_(height)
     , flood_total_(0) {
        if (width < 2 || height < 2) {
            throw std::runtime_error("board must be at at least 2x2");
        }
        cells_.resize(width_ * height_);
        for (auto &cell : cells_) {
            cell.color = colors[rand() % colors.length()];
            cell.flooded = false;
            cell.explored = false;
        }

        // init board by flooding upper left cell with current color
        flood_total_ = flood(getCell(0,0).color);
    }

    CellState & getCell(size_t x, size_t y) {
        return cells_.at(x + width_ * y);
    }

    void print() {
        for (unsigned int y=0; y<height_; y++) {
            for (unsigned int x=0; x<width_; x++) {
                printf("%c ", getCell(x,y).color);
            }
            printf("\n");
        }
    }

    void printState() {
        for (unsigned int y=0; y<height_; y++) {
            for (unsigned int x=0; x<width_; x++) {
                printf("%c ", getCell(x,y).color);
            }
            printf("  ");
            for (unsigned int x=0; x<width_; x++) {
                printf("%c ", (getCell(x,y).flooded ? 'F' : '-'));
            }
            printf("  ");
            for (unsigned int x=0; x<width_; x++) {
                printf("%c ", (getCell(x,y).explored ? 'E' : '-'));
            }
            printf("\n");
        }
    }

    int flood(char flood_c) {
        if (colors_.find(flood_c) != std::string::npos) {
            auto start_us = time_us();
            clearExplored();
            flood_total_ = floodInternal(0, 0, flood_c);
            auto delta_us = time_us() - start_us;
            printf("delta_us = %lld\n", delta_us);
            return flood_total_;
        } else {
            printf("must use one of these colors: \"%s\"\n", colors_.c_str());
            return 0;
        }
    }

    bool complete() {
        return flood_total_ >= (width_ * height_);
    }

private:
    void clearExplored() {
        for (auto &cell : cells_) {
            cell.explored = false;
        }
    }

    int floodInternal(size_t x, size_t y, char flood_c) {
        int flood_total = 0;
        CellState &this_cell = getCell(x,y);
        if (this_cell.explored) {
            return flood_total;
        }
        this_cell.explored = true;

        // capture unflooded cells that match our flood color
        bool explore = false;
        if ( ! this_cell.flooded && this_cell.color == flood_c) {
            this_cell.flooded = true;
            flood_total++;
            explore = true;
        } else if (this_cell.flooded && this_cell.color != flood_c) {
            this_cell.color = flood_c;
            flood_total++;
            explore = true;
        }

        if (explore) {
            if (x > 0) {
                flood_total += floodInternal(x-1, y, flood_c);// explore left
            }
            if (y > 0) {
                flood_total += floodInternal(x, y-1, flood_c);// explore up
            }
            if ((x+1) < width_) {
                flood_total += floodInternal(x+1, y, flood_c);// explore right
            }
            if ((y+1) < height_) {
                flood_total += floodInternal(x, y+1, flood_c);// explore down
            }
        }

        return flood_total;
    }

private:
    std::vector<CellState> cells_;
    std::string colors_;
    size_t width_;
    size_t height_;
    size_t flood_total_;

};

int main() {
    Board board(8,8,"rgby");

    unsigned int steps = 0;
    while ( ! board.complete()) {
        board.print();
        printf("Enter color (q to quit): ");
        char flood_c = getch();
        printf("%c\n", flood_c);
        if (flood_c == 'q') {
            printf("good bye!\n");
            exit(0);
        }
        if (board.flood(flood_c) > 0) {
            steps++;
        }
    }
    board.print();
    printf("You win!\n");
    printf("It took you %d steps to flood the board.\n", steps);
    return 0;
}