extends Node

signal count_changed(count)
signal count_complete()

export var max_count = 3
export var interval = 1

onready var timer = $Timer
onready var beep1 = $beep1
onready var beep2 = $beep2

var _curr_count = 0

func start():
	_curr_count = max_count
	beep1.play()
	emit_signal("count_changed",_curr_count)
	timer.start(interval)

func _on_Timer_timeout():
	_curr_count -= 1
	emit_signal("count_changed",_curr_count)
	if _curr_count > 0:
		beep1.play()
		timer.start(interval)
	else:
		beep2.play()
		emit_signal("count_complete")
