extends Node2D

var player = null
var lSensor = null
var rSensor = null
var fSensor = null
var thinkTimer = Timer.new()
var enabled = false
var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(thinkTimer)# must add in order to process
	rng.randomize()
	thinkTimer.connect("timeout",self,"_think")

func detach():
	enabled = false

func attach(player):
	enabled = true
	self.player = player
	lSensor = player.get_node("leftSensor")
	rSensor = player.get_node("rightSensor")
	fSensor = player.get_node("frontSensor")
	
	if fSensor == null:
		print_debug("front is nill")
	
func roundBegin():
	if enabled:
		player.giveInput("u")
		
		# start thinking...
		thinkTimer.start(0.1)
	
func roundEnd():
	thinkTimer.stop()
	
func _getSensorInfo():
	var info = {"l":false,"r":false,"f":false}
	
	var fci = fSensor.move_and_collide(Vector2(0,0),true,true,true)
	var lci = lSensor.move_and_collide(Vector2(0,0),true,true,true)
	var rci = rSensor.move_and_collide(Vector2(0,0),true,true,true)
	if fci:
		info["f"] = true
	if lci:
		info["l"] = true
	if rci:
		info["r"] = true
		
	return info
	
func _pickRandomDir():
	return rng.randi_range(0,1) > 0
	
func _think():
	var sInfo = _getSensorInfo()
	if sInfo["f"]:
		# something in front of us
		var turnLeft = true
		
		if sInfo["l"] and sInfo["r"]:
			# something on both sides
			turnLeft = _pickRandomDir()
		elif not sInfo["l"] and not sInfo["r"]:
			# nothing on either side
			turnLeft = _pickRandomDir()
		elif sInfo["l"]:
			# something on left side
			turnLeft = false
		elif sInfo["r"]:
			turnLeft = true
			
		if turnLeft:
			player.turnLeft()
		else:
			player.turnRight()
