tool
extends StaticBody

func _ready():
	$CSGPolygon.polygon = $CollisionPolygon.polygon
	$CSGPolygon.depth = $CollisionPolygon.depth
	$CSGPolygon.translation.z = $CSGPolygon.depth / 2