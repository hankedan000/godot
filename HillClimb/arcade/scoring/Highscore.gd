extends Object
class_name Highscore

var _score: int = 0
var _epochTime: int = 0
var _playerName: String = ""

func _init(playerName: String, score: int):
	_score = score
	_epochTime = OS.get_unix_time()
	_playerName = playerName
	
func score():
	return _score
	
func epochTime():
	return _epochTime
	
func playerName():
	return _playerName
	
static func highestAndOldest(lhs: Highscore, rhs: Highscore):
	if lhs.score() > rhs.score():
		return true
	elif lhs.score() < rhs.score():
		return false
	else:
		# Yes, I realize the below satement use a less than.
		# That's because older score's should be sorted with
		# higher precedance than newer scores.
		if lhs.epochTime() < rhs.epochTime():
			return true
	return false
	
func save_game():
	var saveDict = {
			score      = _score,
			epochTime  = _epochTime,
			playerName = _playerName
		}
	return saveDict

func load_game(saveDict):
	_score      = saveDict["score"]
	_epochTime  = saveDict["epochTime"]
	_playerName = saveDict["playerName"]
