extends KinematicBody2D
class_name Player

export var player_fill = Color()
export var isPlayer1 = false

signal hit(this,other)
#     _________________
#    /                 \
#   /       _____       \
#  /       /     \       \
# /       /       \       \
# |       |       |       |
#     ^           ^
#     A           B
# distance from A to B
# units: pixels
const TURN_DISTANCE = 300
# units: radians
var MAX_SLIP_ANGLE = PI / 6.0

var DRIFT_INITIAL_PITCH = 0.5
var DRIFT_FINAL_PITCH = 1.0
var MAX_DRIFT_DURATION = 2.5
var DRIFT_PITCH_HYSTERESIS = 0.03

# --------------------------------------------

var BUTTON_ACTION = "null"
# car's linear velocity
# units: pixels/second
var vLinear = 0
# units: radians/second
var rotVelocity = 0
# units: radians/second
var slipAngleVelocity = 0
var slipAngle = 0
# units: radians/second
var spawnPosition = Vector2()
var spawnAngle = 0
var v = Vector2()
var stopped = true
var driftPitchVel = (DRIFT_FINAL_PITCH - DRIFT_INITIAL_PITCH) / MAX_DRIFT_DURATION
var driftPitch = DRIFT_INITIAL_PITCH

func _ready():
	$ArcadeInput.is_player1 = isPlayer1
	BUTTON_ACTION = $ArcadeInput.get_player_action('b')
	spawnPosition = global_position
	spawnAngle = global_rotation
	$fill.color = player_fill
	
	add_to_group("player")
	respawn()

# update all required physics variables based on a
# request linear velocity of the var
# vLin: pixel/second in a straight line
func setLinearVelocity(vLin):
	var turnRadius = TURN_DISTANCE / 2.0
	
	# compute time in seconds it takes to do
	# 180 degrees turn of the care
	var t_turn = PI * turnRadius / vLin
	
	vLinear = vLin
	rotVelocity = PI / t_turn
	slipAngleVelocity = MAX_SLIP_ANGLE / t_turn
	
func respawn():
	$tire.restart()
	$tire2.restart()
	_enableTires(false)
	slipAngle = 0
	global_position = spawnPosition
	global_rotation = spawnAngle
	v = Vector2()
	$fill.visible = true
	stopped = true
	$driftSound.playing = false
	
func isPlayer1():
	return isPlayer1
	
func setStopped(stopped):
	$driftSound.playing = false
	self.stopped = stopped
	if self.stopped and not stopped:
		# starting
		$tire.restart()
		$tire2.restart()
	elif not self.stopped and stopped:
		# stopping
		_enableTires(false)

func explode():
	$fill.visible = false
	$Particles2D.emitting = true
	stopped = true
	
func _getPerspectiveRotation():
	if isPlayer1:
		return 0
	return PI

func _enableTires(enable):
	$tire.emitting = enable
	$tire2.emitting = enable

func _physics_process(delta):
	if not stopped:
		var turning = false
		
		# handle sound effect intiation/termination
		if Input.is_action_just_pressed(BUTTON_ACTION):
			driftPitch = DRIFT_INITIAL_PITCH
			$driftSound.play(0)
		elif Input.is_action_just_released(BUTTON_ACTION):
			$driftSound.playing = false
			
		# handle drifting effects
		if Input.is_action_pressed(BUTTON_ACTION):
			rotation += rotVelocity * delta
			
			slipAngle += slipAngleVelocity * delta
			turning = true
			driftPitch += driftPitchVel * delta
			if driftPitch > DRIFT_FINAL_PITCH:
				driftPitch -= DRIFT_PITCH_HYSTERESIS
		else:
			slipAngle -= slipAngleVelocity * 2 * delta
			
		$driftSound.pitch_scale = driftPitch
		
		# turn on/off tire screech
		_enableTires(turning)
		
		slipAngle = min(MAX_SLIP_ANGLE,slipAngle)
		slipAngle = max(0,slipAngle)
		
		v = Vector2(0,-1).rotated(rotation - slipAngle) * vLinear * delta
		
		var collision_info = move_and_collide(v)
		if collision_info:
			var other = collision_info.collider
			
			setStopped(true)
			$driftSound.playing = false
			$impactSound.play(0)
			emit_signal("hit",self,other)
