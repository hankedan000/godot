extends VBoxContainer

const SERVER_IP = "192.168.1.65"
const SERVER_PORT = 5679

var peer = NetworkedMultiplayerENet.new()

func _ready():
	set_process(false)
	get_tree().connect("server_disconnected",self,"_server_disonnected")

remote func sync_ball(position):
	$Ball.position = position

func _process(delta):
	for peer in get_tree().get_network_connected_peers():
		rpc_unreliable_id(peer,"sync_ball",$Ball.position)

func _server_disonnected():
	$ServerButton.disabled = false
	$ClientButton.disabled = false

func _on_ServerButton_pressed():
	peer.create_server(SERVER_PORT,4)
	get_tree().network_peer = peer
	$ServerButton.disabled = true
	$ClientButton.disabled = true
	
	$Ball.v = Vector2(5,2)
	set_process(true)

func _on_ClientButton_pressed():
	peer.create_client(SERVER_IP,SERVER_PORT)
	get_tree().network_peer = peer
	$ServerButton.disabled = true
	$ClientButton.disabled = true
