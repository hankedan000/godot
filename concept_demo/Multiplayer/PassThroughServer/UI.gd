extends Panel

const SERVER_HOST = 'localhost'
const SERVER_PORT = 1234
var peer = NetworkedMultiplayerENet.new()

func _ready():
	if OS.has_feature('Server'):
		peer.create_server(SERVER_PORT)
		get_tree().network_peer = peer
	else:
		peer.create_client(SERVER_HOST,SERVER_PORT)
		get_tree().network_peer = peer
