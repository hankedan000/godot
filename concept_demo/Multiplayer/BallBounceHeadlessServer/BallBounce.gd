extends VBoxContainer

const SERVER_IP = "danielhankewycz.com"
const SERVER_PORT = 5679

var peer = NetworkedMultiplayerENet.new()

var headless = false

func _ready():
	headless = OS.has_feature("Server")
	
	set_process(false)
	get_tree().connect("server_disconnected",self,"_server_disonnected")
	
	if headless:
		_on_ServerButton_pressed()

remote func sync_ball(position):
	$Ball.position = position

func _process(delta):
	for peer in get_tree().get_network_connected_peers():
		rpc_unreliable_id(peer,"sync_ball",$Ball.position)

func _server_disonnected():
	$ServerButton.disabled = false
	$ClientButton.disabled = false

func _on_ServerButton_pressed():
	peer.create_server(SERVER_PORT,4)
	get_tree().network_peer = peer
	$ServerButton.disabled = true
	$ClientButton.disabled = true
	
	$Ball.v = Vector2(5,2)
	set_process(true)
	print("Server initialized...")

func _on_ClientButton_pressed():
	peer.create_client(SERVER_IP,SERVER_PORT)
	get_tree().network_peer = peer
	$ServerButton.disabled = true
	$ClientButton.disabled = true
