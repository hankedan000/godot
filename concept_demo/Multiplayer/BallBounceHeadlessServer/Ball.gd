extends KinematicBody2D

var v = Vector2()

func _process(delta):
	var ci = move_and_collide(v)
	
	if ci:
		v = v.bounce(ci.normal)
