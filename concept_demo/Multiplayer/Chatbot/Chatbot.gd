extends VBoxContainer

onready var line_edit = $HBoxContainer/LineEdit
onready var chat = $RichTextLabel

const SERVER_IP = "192.168.1.65"
const SERVER_PORT = 5678

var peer = NetworkedMultiplayerENet.new()

func _ready():
	OS.request_permissions()
	get_tree().connect("network_peer_connected",self,"_player_connected")
	get_tree().connect("network_peer_disconnected",self,"_player_disconnected")
	get_tree().connect("connected_to_server",self,"_connect_ok")
	get_tree().connect("connection_failed",self,"_connection_failed")
	get_tree().connect("server_disconnected",self,"_server_disconnected")

remotesync func recv_message(msg):
	var id = get_tree().get_rpc_sender_id()
	var name = get_player_name(id)
	add_chat(msg,id)

func add_chat(msg,id=-1):
	var my_id = get_tree().get_network_unique_id()
	var name_color = Color.black
	if id == my_id:
		name_color = Color.coral
	elif id != -1:
		name_color = Color.lightgreen
	chat.push_color(name_color)
	chat.append_bbcode("%s: " % get_player_name(id))
	chat.pop()
	chat.append_bbcode("%s\n" % msg)

func send_message(text):
	line_edit.text = ""
	rpc("recv_message",text)

func get_player_name(id):
	match (id):
		-1:
			return "Chatbot"
		1:
			return "Server"
		_:
			return "Client%d" % id

func _on_LineEdit_text_entered(new_text):
	send_message(new_text)

func _on_ServerButton_pressed():
	peer.create_server(SERVER_PORT,4)
	get_tree().network_peer = peer
	$ServerButton.disabled = true
	$ClientButton.disabled = true
	$HBoxContainer.visible = true
	line_edit.grab_focus()

func _on_ClientButton_pressed():
	peer.create_client(SERVER_IP,SERVER_PORT)
	get_tree().network_peer = peer
	$HBoxContainer.visible = true
	$ServerButton.disabled = true
	$ClientButton.disabled = true
	line_edit.grab_focus()

func _player_connected(id):
	add_chat("'%s' joined chat!" % get_player_name(id))
	
func _player_disconnected(id):
	add_chat("'%s' left chat!" % get_player_name(id))

# called on client when successfully connected to server
func _connect_ok():
	add_chat("Connected to %s:%d" % [SERVER_IP,SERVER_PORT])

# called on client when connection to server failed
func _connection_failed():
	add_chat("Failed to connect to %s:%d" % [SERVER_IP,SERVER_PORT])
	
func _server_disconnected():
	add_chat("Server disconnected!")
	$ServerButton.disabled = false
	$ClientButton.disabled = false

func _on_Chatbot_tree_exiting():
	# disconnect self from network
	peer.disconnect_peer(get_tree().get_network_unique_id())
