tool
extends TextEdit

signal line_entered(input)
signal num_lines_changed()

export var prompt = ">:"
export var history_depth = 80
export (Color) var warn_color = Color("ffb940")
export (Color) var error_color = Color("ff4545")

var _prev_num_lines = 1
var _cursor_history = []
var _command_history = []
# represent which historical command the user is current on
# 0   -> newest command
# 1++ -> oldest command
var _curr_history = 0

func _ready():
	text = prompt
	cursor_set_column(prompt.length())

func insert_normal_output_text(text):
	_push_cursor()
	cursor_set_column(0)
	insert_text_at_cursor(text + "\n")
	_pop_cursor()
	
func insert_warn_output_text(text):
	_push_cursor()
	var lines_to_hide = []
	cursor_set_column(0)
#	add_color_region("WARN_BEGIN","WARN_END",warn_color,true)
	
	lines_to_hide.push_back(cursor_get_line())
#	insert_text_at_cursor("WARN_BEGIN\n")
	
	insert_text_at_cursor(text + "\n")
	
	lines_to_hide.push_back(cursor_get_line())
#	insert_text_at_cursor("WARN_END\n")
	
	_pop_cursor()
	
func _push_cursor():
	_cursor_history.push_back(cursor_get_column())
	
func _pop_cursor():
	if _cursor_history.size() > 0:
		cursor_set_column(_cursor_history.pop_back())
		cursor_set_line(get_line_count())

func _push_history(input):
	_command_history.push_front(input)
	if _command_history.size() > history_depth:
		_command_history.pop_back()
	
func _user_enter():
	# at this point, the TextEdit has already inserted a newline
	# 'input' represents the entire user input line (prompt included)
	var before_return = get_line(get_line_count() - 2)
	var after_return = get_line(get_line_count() - 1)
	var input = before_return + after_return
	
	# ihis mess is here to prevent the text from being split accross
	# two lines when someone hits enter in the middle of their input.
	# it removes the split text and re-adds it with a clean newline
	# at the end of the input.
	text = text.substr(0,text.length() - input.length() - 1)
	text += input + "\n"
	
	# move to next line
	text += prompt
	_cursor_to_end()
	
	# get the inputed text without the prompt
	var input_no_prompt = input.substr(prompt.length(),input.length())
	_push_history(input_no_prompt)
	_curr_history = 0
	emit_signal("line_entered",input_no_prompt)
	
func get_current_input():
	var input_line = get_line(get_line_count() - 1)
	return input_line.substr(prompt.length(),input_line.length())
	
func _clear_input_line():
	var input_line = get_line(get_line_count() - 1)
	text = text.substr(0,text.length() - input_line.length())
	text += prompt
	_cursor_to_end()
	
func _cursor_to_end():
	var input_line = get_line(get_line_count() - 1)
	cursor_set_line(get_line_count())
	cursor_set_column(input_line.length())
	
func _up_history():
	if _command_history.size() > 0:
		if _curr_history == 0:
			_push_history(get_current_input())
		_curr_history = min(_curr_history + 1,_command_history.size() - 1)
		_clear_input_line()
		var historical_cmd = _command_history[_curr_history]
		insert_text_at_cursor(historical_cmd)
	
func _down_history():
	if _curr_history > 0:
		_curr_history -= 1
		_clear_input_line()
		var historical_cmd = _command_history[_curr_history]
		insert_text_at_cursor(historical_cmd)
	
func _on_Terminal_gui_input(event):
	if event is InputEventKey and event.is_pressed():
		var scancode = event.get_scancode()
		match(scancode):
			KEY_ENTER:
				_user_enter()
			KEY_UP:
				_up_history()
			KEY_DOWN:
				_down_history()

func _on_Terminal_cursor_changed():
	readonly = cursor_get_line() < (get_line_count() - 1)
	
	if not readonly:
		if cursor_get_column() < prompt.length():
			cursor_set_column(prompt.length())

func _on_Terminal_text_changed():
	# see if we need to scroll to the bottom
	var num_lines = get_line_count()
	if _prev_num_lines != num_lines:
		_prev_num_lines == num_lines
		emit_signal("num_lines_changed")
	
	# prevent user from backspacing the prompt
	if prompt.length() > 0 and cursor_get_column() < prompt.length():
		var input_line = get_line(get_line_count() - 1)
		if input_line.substr(0,prompt.length()) != prompt:
			insert_text_at_cursor(prompt[prompt.length()-1])
