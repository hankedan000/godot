extends VBoxContainer

func _ready():
	_update_line_column_indactor()

func _update_line_column_indactor():
	var line = $Terminal.cursor_get_line()
	var col = $Terminal.cursor_get_column()
	var ind_text = "%d:%d" % [line,col]
	$BottomPanel/Row/LineColumn.text = ind_text

func _on_Terminal_cursor_changed():
	_update_line_column_indactor()

func _on_Terminal_line_entered(input: String):
	if input == "exit":
		get_tree().quit()
	else:
		$Terminal.insert_normal_output_text(input.to_upper())
