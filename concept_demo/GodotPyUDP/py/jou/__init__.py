import json
import math

def send_obj(obj,uuid,sock,addr):
	MAX_OBJ_BYTES_PER_PACKET = 60000
	obj_json = json.dumps(obj,separators=(",",":")).replace("\"","\\\"")

	n_packets = math.ceil(float(len(obj_json)) / MAX_OBJ_BYTES_PER_PACKET)

	for p in range(n_packets):
		slice_i = p * MAX_OBJ_BYTES_PER_PACKET
		slice_f = slice_i + MAX_OBJ_BYTES_PER_PACKET
		obj_slice = obj_json[slice_i:slice_f].replace("\\\"","\"")

		packet = {
			"req_uid": uuid,
			"packet": p,
			"n_packets": n_packets,
			"obj_slice": obj_slice
			}
		packetJSON = json.dumps(packet,separators=(",",":"))
		sock.sendto(packetJSON.encode("utf-8"), addr)