#!/usr/bin/env python3
import socket
import argparse
import jou
import time
import queue
from threading import Thread

parser = argparse.ArgumentParser(
    prog='dispserver',
    usage='%(prog)s [options]',
    description='Services a socket connection to compute fips vectors')

args = parser.parse_args()

HOST = '127.0.0.1'
PORT = 1230

s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
try:
	s.bind((HOST,PORT))
except:
	s.bind(('localhost',PORT))

class WorkerThread(Thread):
	def __init__(self,sock,addr):
		super(WorkerThread,self).__init__()
		self.sock = sock
		self.addr = addr
		self.queue = queue.Queue()
		self.alive = True
		self._stayAlive = True

	def addMsg(self,msg):
		self.queue.put(msg)

	def quit(self):
		print('Requested worker for %s to quit...' % str(self.addr))
		self._stayAlive = False

	def run(self):
		try:
			while self._stayAlive:
				msg = self.queue.get(block=True)
				jou.send_obj(msg,0,self.sock,self.addr)
		except socket.error as e:
			print('caught exception in worker %s' % (str(self.addr)))
			print(e)

		print('Worker for %s stopped.' % str(self.addr))
		self.alive = False

print('Listening...')
workers = {}

class ProducerThread(Thread):
	def __init__(self):
		super(ProducerThread,self).__init__()
		self.alive = True
		self._stayAlive = True

	def quit(self):
		print('Requested producer to quit...')
		self._stayAlive = False

	def run(self):
		try:
			count = 0
			while self._stayAlive:
				time.sleep(1.0)
				count += 1
				delete_me = []

				for addr,worker in workers.items():
					if worker.alive:
						worker.addMsg({"count":count})
					else:
						print('Worker no longer alive... removing from list')
						delete_me.append(addr)

				for addr in delete_me:
					del workers[addr]

		except Exception as e:
			print(e)
			# shutdown the thread

		print("Producer shutdown complete.")
		self.alive = False

producer = ProducerThread()
producer.start()

try:
	while True:
		data, addr = s.recvfrom(1024)

		data = data.decode('utf-8')
		if data == 'quit':
			if addr in workers:
				workers[addr].quit()
		elif data == 'poke':
			print('Found a new client! %s' % (str(addr)))
			worker = WorkerThread(s,addr)
			worker.start()
			workers[addr] = worker
		else:
			print('Unhandled data \'%s\'' % data)

except KeyboardInterrupt:
	print('Shutting down...')

# notify all workers to shutdown
for worker in workers.values():
	worker.quit()

# notify producer to shutdown
producer.quit()

# wait for all workers to join
for worker in workers.values():
	worker.join()

# wait for producer to join
producer.join()

# close the socket now that everyone is joined
s.close()
