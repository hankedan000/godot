extends Node

signal playback_action(action)

onready var prev_action_time = OS.get_ticks_msec()

var recorded_actions = []
var next_action_to_play : InputEventAction = null
var playing = false

var action_press_state := Dictionary()
var action_just_press_state := Dictionary()

func is_playing():
	return playing

func get_history():
	return recorded_actions.duplicate(true)
	
func play_history(history):
	recorded_actions = history
	play()

func play():
	playing = true
	
	# clear any existing actions
	action_press_state.clear()
	action_just_press_state.clear()
	
	# begin playback of all recorded actions
	_play_next_event()

func record(action: String, pressed: bool):
	# don't record anything while we're playing back events
	if playing:
		return
	
	var now = OS.get_ticks_msec()
	
	# make an InputEventAction for playback purposes
	var actionEvent = InputEventAction.new()
	actionEvent.pressed = pressed
	actionEvent.action = action
	
	var record_it = false
	if pressed and not is_action_pressed(action):
		# event is not a reocurring press event... record it!
		record_it = true
	elif not pressed:
		# since release events are reoccuring, alway record them!
		record_it = true
		
	if record_it:
		var delay = (now - prev_action_time) / 1000.0
		recorded_actions.push_back({
			"delay": delay,
			"action": actionEvent})
			
		prev_action_time = now
	
		# handle internal state management of action
		if pressed:
			_action_press(action)
		else:
			_action_release(action)
	
func is_action_pressed(action):
	if action_press_state.has(action):
		return action_press_state[action]
	return false
	
func is_action_released(action):
	if action_press_state.has(action):
		return not action_press_state[action]
	return false
	
func is_action_just_pressed(action):
	if action_just_press_state.has(action):
		var state = action_just_press_state[action]
		# clear the flag
		action_just_press_state[action] = false
		return state
	return false
	
func _action_press(action):
	action_press_state[action] = true
	action_just_press_state[action] = true
	
func _action_release(action):
	action_press_state[action] = false
	action_just_press_state[action] = false
	
func _play_next_event():
	if next_action_to_play:
		if next_action_to_play.pressed:
			_action_press(next_action_to_play.action)
		else:
			_action_release(next_action_to_play.action)
		emit_signal("playback_action",next_action_to_play)
		
	if recorded_actions.empty():
		playing = false
	else:
		var record = recorded_actions.pop_front()
		next_action_to_play = record["action"]
		$Timer.start(record["delay"])
	