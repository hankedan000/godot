extends Node

var Player = preload("res://Characters/Player/Player.tscn")

var curr_player = null

func _ready():
	_spawn_new_player()
	
func _process(delta):
	if is_instance_valid(curr_player):
		$Camera2D.global_position = curr_player.global_position

func _on_Player_respawn(recorded_input):
	# spawn the clone
	var clone = Player.instance()
	clone.global_position = $SpawnPoint.global_position
	add_child(clone)
	clone.playback(recorded_input)
	
	_spawn_new_player()
	
func _spawn_new_player():
	# spawn the player
	curr_player = Player.instance()
	curr_player.global_position = $SpawnPoint.global_position
	curr_player.connect("respawn",self,"_on_Player_respawn")
	add_child(curr_player)