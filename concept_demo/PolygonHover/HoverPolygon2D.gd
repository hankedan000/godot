extends Node2D

signal mouse_entered
signal mouse_exited

func _on_Area_mouse_entered():
	emit_signal("mouse_entered")
	
func _on_Area_mouse_exited():
	emit_signal("mouse_exited")
