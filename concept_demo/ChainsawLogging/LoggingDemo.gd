extends Panel

var my_int = StrObject.new()

func _ready():
	Logger.trace(self,"This is a TRACE log")
	Logger.debug(self,"This is a DEBUG log")
	Logger.info(self,"This is a INFO log")
	Logger.warn(self,"This is a WARN log")
	Logger.error(self,"This is a ERROR log")
	Logger.fatal(self,"This is a FATAL log")

func _on_Timer_timeout():
	Logger.trace(self,"my_int = %s",[my_int])
	Logger.debug(self,"my_int = %s",[my_int])
	Logger.info(self,"my_int = %s",[my_int])

func _on_Button_pressed():
	Logger.info(self,"reset my_int")
	my_int._int = 0
