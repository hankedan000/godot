extends Object

const MAX_LAYERS = 20
var physics2D_Layers = {}

func _init():
	for ll in range(MAX_LAYERS):
		var name = ProjectSettings.get_setting("layer_names/2d_physics/layer_%d" % (ll + 1))
		if name:
			physics2D_Layers[name] = ll
			
func getPhysics2D_LayerBit(layer_name):
	return physics2D_Layers[layer_name]
