import_dirs=$(find -name ".import")
for import_dir in $import_dirs; do
	project_dir=$(dirname $import_dir)
	pushd $project_dir
		git rm --cached -r .import > /dev/null
		echo ".import" >> .gitignore
	popd
done
