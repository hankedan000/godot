extends Area2D

export (int) var blast_radius
export (int) var max_damage
export (bool) var armed

# list of bodies that are in the blast radius
var targets = []

func _ready():
	# make the blast radius unique to this explosive
	var myShape = $BlastArea/CollisionShape2D.shape.duplicate()
	myShape.radius = blast_radius
	$BlastArea/CollisionShape2D.shape = myShape
	
func start():
	pass

func detonate():
	if armed:
		$Sprite.hide()
		$Explosion.show()
		$Explosion.play("fire")
		
		# take damage from all targets in blast area
		for target in targets:
			# the further from the explosive the less it hurts
			var dist = (target.global_position - global_position).length()
			var damage = int(max_damage * (blast_radius - dist) / blast_radius)
			target.takeDamage(damage)

func _on_BlastArea_body_entered(body):
	if body.has_method("takeDamage"):
		targets.append(body)

func _on_BlastArea_body_exited(body):
	var index = targets.find(body)
	if index >= 0:
		targets.remove(index)

func _on_Explosive_body_entered(body):
	detonate()

func _on_Explosion_animation_finished():
	queue_free()
