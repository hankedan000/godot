extends "res://projectiles/Explosives/Explosive.gd"

export (float) var arming_delay

var GREEN_ARMED_SPRITE = preload("res://assets/landmine_green_armed_default.png")
var GREEN_UNARMED_SPRITE = preload("res://assets/landmine_green_unarmed_default.png")

func _ready():
	$Sprite.texture = GREEN_UNARMED_SPRITE
	if armed:
		$Sprite.texture = GREEN_ARMED_SPRITE
	
	$ArmTimer.wait_time = arming_delay

func start():
	$ArmTimer.start()

func _on_ArmTimer_timeout():
	armed = true
	$Sprite.texture = GREEN_ARMED_SPRITE
