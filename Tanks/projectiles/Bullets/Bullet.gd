extends Area2D

signal died

export (float) var speed
export (float) var lifetime
export (int) var damage
# value of 1.0 means it will track target very strongly
# value of 0.0 means no steering is applied
export (float) var steer_strength
export (int) var controlling_user = -1

# distance the bullet needs to travel before clearing the muzzle
# units: pixels
export (int) var muzzle_clear_distance = 10

var heading = 0
var target = null
var _distance = 0
var _orig_collision_mask = 0

func _ready():
	# prevent bullet from colliding until it clears the muzzle
	_orig_collision_mask = collision_mask
	collision_mask = 0
	
func get_inventory_item():
	return $InventoryItem

func start(_position,_direction,_target=null):
	position = _position
	rotation = _direction
	target = _target
	$lifetime.start(lifetime)
	heading = _direction

func explode():
	# stop moving once exploded
	set_physics_process(false)
	
	$Sprite.hide()
	$Explosion.show()
	$Explosion.play("smoke")
	
func seek():
	# default to maintaining current direction
	var steer = heading
	
	if controlling_user != -1:
		# steer based on user input
		if Input.is_action_pressed("p%d_left" % controlling_user):
			steer = heading + -PI/8
		elif Input.is_action_pressed("p%d_right" % controlling_user):
			steer = heading + PI/8
		
	elif is_instance_valid(target):
		# auto home to target
		steer = (target.position - position).angle()
		
	return steer
	
func _physics_process(delta):
	heading = lerp(heading,seek(),steer_strength)
	var velocity = Vector2(1,0).rotated(heading) * speed
	rotation = heading
	
	var delta_position = velocity * delta
	_distance += delta_position.length()
	
	if _distance > muzzle_clear_distance:
		collision_mask = _orig_collision_mask
	
	position += delta_position

func _on_lifetime_timeout():
	explode()

func _on_Bullet_body_entered(body):
	if body.has_method("takeDamage"):
		body.takeDamage(damage)
	
	# for toggling switches
	if body.has_method("toggle"):
		body.toggle()
	
	explode()

func _on_Explosion_animation_finished():
	emit_signal("died")
	queue_free()
