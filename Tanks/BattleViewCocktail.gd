extends Node2D

export (PackedScene) var Map

var map = null

func _ready():
	map = Map.instance()
	$Views/PlayerView1/Viewport.add_child(map)
	
	map.connect("gameover",self,"_on_map_gameover")
	map.connect("player2_camera_target_changed",self,"on_player2_camera_target_changed")
	
	var player = map.get_player()
	$Views/PlayerView1.bind_tile_map(map.get_tile_map())
	$Views/PlayerView1.bind_target(player)
	$Views/PlayerView1.bind_player_to_hud(player)
	player.connect("acquired_new_turret_item",$Views/Rotated/PlayerView2/HUD/VBoxContainer/Inventory,"add_item")
	player.connect("acquired_new_tank_item",$Views/PlayerView1/HUD/VBoxContainer/Inventory,"add_item")
	player.connect("turret_inventory_item_select",$Views/Rotated/PlayerView2/HUD/VBoxContainer/Inventory,"set_selected_item_by_name")
	player.connect("tank_inventory_item_select",$Views/PlayerView1/HUD/VBoxContainer/Inventory,"set_selected_item_by_name")
	$Views/Rotated/PlayerView2.bind_tile_map(map.get_tile_map())
	$Views/Rotated/PlayerView2.bind_target(map.get_player2_target())
	$Views/Rotated/PlayerView2.bind_player_to_hud(player)
	$Views/Rotated/PlayerView2/Viewport.world_2d = $Views/PlayerView1/Viewport.world_2d
	
func _on_map_gameover():
	$GameoverDialog.popup()
	
func on_player2_camera_target_changed():
	$Views/Rotated/PlayerView2.bind_target(map.get_player2_target())

func _on_GameoverDialog_restart():
	get_tree().reload_current_scene()
	
	# We could try doing map.queue_free() and then reloading
	# what we did in the _ready() method. I think we'll need
	# to make a method that clear out the player's HUD
	# inventories because those aren't cleaned up when we
	# delete the map
