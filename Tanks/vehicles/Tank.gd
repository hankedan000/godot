extends KinematicBody2D

signal health_changed(percent_remaining)
signal dead
signal bullet_shot(bullet,_position,direction)
signal mine_dropped(mine,_position,direction)
signal acquired_new_turret_item(item)
signal acquired_new_tank_item(item)
signal turret_inventory_item_select(item_name)
signal tank_inventory_item_select(item_name)

export (Array, PackedScene) var TurretInventory
export (Array, PackedScene) var TankInventory
export (float) var max_speed
export (float) var rotation_speed
export (float) var gun_cooldown
export (float) var mine_cooldown
export (int) var max_health
export (Color) var minimap_color = Color(1,1,1,1)

var turret_inventory_items = []
var current_turret_item = 0
var tank_inventory_items = []
var current_tank_item = 0
var velocity := Vector2()
var canShoot = true
var canDropMine = true
var alive = true
var health

func _ready():
	$GunCooldown.wait_time = gun_cooldown
	$MineCooldown.wait_time = mine_cooldown
	health = max_health
	_notifyHealthChange()
	call_deferred("_init_inventory")
	
func _init_inventory():
	for InventoryItem in TurretInventory:
		add_item_to_turret_inventory(InventoryItem.instance())
		
	for InventoryItem in TankInventory:
		add_item_to_tank_inventory(InventoryItem.instance())
		
func add_item_to_turret_inventory(item):
	turret_inventory_items.append(item)
	emit_signal("acquired_new_turret_item",item)
	
func add_item_to_tank_inventory(item):
	tank_inventory_items.append(item)
	emit_signal("acquired_new_tank_item",item)
	
func next_turret_item():
	if current_turret_item + 1 < turret_inventory_items.size():
		current_turret_item += 1
		emit_signal("turret_inventory_item_select",_get_current_turret_item().item_name)
	
func prev_turret_item():
	if current_turret_item > 0:
		current_turret_item -= 1
		emit_signal("turret_inventory_item_select",_get_current_turret_item().item_name)
	
func next_tank_item():
	if current_tank_item + 1 < tank_inventory_items.size():
		current_tank_item += 1
		emit_signal("tank_inventory_item_select",_get_current_tank_item().item_name)
	
func prev_tank_item():
	if current_tank_item > 0:
		current_tank_item -= 1
		emit_signal("tank_inventory_item_select",_get_current_tank_item().item_name)

func control(delta):
	pass
	
func explode():
	emit_signal("dead")
	queue_free()
	
func _get_current_turret_item():
	var item = null
	if current_turret_item < turret_inventory_items.size():
		item = turret_inventory_items[current_turret_item]
	return item
	
func _get_current_tank_item():
	var item = null
	if current_tank_item < tank_inventory_items.size():
		item = tank_inventory_items[current_tank_item]
	return item
	
func shoot(target=null):
	if canShoot:
		var currItem = _get_current_turret_item()
		if currItem == null:
			return
		
		var Projectile = currItem.Item
		canShoot = false
		emit_signal("bullet_shot",Projectile,$turret/muzzle.global_position,$turret.global_rotation,target)
		$GunCooldown.start()
		
		$turret/MuzzleFlash.show()
		$turret/MuzzleFlash.play()
		
func dropMine():
	if canDropMine:
		var currItem = _get_current_tank_item()
		if currItem == null:
			return
		
		var Mine = currItem.Item
		canDropMine = false
		emit_signal("mine_dropped",Mine,global_position,global_rotation)
		$MineCooldown.start()
		
func takeDamage(damage):
	health -= damage
	health = max(health,0)
	if health <= 0:
		explode()
	_notifyHealthChange()
	
func giveHealth(amount):
	health = min(health + amount,max_health)
	_notifyHealthChange()
	
func _notifyHealthChange():
	emit_signal("health_changed",health * 100.0 / max_health)
	
func _physics_process(delta):
	if not alive:
		return
	control(delta)
	move_and_slide(velocity)

func _on_GunCooldown_timeout():
	canShoot = true

func _on_MineCooldown_timeout():
	canDropMine = true

func _on_MuzzleFlash_animation_finished():
	$turret/MuzzleFlash.hide()
