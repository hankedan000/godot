extends "res://vehicles/Tank.gd"

var turretGlobalAngle := 0.0

# When set to false, turret motion and shooting is disabled.
# This is used to prevent player 2 from controlling the
# turret while they're currently steering a UserGuidedMissle.
var gun_enabled = true

func control(delta):
	# handle tank rotation
	var tankDir = 0
	if Input.is_action_pressed("p1_left"):
		tankDir = -1
	elif Input.is_action_pressed("p1_right"):
		tankDir = 1
	elif Input.is_action_pressed("p1_down"):
		next_tank_item()
	elif Input.is_action_pressed("p1_up"):
		prev_tank_item()
	rotation += rotation_speed * tankDir * delta
	
	if gun_enabled:
		# handle user control of turret rotation
		var turretDir = 0
		if Input.is_action_pressed("p2_left"):
			turretDir = -1
		elif Input.is_action_pressed("p2_right"):
			turretDir = 1
		elif Input.is_action_pressed("p2_down"):
			next_turret_item()
		elif Input.is_action_pressed("p2_up"):
			prev_turret_item()
		turretGlobalAngle += rotation_speed * turretDir * delta
		
		# handle shooting
		if Input.is_action_pressed($p2Actions.get_player_action('b')):
			shoot()
			
	$turret.global_rotation = turretGlobalAngle
	
	# handle mine dropping
	if Input.is_action_pressed($p1Actions.get_player_action('1')):
		dropMine()
	
	# handle tank speed
	var targetSpeed = 0
	if Input.is_action_pressed($p1Actions.get_player_action('b')):
		targetSpeed = max_speed
	
	var nextSpeed = lerp(velocity.length(),targetSpeed,0.1)
	velocity = Vector2(nextSpeed,0).rotated(rotation)
	
func isPlayer():
	return true