tool
extends "res://vehicles/Tank.gd"

export (int) var detect_radius setget _set_detect_radius

onready var parent = get_parent()
var speed = 0
var target = null
var turretGlobalAngle = 0
var ready = false

func _ready():
	turretGlobalAngle = $turret.global_rotation

func _set_detect_radius(value: int):
	detect_radius = value
	if !Engine.editor_hint and not ready:
		yield(self, 'ready')
	
	var circle = CircleShape2D.new()
	circle.radius = detect_radius
	$detectRadius/CollisionShape2D.shape = circle
	ready = true

func control(delta):
	# handle tank motion
	if parent is PathFollow2D:
		# handle look ahead logic
		if $lookAhead.is_colliding() or $lookAhead2.is_colliding():
			speed = lerp(speed,0,0.15)# slow down
		else:
			speed = lerp(speed,max_speed,0.05)# speed up
		
		parent.set_offset(parent.get_offset() + speed * delta)
		# make sure enemy can't get pushed off track
		position = Vector2()
	else:
		pass
	
	if target:
		# handle turret motion
		var targetDir = (target.global_position - global_position).normalized()
		turretGlobalAngle = Utils.lerp_angle(turretGlobalAngle,targetDir.angle(),0.03)
		
		# determine if enemy should shoot
		var currentDir = Vector2(1,0).rotated(turretGlobalAngle)
		if currentDir.dot(targetDir) > 0.9:
			shoot(target)
			
	$turret.global_rotation = turretGlobalAngle

func _on_detectRadius_body_entered(body):
	if body.has_method("takeDamage"):
		target = body

func _on_detectRadius_body_exited(body):
	if body == target:
		target = null
	
func isPlayer():
	return false
