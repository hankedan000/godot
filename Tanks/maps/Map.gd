extends Node2D

signal player2_camera_target_changed
signal gameover

var player2_camera_target

func _ready():
	_set_player2_camera_target($Player)
	$Player.connect("dead",self,"_on_player_died")
	
func _set_player2_camera_target(value):
	player2_camera_target = value
	emit_signal("player2_camera_target_changed")

func get_player():
	return $Player
	
func get_player2_target():
	return player2_camera_target
	
func get_tile_map():
	return $Ground

func _on_bullet_shot(bullet,_position,_direction,_target):
	var newBullet = bullet.instance()
	
	if newBullet.controlling_user != -1:
		$Player.gun_enabled = false
		_set_player2_camera_target(newBullet)
		newBullet.connect("died",self,"_on_tracking_bullet_died")
		
	add_child(newBullet)
	newBullet.start(_position,_direction,_target)

func _on_mine_dropped(mine,_position,direction):
	var newMine = mine.instance()
	newMine.global_position = _position
	newMine.global_rotation = direction
	add_child(newMine)
	newMine.start()
	
func _on_player_died():
	emit_signal("gameover")
	
func _on_tracking_bullet_died():
	if is_instance_valid($Player):
		$Player.gun_enabled = true
		_set_player2_camera_target($Player)
