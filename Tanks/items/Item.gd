extends Area2D

var collectable = true

func collect(body):
	pass

func _on_Item_body_entered(body):
	if collectable:
		# let subclass perform collection
		collect(body)
		
		# play optional sound effect
		$CollectSound.play()
			
		queue_free()