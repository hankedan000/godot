tool
extends StaticBody2D

signal button_pressed()
signal button_released()

export (bool) var pressed = false setget _set_pressed
export (bool) var oneshot = false
export (Texture) var pressed_texture setget _set_pressed_texture
export (Texture) var released_texture setget _set_released_texture

# set to true once the button is toggled once
var _toggled_once = false
var ready = false

func _ready():
	ready = true

func _set_pressed(_pressed: bool):
	pressed = _pressed
	_update_sprite()
		
func _set_pressed_texture(value: Texture):
	pressed_texture = value
	_update_sprite()
		
func _set_released_texture(value: Texture):
	released_texture = value
	_update_sprite()
	
func _update_sprite():
	if !Engine.editor_hint and not ready:
		yield(self, 'tree_entered')
	
	if pressed:
		$Sprite.texture = pressed_texture
	else:
		$Sprite.texture = released_texture

func toggle():
	# prevent button from being flipped again if its oneshotted
	if oneshot and _toggled_once:
		return
	
	# change button state
	pressed = not pressed
	_toggled_once = true
	
	# change the sprite based on the current state
	_update_sprite()
	
	if pressed:
		emit_signal("button_pressed")
	else:
		emit_signal("button_released")
