extends "res://items/Item.gd"

export (int) var health_amount

func collect(body: Node2D):
	if body.has_method("giveHealth"):
		body.giveHealth(health_amount)