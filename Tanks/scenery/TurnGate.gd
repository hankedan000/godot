tool
extends StaticBody2D

export (float) var swing_angle_degrees
export (bool) var open_cw = true
export (float) var duration = 2

# same for later
onready var _base_rotation_degrees = rotation_degrees
var _opened = false

func open():
	if not _opened:
		var i_angle = _base_rotation_degrees
		var f_angle = i_angle - swing_angle_degrees
		if open_cw:
			f_angle = i_angle + swing_angle_degrees
			
		$Tween.interpolate_property(
			self,"rotation_degrees",i_angle,f_angle,
			duration,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN_OUT)
		$Tween.start()

func _on_Tween_tween_all_completed():
	_opened = true
