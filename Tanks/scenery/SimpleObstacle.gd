tool
extends StaticBody2D

enum Items {barrelBlack_side, barrelBlack_top, barrelGreen_side, barrelGreen_top, barrelRed_side, barrelRed_top, barrelRust_side, barrelRust_top, barricadeMetal, barricadeWood, crateMetal, crateMetal_side, crateWood, crateWood_side, fenceRed, fenceYellow, oilSpill_large, oilSpill_small, sandbagBeige, sandbagBeige_open, sandbagBrown, sandbagBrown_open, specialBarrel1, specialBarrel1_outline, specialBarrel2, specialBarrel2_outline, specialBarrel3, specialBarrel3_outline, specialBarrel4, specialBarrel4_outline, specialBarrel5, specialBarrel5_outline, specialBarrel6, specialBarrel6_outline, specialBarrel7, specialBarrel7_outline, treeBrown_large, treeBrown_leaf, treeBrown_small, treeBrown_twigs, treeGreen_large, treeGreen_leaf, treeGreen_small, treeGreen_twigs, wireCrooked, wireStraight, }

export (Items) var type setget _update

var ready = false
var regions = {
	Items.barrelBlack_side: Rect2(150, 44, 28, 20),
	Items.barrelBlack_top: Rect2(150, 64, 24, 24),
	Items.barrelGreen_side: Rect2(359, 43, 28, 20),
	Items.barrelGreen_top: Rect2(366, 63, 24, 24),
	Items.barrelRed_side: Rect2(206, 43, 28, 20),
	Items.barrelRed_top: Rect2(342, 63, 24, 24),
	Items.barrelRust_side: Rect2(178, 43, 28, 20),
	Items.barrelRust_top: Rect2(318, 63, 24, 24),
	Items.barricadeMetal: Rect2(184, 63, 28, 28),
	Items.barricadeWood: Rect2(72, 61, 28, 28),
	Items.crateMetal: Rect2(100, 61, 28, 28),
	Items.crateMetal_side: Rect2(240, 61, 28, 28),
	Items.crateWood: Rect2(212, 63, 28, 28),
	Items.crateWood_side: Rect2(268, 61, 28, 28),
	Items.fenceRed: Rect2(294, 215, 16, 48),
	Items.fenceYellow: Rect2(228, 215, 16, 52),
	Items.oilSpill_large: Rect2(244, 213, 50, 50),
	Items.oilSpill_small: Rect2(22, 77, 14, 14),
	Items.sandbagBeige: Rect2(272, 181, 22, 32),
	Items.sandbagBeige_open: Rect2(216, 173, 28, 42),
	Items.sandbagBrown: Rect2(0, 59, 22, 32),
	Items.sandbagBrown_open: Rect2(244, 171, 28, 42),
	Items.specialBarrel1: Rect2(128, 48, 22, 14),
	Items.specialBarrel1_outline: Rect2(72, 43, 26, 18),
	Items.specialBarrel2: Rect2(366, 31, 24, 12),
	Items.specialBarrel2_outline: Rect2(262, 29, 28, 16),
	Items.specialBarrel3: Rect2(306, 7, 28, 10),
	Items.specialBarrel3_outline: Rect2(334, 29, 32, 14),
	Items.specialBarrel4: Rect2(274, 8, 32, 10),
	Items.specialBarrel4_outline: Rect2(188, 29, 36, 14),
	Items.specialBarrel5: Rect2(188, 17, 26, 12),
	Items.specialBarrel5_outline: Rect2(234, 45, 30, 16),
	Items.specialBarrel6: Rect2(26, 9, 26, 8),
	Items.specialBarrel6_outline: Rect2(0, 29, 30, 12),
	Items.specialBarrel7: Rect2(242, 9, 26, 8),
	Items.specialBarrel7_outline: Rect2(290, 30, 30, 12),
	Items.treeBrown_large: Rect2(64, 323, 64, 64),
	Items.treeBrown_leaf: Rect2(46, 205, 10, 8),
	Items.treeBrown_small: Rect2(354, 87, 36, 36),
	Items.treeBrown_twigs: Rect2(128, 62, 22, 26),
	Items.treeGreen_large: Rect2(0, 323, 64, 64),
	Items.treeGreen_leaf: Rect2(176, 275, 10, 8),
	Items.treeGreen_small: Rect2(318, 87, 36, 36),
	Items.treeGreen_twigs: Rect2(296, 63, 22, 26),
	Items.wireCrooked: Rect2(168, 171, 48, 44),
	Items.wireStraight: Rect2(0, 41, 70, 17),
}

func _ready():
	ready = true

func _update(_type):
	type = _type
	if !Engine.editor_hint and not ready:
		yield(self, 'ready')
	$Sprite.region_rect = regions[type]
	var rect = RectangleShape2D.new()
	rect.extents = $Sprite.region_rect.size / 2
	$CollisionShape2D.shape = rect

