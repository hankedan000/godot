extends Camera2D

var target = null

func _process(delta):
	if is_instance_valid(target):
		position = target.global_position