tool
extends Button

export (PackedScene) var map = null
export (Texture) var preview = null setget set_preview

var ready = false

func _ready():
	ready = true
	
func set_preview(value):
	preview = value
	
	if !Engine.editor_hint and not ready:
		yield(self, 'ready')
		
	$MarginContainer/VBoxContainer/HBoxContainer2/MapPreview/MarginContainer/PreviewImage.texture = value

func _on_MarginContainer_resized():
	# keep the map preview window square aspect ratio
	var map_preview = $MarginContainer/VBoxContainer/HBoxContainer2/MapPreview
	var img_size = map_preview.rect_size
	var edge_size = min(img_size.x,img_size.y)
	map_preview.rect_size = Vector2(edge_size,edge_size)


func _on_LevelButton_pressed():
	print_debug("pressed")
