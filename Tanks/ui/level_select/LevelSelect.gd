extends MarginContainer

func _ready():
	if $Margin/Scroll/Grid.get_child_count() > 0:
		$Margin/Scroll/Grid.get_child(0).grab_focus()