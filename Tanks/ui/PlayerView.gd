extends ViewportContainer

func _ready():
	# FIXME: It seems like Godot has a bug where the 2nd Viewport doesn't get
	# its size set even though the ViewportContainer has stretch set true.
	$Viewport.size = get_rect().size

func bind_tile_map(tile_map: TileMap):
	_set_camera_limits(tile_map)
	$HUD/VBoxContainer/Container/MiniMap.tile_map = tile_map
	
func bind_player_to_hud(player):
	player.connect("health_changed",$HUD,"update_healthbar")
	
func bind_target(target):
	$Viewport/Camera.target = target
	
func _set_camera_limits(tile_map: TileMap):
	# restrict players camera to only inside the map's tile space
	var mapLimits = tile_map.get_used_rect()
	var mapCellSize = tile_map.cell_size
	$Viewport/Camera.limit_left = mapLimits.position.x * mapCellSize.x
	$Viewport/Camera.limit_right = mapLimits.end.x * mapCellSize.x
	$Viewport/Camera.limit_top = mapLimits.position.y * mapCellSize.y
	$Viewport/Camera.limit_bottom = mapLimits.end.y * mapCellSize.y