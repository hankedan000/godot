extends MarginContainer

var GREEN_BAR = preload("res://assets/UI/barHorizontal_green_mid 200.png")
var YELLOW_BAR = preload("res://assets/UI/barHorizontal_yellow_mid 200.png")
var RED_BAR = preload("res://assets/UI/barHorizontal_red_mid 200.png")

func _ready():
	$VBoxContainer/Inventory.clear_inventory()

func update_healthbar(percent_remaining):
	# set the bar color based on percent remaining
	var texProgress = GREEN_BAR
	if percent_remaining <= 30:
		texProgress = RED_BAR
	elif percent_remaining <= 60:
		texProgress = YELLOW_BAR
	$VBoxContainer/Container/HealthBar.texture_progress = texProgress
	
	# give the health bar a "sliding" effect
	$Tween.interpolate_property($VBoxContainer/Container/HealthBar,
		"value",
		$VBoxContainer/Container/HealthBar.value,
		percent_remaining,
		0.1,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	$Tween.start()
	
	
