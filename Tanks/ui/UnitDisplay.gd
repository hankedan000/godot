extends Node2D

var GREEN_BAR = preload("res://assets/UI/barHorizontal_green_mid 200.png")
var YELLOW_BAR = preload("res://assets/UI/barHorizontal_yellow_mid 200.png")
var RED_BAR = preload("res://assets/UI/barHorizontal_red_mid 200.png")

func _ready():
	for child in get_children():
		child.hide()

func _process(delta):
	global_rotation = 0

func update_healthbar(percent_remaining):
	# only show health when it's not at 100%
	if percent_remaining < 100:
		$HealthBar.show()
	
	# set the bar color based on percent remaining
	$HealthBar.texture_progress = GREEN_BAR
	if percent_remaining <= 30:
		$HealthBar.texture_progress = RED_BAR
	elif percent_remaining <= 60:
		$HealthBar.texture_progress = YELLOW_BAR
	
	$HealthBar.value = percent_remaining
