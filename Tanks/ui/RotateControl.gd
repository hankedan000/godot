extends Control

func _ready():
	for child in get_children():
		child.set_rotation(PI)
		child.rect_position = get_rect().size
	