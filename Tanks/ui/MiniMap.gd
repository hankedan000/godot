extends TextureRect

export (int) var tile_size = 10
export (float) var refresh_rate = 2
export (float) var background_alpha = 0.4

var tile_map: TileMap setget _set_tile_map
var ground_rect = Rect2()
var units_to_display = []

func _ready():
	$Refresh.wait_time = 1.0 / refresh_rate

func _set_tile_map(value: TileMap):
	tile_map = value
	ground_rect = tile_map.get_used_rect()
	ground_rect.size *= tile_size
	
	# manually trigger a refresh
	_on_Refresh_timeout()
	# schedule redraw timer
	$Refresh.start()

func _draw():
	draw_rect(ground_rect,Color(0,1,0,background_alpha),true)
	for unit in units_to_display:
		draw_rect(
			Rect2(unit["position"].x,unit["position"].y,tile_size,tile_size),
			unit["color"],
			true)

func _on_Refresh_timeout():
	var trackables = get_tree().get_nodes_in_group("trackables")
	units_to_display.clear()
	for trackable in trackables:
		var world_pos = trackable.global_position
		var map_pos = tile_map.world_to_map(world_pos) * tile_size
		
		# only draw the unit if it's within bounds of the map
		if ground_rect.has_point(map_pos):
			var unit = Dictionary()
			unit["position"] = map_pos
			unit["color"] = trackable.minimap_color
			
			units_to_display.append(unit)
			
	# request a redraw
	update()
