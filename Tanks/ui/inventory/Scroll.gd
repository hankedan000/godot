tool
extends ScrollContainer

export (int) var selected_item setget _set_selected_item

var ready = false

func _ready():
	ready = true

func get_selected_item():
	return $Container.get_child(selected_item)
	
func item_count():
	return $Container.get_child_count()
	
func is_valid_item(index: int):
	return item_count() > 0 and index >= 0 and index < item_count()
	
func get_item(index: int):
	var item = null
	if is_valid_item(index):
		item = $Container.get_child(index)
	return item
	
func set_selected_item_by_name(item_name: String):
	# search item in the inventory based on the name
	var found_item = false
	var index = 0
	while index < item_count():
		var item = get_item(index)
		if item.item_name == item_name:
			found_item = true
			break
		index += 1
	
	if found_item:
		_set_selected_item(index)

func _set_selected_item(value: int):
	if !Engine.editor_hint and not ready:
		yield(self, 'tree_entered')
		
	if is_valid_item(value):
		# deactive current item
		if get_selected_item():
			get_selected_item().pressed = false
		
		selected_item = value
		
		# start the sliding animation
		var items = $Container.get_children()
		$Tween.stop_all()
		$Tween.interpolate_property(
			self,"scroll_vertical",scroll_vertical,items[value].rect_position.y,
			0.1,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN)
		$Tween.start()
		
func _on_Tween_tween_all_completed():
	var item = get_selected_item()
	if item:
		item.pressed = true
