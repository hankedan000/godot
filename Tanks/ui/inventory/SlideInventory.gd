tool
extends Control

enum SelectorAlign {TOP,MIDDLE,BOTTOM}

export (SelectorAlign) var selector_alignment setget _set_selector_alignment

var ready = false

func _ready():
	ready = true

func add_item(item: InventoryItem):
	$Selection/Scroll/Container.add_child(item)
	
func set_selected_item_by_name(item_name: String):
	$Selection/Scroll.set_selected_item_by_name(item_name)

func clear_inventory():
	for child in $Selection/Scroll/Container.get_children():
		child.queue_free()

func _set_selector_alignment(value):
	selector_alignment = value
	if !Engine.editor_hint and not ready:
		yield(self, 'ready')
	
	var inventory_h = rect_size.y
	var selector_h = $Selection.rect_size.y
	
	match (value):
		SelectorAlign.TOP:
			$Selection.rect_position.y = 0
			
		SelectorAlign.MIDDLE:
			$Selection.rect_position.y = inventory_h / 2 - selector_h / 2
			
		SelectorAlign.BOTTOM:
			$Selection.rect_position.y = inventory_h - selector_h

func _on_SlideInventory_resized():
	_set_selector_alignment(selector_alignment)
