tool
extends TextureButton
class_name InventoryItem

export (int) var item_count = 0
export (String) var item_name
export (PackedScene) var Item setget _set_Item

var ready = false

func _ready():
	_set_sprite_from_item()
	ready = true
			
func _set_Item(value: PackedScene):
	Item = value
	_set_sprite_from_item()
			
func _set_sprite_from_item():
	if $Sprite.texture == null and Item:
		# attempt to load sprite texture from Item scene
		var item_instance = Item.instance()
		if item_instance.has_node("Sprite"):
			var item_sprite = item_instance.get_node("Sprite")
			$Sprite.texture = item_sprite.texture
			$Sprite.region_enabled = item_sprite.region_enabled
			$Sprite.region_rect = item_sprite.region_rect
			$Sprite.rotation = $Sprite.rotation + item_sprite.rotation

func _on_InventoryItem_resized():
	if !Engine.editor_hint and not ready:
		yield(self, 'ready')
	
	var w = rect_size.x
	var h = rect_size.y
	
	$Sprite.position.x = w / 2
	$Sprite.position.y = h / 2
