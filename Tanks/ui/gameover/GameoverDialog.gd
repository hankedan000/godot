extends Popup

signal restart
signal back

func _on_Restart_pressed():
	emit_signal("restart")
	hide()

func _on_Back_pressed():
	emit_signal("back")
	hide()

func _on_GameoverDialog_about_to_show():
	$Margin/VBoxContainer/GridContainer/Restart.grab_focus()
