extends Node
class_name ArcadeInput

export var isPlayer1 = true

signal dir_pressed(dir)

const P1_ACTIONS = {"u": "ui_up",
	                "d": "ui_down",
					"l": "ui_left",
					"r": "ui_right",
					"b": "ui_accept",
					"1": "p1_start",
					"2": "p2_start"}

const P2_ACTIONS = {"u": "p2_up",
					"d": "p2_down",
					"l": "p2_left",
					"r": "p2_right",
					"b": "p2_button"}
					
func _ready():
	pass

func _input(event):
	var DIRS = ['u','d','l','r']
	
	for dir in DIRS:
		if event.is_action_pressed(getActionName(dir)):
			emit_signal("dir_pressed",dir)

func getActionName(controlChar):
	if isPlayer1:
		return P1_ACTIONS[controlChar]
	else:
		return P2_ACTIONS[controlChar]
		