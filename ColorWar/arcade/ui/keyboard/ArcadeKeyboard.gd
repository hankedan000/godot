extends Panel

export var isPlayer1 = true

signal char_entered(c)
signal rub
signal end

func _ready():
	$ArcadeInput.isPlayer1 = isPlayer1
	self.connect("visibility_changed",self,"_onVisibiltyChanged")
	_onVisibiltyChanged()
	
	for button in $keyGrid.get_children():
		button.connect("pressed",self,"_onKeyPressed",[button.name])

func _onVisibiltyChanged():
	if visible:
		$keyGrid/a.grab_focus()
		
func _onKeyPressed(name: String):
	match(name):
		"period":
			emit_signal("char_entered",'.')
		"hyphen":
			emit_signal("char_entered",'-')
		"rub":
			emit_signal("rub")
		"end":
			emit_signal("end")
		_:
			emit_signal("char_entered",name)

func _on_ArcadeInput_dir_pressed(dir):
	if visible:
		var nextFocusPath = null
		match(dir):
			'u': nextFocusPath = get_focus_owner().focus_neighbour_top
			'd': nextFocusPath = get_focus_owner().focus_neighbour_bottom
			'l': nextFocusPath = get_focus_owner().focus_neighbour_left
			'r': nextFocusPath = get_focus_owner().focus_neighbour_right
		print_debug(get_focus_owner())
