tool
extends ColorRect

export (PackedScene) var SpriteScene = null
export var scale = 1.0
export var max_sprites = 20
export var min_fall_speed = 5
export var max_fall_speed = 20
export var min_rotation_speed_degrees = 1
export var max_rotation_speed_degrees = 10

var _max_sprite_dim = 0
var _half_max_sprite_dim = 0

func _process(delta):
	for c in $MySprites.get_children():
		c.position += Vector2(0,c.fall_speed) * delta
		c.rotation_degrees += c.rot_speed * delta
		
		# see if sprite is falling off the bottom of the screen
		if (c.position.y - rect_size.y) > _half_max_sprite_dim:
			c.position.x = rand_range(0,rect_size.x)
			c.position.y = - _half_max_sprite_dim

func _on_FallingCones_resized():
	if SpriteScene == null:
		# no sprites to spawn
		return
	if rect_size.length() < 10:
		# wait till control is resized properly
		return
	if $MySprites.get_child_count() > 0:
		# already placed children
		return
	
	for i in range(max_sprites):
		var new_sprite = SpriteScene.instance()
		
		var sprite_rect = new_sprite.get_rect()
		_max_sprite_dim = sprite_rect.size.length() * scale
		_half_max_sprite_dim = _max_sprite_dim / 2.0
		
		new_sprite.fall_speed = rand_range(min_fall_speed,max_fall_speed)
		var dir = 1 if (randi() % 2) == 0 else -1
		new_sprite.rot_speed = rand_range(min_rotation_speed_degrees,max_rotation_speed_degrees) * dir
		new_sprite.position.x = rand_range(0,rect_size.x)
		new_sprite.position.y = rand_range(-_half_max_sprite_dim,rect_size.y)
		new_sprite.rotation_degrees = rand_range(0,360)
		new_sprite.scale = Vector2(1,1) * scale
		$MySprites.add_child(new_sprite)
