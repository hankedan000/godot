extends PanelContainer

signal single_player_selected()
signal multi_player_selected()

func _on_SinglePlayer_pressed():
	emit_signal("single_player_selected")

func _on_MultiPlayer_pressed():
	emit_signal("multi_player_selected")
