extends PanelContainer

signal quit_selected()
signal next_selected()

func set_course_clear(drive_time, cone_count):
	var label_text = '%0.3fs' % drive_time
	if cone_count > 0:
		label_text += ' +%d' % cone_count
	$MarginContainer/VBoxContainer/CourseClearLabel.show()
	$MarginContainer/VBoxContainer/OffCourseLabel.hide()
	$MarginContainer/VBoxContainer/TimeLabel.show()
	$MarginContainer/VBoxContainer/TimeLabel.text = label_text

func set_off_course():
	$MarginContainer/VBoxContainer/CourseClearLabel.hide()
	$MarginContainer/VBoxContainer/OffCourseLabel.show()
	$MarginContainer/VBoxContainer/TimeLabel.hide()
	$MarginContainer/VBoxContainer/TimeLabel.text = ""

func _on_Quit_pressed():
	emit_signal("quit_selected")

func _on_Next_pressed():
	emit_signal("next_selected")
