extends Node
class_name Types

enum ConeType {
	Upright,
	Pointer
}

enum ConeColor {
	Orange,
	Yellow,
	Green
}
