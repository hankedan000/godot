extends Node

const CACHE_FILE = "user://cache.json"

var _cache = {}

func _ready():
	get_tree().root.connect("tree_exiting",self,"_on_tree_exited")
	_restore_cache()

func put_var(key: String, value):
	_cache[key] = value

func get_var(key: String, default=null):
	if _cache.has(key):
		return _cache[key]
	else:
		return default

func _restore_cache():
	var file = File.new()
	if file.open(CACHE_FILE,File.READ) == OK:
		var cache_str = file.get_as_text()
		var cache_json = JSON.parse(cache_str)
		if cache_json.error == OK:
			_cache = cache_json.result
		file.close()
	print("restored cache")
	
func _save_cache():
	var file = File.new()
	if file.open(CACHE_FILE,File.WRITE) == OK:
		var cache_str = JSON.print(_cache,"  ")
		file.store_string(cache_str)
		file.close()
	print("saved cache")

func _on_tree_exited():
	_save_cache()
