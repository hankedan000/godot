extends Node
class_name ClassUtils

static func instance_user_class(user_class_name):
	var proj_classes = ProjectSettings.get_setting("_global_script_classes")
	for p_class in proj_classes:
		if p_class.class == user_class_name:
			return load(p_class.path).new()
	return null
