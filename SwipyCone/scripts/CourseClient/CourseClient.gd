extends Node

signal request_failed()
signal request_timeout()
signal request_canceled()
signal course_get_completed(courses)
signal course_post_completed(id)

export var timeout = 5 setget _set_timeout

onready var http_request := $HTTPRequest

const COURSE_ENDPOINT := "/course"

enum RequestType {
	None, CourseGet, CourseInsert
}

var _host = null
var _pending_request_type = RequestType.None

func _set_timeout(value):
	timeout = value
	http_request.timeout = timeout

# example: "https://127.0.0.1:3000"
func set_host(host):
	_host = host

func request_courses(start_id, n_courses):
	if _host == null:
		push_error("host is null. call set_host()")
		return
	
	var url = _host + COURSE_ENDPOINT
	url += '?start_id=%d' % start_id
	url += '&n_courses=%d' % n_courses
	cancel_request()
	var error = http_request.request(url,[],false,HTTPClient.METHOD_GET,"")
	if error == OK:
		_pending_request_type = RequestType.CourseGet
	else:
		push_error('course server request failed! error = %s' % error)

func post_course(course_size, ground_color, obstacles, name = null, designer = null):
	if _host == null:
		push_error("host is null. call set_host()")
		return
	
	var url = _host + COURSE_ENDPOINT
	url += '?course_size=%s' % JSON.print(course_size).http_escape()
	url += '&ground_color=%s' % str(ground_color).http_escape()
	url += '&obstacles=%s' % JSON.print(obstacles).http_escape()
	if name is String:
		url += '&name=%s' % name.http_escape()
	if designer is String:
		url += '&designer=%s' % designer.http_escape()
	cancel_request()
	var error = http_request.request(url,[],false,HTTPClient.METHOD_POST,"")
	if error == OK:
		_pending_request_type = RequestType.CourseInsert
	else:
		push_error('course server request failed! error = %s' % error)

func cancel_request():
	if _pending_request_type != RequestType.None:
		http_request.cancel_request()
		_pending_request_type = RequestType.None
		emit_signal("request_canceled")

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	if result == HTTPRequest.RESULT_SUCCESS:
		if response_code != 200:
			print('Uncaught repsonse_code %d' % response_code)
			emit_signal("request_failed")
			_pending_request_type = RequestType.None
			return
		
		var body_str = body.get_string_from_utf8()
		var json_res = JSON.parse(body_str)
		if json_res.error != OK:
			print("failed to parse JSON from body_str: '%s'" % body_str)
			emit_signal("request_failed")
			_pending_request_type = RequestType.None
			return
		
		match _pending_request_type:
			RequestType.CourseGet:
				var courses = json_res.result
				if not courses is Array or courses.size() < 1:
					print("courses is not array or empty: %s" % courses)
					emit_signal("request_failed")
				emit_signal("course_get_completed",courses)
			RequestType.CourseInsert:
				emit_signal("course_post_completed",json_res.result['id'])
			_:
				push_warning("Unhandled request_completed for type %s" % _pending_request_type)
	elif result == HTTPRequest.RESULT_TIMEOUT:
		emit_signal("request_timeout")
	else:
		print("Uncaught HTTPRequest results code %d" % result)
		emit_signal("request_failed")
	
	_pending_request_type = RequestType.None
