extends Node
class_name FileUtils

static func list_files_in_dir(path):
	var files = []
	var dir := Directory.new()
	dir.open(path)
	dir.list_dir_begin()

	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(dir.get_current_dir() + '/' + file)
			print(files[files.size() - 1])

	dir.list_dir_end()

	return files
