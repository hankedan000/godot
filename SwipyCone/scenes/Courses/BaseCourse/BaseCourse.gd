extends ColorRect
class_name BaseCourse

signal player_start()
signal player_stop()
signal course_start()
signal course_cleared(drive_time, cone_count)
signal off_course()
signal course_invalid(warnings, errors)

# all PhysicsCones that are defined in the course (includes those within Element)
var _all_p_cones = []
# PhysicsCones that are completly standalone (ie. not within an Element)
var _all_stand_alone_p_cones = []
# all BaseElements that are defined in the course
var _all_elements = []

# every course needs a start & finish gate
var _start_gate = null
var _finish_gate = null

# populated by _discover_course() and used for validation
var _course_warnings = []
var _course_errors = []

# all PhysicsCones in the course. sorted based on clear_order property.
var _p_cones_by_order = []
class PhysicsConeSortByClearOrder:
	static func sort_ascending(a, b):
		if a.clear_order < b.clear_order:
			return true
		return false

var _drive_time = 0
var _cones_hit = 0
var _curr_cleared_cone = -1
var _next_clear_order = null
var _on_course = false

const CONE_PENALTY = 2.0# seconds
const DEBUG = false

func _ready():
	_discover_course()
	_setup_camera()
	_setup_reveals()
	_p_cones_by_order.clear()
	_p_cones_by_order.append_array(_all_p_cones)
	_p_cones_by_order.sort_custom(PhysicsConeSortByClearOrder,'sort_ascending')
	
	# connect all p_cone signals
	for p_cone in _p_cones_by_order:
		p_cone.connect("cleared",self,"_on_cone_cleared")
		p_cone.connect("hit",self,"_on_cone_hit")
		p_cone.connect("off_course",self,"_on_cone_off_course")
	
	reset_course()
	
func _input(event):
	# ignore all inputs besides mouse
	if not event is InputEventMouse and not event is InputEventMouseMotion:
		return
	
	# figure out where mouse is located within the world
	# https://www.reddit.com/r/godot/comments/gj4yev/convert_eventposition_mouse_to_local_position_in/
	var viewport = get_camera().get_viewport()
	var mouse_pos_in_world = viewport.canvas_transform.affine_inverse().xform(event.position)
	
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed && ! player_driving():
				reset_course()
				
				# make sure player position is set before enable collision detection
				get_player().global_position = mouse_pos_in_world
				_set_player_collision(true)
			elif ! event.pressed && player_driving():
				_set_player_collision(false)
				if player_on_course():
					_off_course()
	elif event is InputEventMouse:
		if player_driving():
			get_player().global_position = mouse_pos_in_world
		if player_on_course():
			get_race_line().add_point(mouse_pos_in_world)

func _process(delta):
	if player_on_course():
		_drive_time += delta

func _discover_course():
	_all_p_cones.clear()
	_all_stand_alone_p_cones.clear()
	_all_elements.clear()
	_start_gate = null
	_finish_gate = null
	_course_warnings.clear()
	_course_errors.clear()
	
	_discover_cones($Cones)
	
	if _start_gate == null:
		_course_errors.push_back("No start gate")
	if _finish_gate == null:
		_course_errors.push_back("No finish gate")

func _discover_cones(node):
	if node is PhysicsCone:
		_all_p_cones.append(node)
		_all_stand_alone_p_cones.append(node)
	elif node is BaseElement:
		_all_p_cones.append_array(node.get_cones())
		_all_elements.append(node)
		if node is StartFinishGate:
			if node.is_start:
				if _start_gate == null:
					_start_gate = node
				else:
					_course_warnings.push_back("Found duplicate start gate")
			else:
				if _finish_gate == null:
					_finish_gate = node
				else:
					_course_warnings.push_back("Found duplicate finish gate")
	else:
		for c in node.get_children():
			_discover_cones(c)
			
func _setup_camera():
	get_camera().position = rect_size / 2.0
	
	# determine if we need to rotate the camera to best fit the course
	var is_course_portrait = rect_size.y > rect_size.x
	var view_size = get_camera().get_viewport().get_visible_rect().size
	var is_viewport_portrait = view_size.y > view_size.x
	if is_course_portrait != is_viewport_portrait:
		get_camera().rotation_degrees = -90
		view_size = view_size.rotated(PI/2)
		
	var course_ratio = rect_size.y / rect_size.x
	var view_ratio = view_size.y / view_size.x
	var zoom = 1.0
	if course_ratio > view_ratio:
		# screen wider than course; use height to set zoom
		zoom = rect_size.y / view_size.y
	else:
		# screen taller than course, use width to set zoom
		zoom = rect_size.x / view_size.x
	get_camera().zoom = Vector2(zoom,zoom)

func _setup_reveals():
	get_start_reveal().hide()
	if get_start_gate() != null:
		get_start_reveal().global_position = get_start_gate().global_position
		get_start_reveal().show()
	get_finish_reveal().hide()
	if get_finish_gate() != null:
		get_finish_reveal().global_position = get_finish_gate().global_position
		get_finish_reveal().show()

# determines the next 'clear_order' given the current cone that the player has
# cleared
func _get_next_clear_order(start_idx, curr_clear_order):
	if _p_cones_by_order.size() > 0:
		if start_idx < 0:
			return _p_cones_by_order[0].clear_order
		elif curr_clear_order == null:
			return null
		else:
			for i in range(start_idx+1,_p_cones_by_order.size()):
				if _p_cones_by_order[i].clear_order > curr_clear_order:
					return _p_cones_by_order[i].clear_order
			
			# never found a next cone to clear. must be at the end of the course
			return null
	else:
		return null

func _setup_next_clear_order():
	for p_cone in _p_cones_by_order:
		# only enable physics on cones that are in the next clear_order
		p_cone.disabled = p_cone.clear_order != _next_clear_order

func reset_course():
	get_race_line().clear_points()
	_set_player_collision(false)
	_drive_time = 0
	_cones_hit = 0
	_curr_cleared_cone = -1
	_next_clear_order = _get_next_clear_order(_curr_cleared_cone,_next_clear_order)
	_on_course = false
	_setup_next_clear_order()
	
func show_reveal():
	$Reveals.show()
	
func hide_reveal():
	$Reveals.hide()
	
func get_race_line():
	return $RaceLine

func get_player():
	return $Player
	
func get_camera():
	return $Camera
	
func get_start_gate():
	return _start_gate
	
func get_finish_gate():
	return _finish_gate
	
func get_start_reveal():
	return $Reveals/Viewport/StartReveal
	
func get_finish_reveal():
	return $Reveals/Viewport/FinishReveal

func player_on_course():
	return _on_course

func player_driving():
	return get_player().monitorable
	
func get_drive_time():
	return _drive_time
	
func to_dict():
	var data = {
		'obstacles' : [],
		'ground_color' : color.to_html(true),
		'course_size' : {'x' : rect_size.x, 'y' : rect_size.y}
	}
	for p_cone in _all_stand_alone_p_cones:
		data.obstacles.append({
			'scene_res' : p_cone.filename,
			'data' : p_cone.to_dict()
		})
	for element in _all_elements:
		data.obstacles.append({
			'scene_res' : element.filename,
			'data' : element.to_dict()
		})
	return data
	
func from_dict(data):
	self.color = Color(data.ground_color)
	self.rect_size.x = data.course_size.x
	self.rect_size.y = data.course_size.y
	for c in $Cones.get_children():
		$Cones.remove_child(c)
	for obstacle in data.obstacles:
		var obs_inst = ResourceLoader.load(obstacle.scene_res).instance()
		if obs_inst != null:
			obs_inst.from_dict(obstacle.data)
			$Cones.add_child(obs_inst)
		else:
			printerr('Failed to instance scene from "%s"' % obstacle.scene_res)
	
func save_to_file(filepath):
	var data = to_dict()
	var file = File.new()
	if OK == file.open(filepath, File.WRITE):
		file.store_string(JSON.print(data,"  ",true))
		file.close()
	
func restore_from_file(filepath):
	var file = File.new()
	if OK == file.open(filepath, File.READ):
		var json_str = file.get_as_text()
		file.close()
		var json_res = JSON.parse(json_str)
		if json_res.error == OK:
			from_dict(json_res.result)
		else:
			printerr('failed to parse course JSON file "%s"' % filepath)
	else:
		printerr('failed to open course file "%s"' % filepath)

func validate_course():
	_discover_course()
	if _course_errors.size() > 0:
		emit_signal("course_invalid",_course_warnings,_course_errors)
	return _course_errors.size() == 0

func _set_player_collision(enabled):
	get_player().set_deferred("monitorable",enabled)
	if enabled:
		get_player().start_tail()
		emit_signal("player_start")
	else:
		get_player().stop_tail()
		emit_signal("player_stop")

func _start_course():
	_on_course = true
	emit_signal("course_start")

func _finish_course():
	_set_player_collision(false)
	_on_course = false
	emit_signal("course_cleared",_drive_time,_cones_hit)

func _off_course():
	_set_player_collision(false)
	_on_course = false
	emit_signal("off_course")

func _process_cleared_cone(p_cone):
	_curr_cleared_cone = _p_cones_by_order.find(p_cone)
	_next_clear_order = _get_next_clear_order(_curr_cleared_cone,_next_clear_order)
	if DEBUG:
		print('_p_cones_by_order = %s' % [_p_cones_by_order])
		print('_curr_cleared_cone = %s' % [_curr_cleared_cone])
		print('_next_clear_order = %s' % [_next_clear_order])
	_setup_next_clear_order()
	
	var parent_element = p_cone.get_parent_element()
	if parent_element is StartFinishGate and parent_element.is_start:
		_start_course()
	elif parent_element is StartFinishGate and ! parent_element.is_start:
		var kill_count_str = ""
		if _cones_hit > 0:
			kill_count_str = "+" + str(_cones_hit)
		print("COURSE CLEARED! _drive_time = %0.3f%s" % [_drive_time,kill_count_str])
		_finish_course()

func _on_cone_hit(p_cone):
	if DEBUG:
		print('_on_cone_hit - %s' % [p_cone])
	
	# disable cone so we don't potentially trigger it's off-course signal
	p_cone.disabled = true
	
	var parent_element = p_cone.get_parent_element()
	if parent_element is StartFinishGate:
		# hitting finish cones is concidered off course
		_off_course()
	else:
		_cones_hit += 1
		_process_cleared_cone(p_cone)
	
func _on_cone_cleared(p_cone):
	if DEBUG:
		print('_on_cone_cleared() - %s' % [p_cone])
		
	_process_cleared_cone(p_cone)

func _on_cone_off_course(p_cone):
	if DEBUG:
		print('_on_cone_off_course - %s' % [p_cone])
	
	_off_course()
