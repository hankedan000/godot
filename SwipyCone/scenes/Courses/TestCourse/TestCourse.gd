extends BaseCourse

func _ready():
	save_to_file('/tmp/course.json')
	
	if self.validate_course():
		print('course valid')

func _on_TestCourse_course_invalid(warnings, errors):
	print('course invalid!!!')
	if warnings.size() > 0:
		print('Warnings:')
		for warning in warnings:
			print(' * %s' % warning)
	if errors.size() > 0:
		print('Errors:')
		for error in errors:
			print(' * %s' % error)
