extends PanelContainer

onready var file_menu_button := $VBoxContainer/MenuBar/FileMenuButton
onready var course_view := $VBoxContainer/ViewportContainer/Viewport
onready var course := $VBoxContainer/ViewportContainer/Viewport/Course
onready var server_edit := $VBoxContainer/ToolBar/ServerEdit
onready var course_id_edit := $VBoxContainer/ToolBar/CourseIdEdit
onready var course_client := $CourseClient
onready var file_dialog := $FileDialog

const FILE_MENU_IMPORT = 0
const FILE_MENU_EXPORT = 1

var DEFAULT_IMPORT_DIR := OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP)
var DEFAULT_EXPORT_DIR := OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP)
const CACHE_KEY_PREV_IMPORT_DIR := "CourseEditor.prev_import_dir"
const CACHE_KEY_PREV_EXPORT_DIR := "CourseEditor.prev_export_dir"

# true if importing a file, false otherwise
var _file_import = false

func _ready():
	file_menu_button.get_popup().add_item("Import from file", FILE_MENU_IMPORT)
	file_menu_button.get_popup().add_item("Export to file", FILE_MENU_EXPORT)
	file_menu_button.get_popup().connect("id_pressed",self,"_on_FileMenu_id_pressed")

func _show_import_dialog():
	course_view.gui_disable_input = true
	file_dialog.mode = FileDialog.MODE_OPEN_FILE
	file_dialog.window_title = "Import from file"
	file_dialog.popup_exclusive = true
	file_dialog.current_dir = Cache.get_var(CACHE_KEY_PREV_IMPORT_DIR,DEFAULT_IMPORT_DIR)
	file_dialog.popup_centered_clamped(rect_size/2)

func _show_export_dialog():
	course_view.gui_disable_input = true
	file_dialog.mode = FileDialog.MODE_SAVE_FILE
	file_dialog.window_title = "Export to file"
	file_dialog.popup_exclusive = true
	file_dialog.current_dir = Cache.get_var(CACHE_KEY_PREV_EXPORT_DIR,DEFAULT_EXPORT_DIR)
	file_dialog.popup_centered_clamped(rect_size/2)

func _on_GetCourse_pressed():
	var start_id = int(course_id_edit.text)
	course_client.set_host(server_edit.text)
	course_client.request_courses(start_id,1)

func _on_InsertCourse_pressed():
	var course_dict = course.to_dict()
	course_client.set_host(server_edit.text)
	course_client.post_course(
		course_dict.course_size,
		course_dict.ground_color,
		course_dict.obstacles,
		null,# course name
		null)# designer name

func _on_FileMenu_id_pressed(id):
	match id:
		FILE_MENU_IMPORT:
			_file_import = true
			_show_import_dialog()
		FILE_MENU_EXPORT:
			_file_import = false
			_show_export_dialog()
		_:
			push_warning("Unsupported file menu id %d" % id)

func _on_FileDialog_file_selected(path):
	if _file_import:
		Cache.put_var(CACHE_KEY_PREV_IMPORT_DIR,path.get_base_dir())
		course.restore_from_file(path)
		course._ready()
	else:
		Cache.put_var(CACHE_KEY_PREV_EXPORT_DIR,path.get_base_dir())
		course.save_to_file(path)

func _on_CourseClient_course_get_completed(courses):
	if courses.size() >= 1:
		course.from_dict(courses[0])

func _on_CourseClient_course_post_completed(id):
	print('course posted to server with id %d' % id)


func _on_ServerEdit_text_entered(new_text):
	print(new_text)
