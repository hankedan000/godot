extends BaseCourse

var _clear_count = 0

func _on_StandingCone_cleared(p_cone):
	_clear_count += 1
	$ClearCount.text = str(_clear_count)
