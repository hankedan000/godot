tool
extends Sprite
class_name ConeSprite

export (Types.ConeType) var type = Types.ConeType.Upright setget _set_type
export (Types.ConeColor) var color = Types.ConeColor.Orange setget _set_color

func _ready():
	_update_region()
	
func _set_type(value):
	type = value
	_update_region()

func _set_color(value):
	color = value
	_update_region()
	
const SPRITE_GRID := 64
func _update_region():
	region_enabled = true
	region_rect = Rect2(
		int(type) * SPRITE_GRID, int(color) * SPRITE_GRID,
		SPRITE_GRID, SPRITE_GRID)
