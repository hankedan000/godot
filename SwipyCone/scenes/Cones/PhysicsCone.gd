extends Node2D
class_name PhysicsCone

signal cleared(p_cone)# fired when the player successfully navigated the cone
signal off_course(p_cone)# fired when player goes off course on this cone
signal hit(p_cone)# fired when player hits the cone

export (bool) var disabled := false setget _set_disabled

# order in which the cone needs to be cleared from the course.
# lower indexed cones must be cleared before higher indexed cones; else, the
# player will be off-course.
export var clear_order : int = 0

var _parent_element : BaseElement = null

func _ready():
	var n = self.get_parent()
	while n != null:# and n != get_tree().root:
		if n is BaseElement:
			_parent_element = n
			break
		n = n.get_parent()
		
func get_parent_element():
	return _parent_element

func _set_disabled(value):
	disabled = value
	
func to_dict():
	return {
		'clear_order' : clear_order,
		'position' : {'x' : position.x, 'y' : position.y},
		'rotation' : rotation
	}
	
func from_dict(data):
	clear_order = data.clear_order
	position.x = data.position.x
	position.y = data.position.y
	rotation = data.rotation
