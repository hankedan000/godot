tool
extends PhysicsCone
class_name StandingCone

export var clear_dist := 400.0 setget _set_clear_dist
export var clear_width := 5.0

export var off_course_disabled := false setget _set_off_course_disabled
export var off_course_dist := 400.0 setget _set_off_course_dist
export var off_course_width := 5.0

export (int,0,3) var pointer_count := 0 setget _set_pointer_count
export (Types.ConeColor) var color = Types.ConeColor.Orange setget _set_color

var _ready_called = false

func _ready():
	# duplicate collision shapes or else they will be shared between cones
	$ClearArea/Shape.shape = $ClearArea/Shape.shape.duplicate()
	$HitArea/Shape.shape = $HitArea/Shape.shape.duplicate()
	$OffArea/Shape.shape = $OffArea/Shape.shape.duplicate()
	if ! Engine.editor_hint:
		$EditorLabels.queue_free()# no need to keep these around in real game
	else:
		$EditorLabels.visible = true
	_ready_called = true
	
func get_user_class():
	return "StandingCone"
	
func to_dict():
	var data = .to_dict()
	data['clear_dist'] = clear_dist
	data['clear_width'] = clear_width
	data['off_course_disabled'] = off_course_disabled
	data['off_course_dist'] = off_course_dist
	data['off_course_width'] = off_course_width
	data['pointer_count'] = pointer_count
	data['color'] = Types.ConeColor.keys()[int(color)]
	return data
	
func from_dict(data):
	.from_dict(data)
	self.clear_dist = data.clear_dist
	self.clear_width = data.clear_width
	self.off_course_disabled = data.off_course_disabled
	self.off_course_dist = data.off_course_dist
	self.off_course_width = data.off_course_width
	self.pointer_count = data.pointer_count
	self.color = Types.ConeColor.get(data.color)
	
func _process(_delta):
	if Engine.editor_hint:
		_update_editor_label()
		
func _update_editor_label():
	$EditorLabels.global_rotation_degrees = 0
	$EditorLabels/OrderLabel.text = str(clear_order)
	$EditorLabels/OrderLabel.set_global_position($EditorLabels.global_position + Vector2(20,-30))

# override PhysicsCone._set_disabled()
func _set_disabled(value):
	disabled = value
	if ! _ready_called:
		yield(self,"ready")
	$ClearArea.set_deferred("monitoring", ! disabled)
	$ClearArea.visible = ! disabled
	$HitArea.set_deferred("monitoring", ! disabled)
	$HitArea.visible = ! disabled
	$OffArea.set_deferred("monitoring", ! disabled)
	$OffArea.visible = ! disabled

func _set_clear_dist(value):
	clear_dist = value
	if ! _ready_called:
		yield(self,"ready")
	var half = clear_dist / 2.0
	$ClearArea/Shape.shape.extents.x = half
	$ClearArea/Shape.transform.origin.x = half

func _set_off_course_disabled(value):
	off_course_disabled = value
	if ! _ready_called:
		yield(self,"ready")
	$OffArea/Shape.disabled = value
	$OffArea.visible = ! value

func _set_off_course_dist(value):
	off_course_dist = value
	if ! _ready_called:
		yield(self,"ready")
	var half = off_course_dist / 2.0
	$OffArea/Shape.shape.extents.x = half
	$OffArea/Shape.transform.origin.x = half

func _set_pointer_count(value):
	pointer_count = value
	if ! _ready_called:
		yield(self,"ready")
	$Pointer1.visible = pointer_count >= 1
	$Pointer2.visible = pointer_count >= 2
	$Pointer3.visible = pointer_count >= 3
	
func _set_color(value):
	color = value
	if ! _ready_called:
		yield(self,"ready")
	$Standing.color = color
	$Pointer1.color = color
	$Pointer2.color = color
	$Pointer3.color = color

func _on_ClearArea_area_entered(area):
	emit_signal("cleared",self)

func _on_HitArea_area_entered(area):
	emit_signal("hit",self)

func _on_OffArea_area_entered(area):
	emit_signal("off_course",self)
