extends Node2D
class_name BaseElement

# returns the list of PhysicsCones in the element
func get_cones():
	return []

func to_dict():
	return {
		'position' : {'x' : position.x, 'y' : position.y},
		'rotation' : rotation
	}
	
func from_dict(data):
	position.x = data.position.x
	position.y = data.position.y
	rotation = data.rotation
