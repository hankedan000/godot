tool
extends BaseElement
class_name BasicGate

export var gate_distance = 200 setget _set_gate_distance
export var clear_order = 0 setget _set_clear_order
export (int,0,3) var pointer_count := 0 setget _set_pointer_count
export (Types.ConeColor) var color = Types.ConeColor.Orange setget _set_color

var _ready_called = false

func _ready():
	if ! Engine.editor_hint:
		$DirArrow.queue_free()# no need to keep around in real game
	else:
		$DirArrow.visible = true
	_ready_called = true
	
func get_user_class():
	return "BasicGate"

func to_dict():
	var data = .to_dict()
	data['gate_distance'] = gate_distance
	data['clear_order'] = clear_order
	data['pointer_count'] = pointer_count
	data['color'] = Types.ConeColor.keys()[int(color)]
	return data
	
func from_dict(data):
	.from_dict(data)
	self.gate_distance = data.gate_distance
	self.clear_order = data.clear_order
	self.pointer_count = data.pointer_count
	self.color = Types.ConeColor.get(data.color)
	
func _set_gate_distance(value):
	gate_distance = value
	if ! _ready_called:
		yield(self,"ready")
	var half_dist = gate_distance / 2.0
	$LeftCone.transform.origin.x = -half_dist
	$LeftCone.clear_dist = half_dist
	$RightCone.transform.origin.x = half_dist
	$RightCone.clear_dist = half_dist
	
func _set_clear_order(value):
	clear_order = value
	if ! _ready_called:
		yield(self,"ready")
	for cone in get_cones():
		cone.clear_order = clear_order

func _set_pointer_count(value):
	pointer_count = value
	if ! _ready_called:
		yield(self,"ready")
	for cone in get_cones():
		cone.pointer_count = pointer_count

func _set_color(value):
	color = value
	if ! _ready_called:
		yield(self,"ready")
	for cone in get_cones():
		cone.color = color

# returns the list of PhysicsCones in the element
func get_cones():# override
	return [$LeftCone,$RightCone]
