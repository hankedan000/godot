tool
extends BasicGate
class_name StartFinishGate

signal color_set()

export (bool) var is_start := false setget _set_is_start

var _color_set = false

func _ready():
	_set_is_start(is_start)
	
func get_user_class():
	return "StartFinishGate"

func to_dict():
	var data = .to_dict()
	data['is_start'] = is_start
	return data
	
func from_dict(data):
	.from_dict(data)
	self.is_start = data.is_start

# override BasicGate's method so that it doesn't change the cone color
# we only want the 'is_state' property to contorl color
func _set_color(value):
	_color_set = true
	emit_signal("color_set")

func _set_is_start(value):
	is_start = value
	if ! _ready_called:
		yield(self,"ready")
	# make sure color is set first or else it will will overwrite use
	if ! _color_set:
		yield(self,"color_set")
	
	$Label.text = ("START" if is_start else "FINISH")
	for p_cone in get_cones():
		p_cone.color = (Types.ConeColor.Green if is_start else Types.ConeColor.Yellow)
