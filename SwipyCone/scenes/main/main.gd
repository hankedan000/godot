extends Control

onready var course := $CourseView/Viewport/Course
onready var course_view := $CourseView
onready var course_viewport := $CourseView/Viewport
onready var preview_dialog := $CourseWalkDialog
onready var preview_countdown_label := $PreviewCountDownLabel
onready var preview_timer := $PreviewTimer
onready var results_panel := $CourseResultsPanel
onready var menu_root := $MenuRoot

const COURSE_WALK_DURATION = 5# seconds

enum CourseState {
	# no course loaded, just default BaseCourse scene loaded
	Init,
	# before player touches to start 
	PreCourseWalk,
	# show timer for course walk
	CourseWalk,
	# ready to cross start line and begin timing
	Staged,
	# player is driving on course
	OnCourse,
	# crossed finish line, or went off-course
	Finished
}

var _curr_course_idx = -1
var _local_courses = []

var _course_state = CourseState.Init

func _ready():
	_local_courses = FileUtils.list_files_in_dir("res://assets/courses")
	go_to_menu_root()
	
func _process(_delta):
	if _course_state == CourseState.CourseWalk:
		_update_preview_countdown(preview_timer.time_left)
		
func _update_preview_countdown(time_left):
	preview_countdown_label.text = "Course Preview: %0.2fs" % time_left

func go_to_menu_root():
	menu_root.show()
	set_course_state(CourseState.Init)
	
func go_to_next_course():
	if _local_courses.empty():
		return
	
	_curr_course_idx = (_curr_course_idx + 1) % _local_courses.size()
	
	course.restore_from_file(_local_courses[_curr_course_idx])
	course._ready()
	menu_root.hide()
	set_course_state(CourseState.PreCourseWalk)
	
func set_course_state(next_state):
	match next_state:
		CourseState.Init:
			course_viewport.gui_disable_input = true
			course.show_reveal()
			preview_dialog.hide()
			preview_countdown_label.hide()
			results_panel.hide()
			course_view.hide()
		CourseState.PreCourseWalk:
			course_viewport.gui_disable_input = true
			course.show_reveal()
			preview_dialog.show()
			preview_countdown_label.hide()
			results_panel.hide()
			course_view.show()
		CourseState.CourseWalk:
			course_viewport.gui_disable_input = true
			course.hide_reveal()
			preview_dialog.hide()
			preview_countdown_label.show()
			results_panel.hide()
			course_view.show()
		CourseState.Staged:
			course_viewport.gui_disable_input = false
			course.show_reveal()
			preview_dialog.hide()
			preview_countdown_label.hide()
			results_panel.hide()
			course_view.show()
		CourseState.OnCourse:
			course_viewport.gui_disable_input = false
			course.hide_reveal()
			preview_dialog.hide()
			preview_countdown_label.hide()
			results_panel.hide()
			course_view.show()
		CourseState.Finished:
			course_viewport.gui_disable_input = true
			course.show_reveal()
			preview_dialog.hide()
			preview_countdown_label.hide()
			results_panel.show()
			course_view.show()
	_course_state = next_state

func _on_Course_course_start():
	set_course_state(CourseState.OnCourse)

func _on_Course_course_cleared(drive_time, cone_count):
	results_panel.set_course_clear(drive_time,cone_count)
	set_course_state(CourseState.Finished)

func _on_Course_off_course():
	results_panel.set_off_course()
	set_course_state(CourseState.Finished)

func _on_MainMenu_single_player_selected():
	go_to_next_course()

func _on_CourseResultsPanel_quit_selected():
	go_to_menu_root()

func _on_CourseResultsPanel_next_selected():
	go_to_next_course()

func _on_PreviewTimer_timeout():
	set_course_state(CourseState.Staged)

func _on_CourseWalkDialog_start():
	preview_timer.start(COURSE_WALK_DURATION)
	_update_preview_countdown(COURSE_WALK_DURATION)
	set_course_state(CourseState.CourseWalk)
