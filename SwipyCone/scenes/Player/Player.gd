extends Area2D

onready var tail := $Tail

var _prev_position = null
var _enable_tail = false

func _physics_process(_delta):
	if ! _enable_tail:
		return
	
	if _prev_position != null:
		var vec : Vector2 = global_position - _prev_position
		var vec_len = vec.length()
		tail.disabled = vec_len < 50
		tail.shape.extents.x = vec_len / 2.0
		tail.position.x = vec_len / -2.0
		rotation = vec.angle()
	_prev_position = global_position

func start_tail():
	_prev_position = null
	_enable_tail = true

func stop_tail():
	_enable_tail = false
