extends KinematicBody2D

const HOP_DIST = 50.0
const HOP_TIME = 0.2
var hopping = false

func _input(event):
	if event is InputEvent and event.is_pressed():
		var velocity = Vector2()
		
		var animation = ""
		if Input.is_action_pressed("ui_down"):
			animation = "down_hop"
			velocity.y = 1.0
		elif Input.is_action_pressed("ui_up"):
			animation = "up_hop"
			velocity.y = -1.0
		elif Input.is_action_pressed("ui_left"):
			animation = "left_hop"
			velocity.x = -1.0
		elif Input.is_action_pressed("ui_right"):
			animation = "right_hop"
			velocity.x = 1.0
			
		if not hopping and velocity.length() > 0.0:
			$AnimatedSprite.frame = 0
			$AnimatedSprite.play(animation)
			$Tween.interpolate_property(
				self,"position",
				position,
				position + HOP_DIST * velocity,
				HOP_TIME,
				Tween.TRANS_LINEAR,
				Tween.EASE_IN_OUT)
			$Tween.start()
			hopping = true
			
func kill():
	queue_free()
		
func _on_Tween_tween_all_completed():
	hopping = false
