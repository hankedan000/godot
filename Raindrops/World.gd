extends Node2D

export var Raindrop : PackedScene
# number of drops per timer cycle
const DROP_COUNT = 3

func _on_GoalArea_frog_entered():
	$WinLabel.visible = true

func makeRaindrop():
	var newDrop = Raindrop.instance()
	add_child(newDrop)
	
	if newDrop is Node2D:
		var screenX = get_viewport().get_visible_rect().size.x
		var screenY = get_viewport().get_visible_rect().size.y
		
		newDrop.position.x = randi() % int(screenX)
		newDrop.position.y = randi() % int(screenY)

func _on_Timer_timeout():
	for drop in range(DROP_COUNT):
		makeRaindrop()
