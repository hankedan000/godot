extends Node2D

const DROP_TIME = 2.0
const SPLAT_TIME = 0.2
const DRY_TIME = 3.0
var falling = true
var drying = false

func _ready():
	$Shadow.modulate = Color(1,1,1,0)
	$Puddle.hide()
	
	$Tween.interpolate_property(
		$Shadow,"modulate",
		Color(0.2,0.2,0.2,0),
		Color(0.2,0.2,0.2,1),
		DROP_TIME,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN)
	
	$Tween.interpolate_property(
		$Shadow,"scale",
		Vector2(0.5,0.5),
		Vector2(3.0,3.0),
		DROP_TIME,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN)
	
	$Tween.start()

func _on_Tween_tween_all_completed():
	if falling:
		# shadow tweens finished
		$Shadow.hide()
		$Puddle.show()
		$Forcefield/Shape.disabled = false
		falling = false
		
		$Tween.interpolate_property(
			$Puddle,"scale",
			Vector2(0.5,0.5),
			Vector2(1.5,1.5),
			SPLAT_TIME,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN)
		$Tween.start()
	elif not drying:
		# splat tween finished
		$Tween.interpolate_property(
			$Puddle,"modulate",
			Color(1,1,1,1),
			Color(1,1,1,0),
			DRY_TIME,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN)
		$Tween.start()
		drying = true
	else:
		# kill the rain drop
		queue_free()

func _on_Forcefield_body_entered(body):
	if body.has_method("kill"):
		body.kill()
