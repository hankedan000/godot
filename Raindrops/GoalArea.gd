extends Area2D

signal frog_entered

func _on_GoalArea_body_entered(body):
	emit_signal("frog_entered")
