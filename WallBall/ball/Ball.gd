extends KinematicBody2D

export var speed = 3
var v = Vector2()

func _ready():
	v = Vector2(randf(),randf()).normalized() * speed

func _process(delta):
	var collision = move_and_collide(v)
	
	if collision:
		var other = collision.collider
		if other.has_method('kill'):
			other.kill()
		else:
			v = v.bounce(collision.normal)
