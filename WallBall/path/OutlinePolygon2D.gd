extends Polygon2D
class_name OutlinePolygon2D

export (Color) var outline_fill = Color(0,0,0,1)
export (float) var outline_width = 0.1

func _draw():
	var poly = get_polygon()
	for i in range(1 , poly.size()):
		draw_line(poly[i-1] , poly[i], outline_fill , outline_width)
	draw_line(poly[poly.size() - 1] , poly[0], outline_fill , outline_width)
