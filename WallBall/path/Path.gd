extends Path2D

export var width = 5

func _ready():
	redraw_line()
	$Line2D.width = width
	
func redraw_line():
	$Line2D.clear_points()
		
	for pp in range(curve.get_point_count()):
		$Line2D.add_point(curve.get_point_position(pp))