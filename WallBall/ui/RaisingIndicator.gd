extends Node2D

var raise_offset = Vector2(0,-50)
var duration = 2.0
var color = Color(1,1,1)
var text = "25" setget _set_text

var _init_position = Vector2()

func start():
	_init_position = global_position
	$Tween.interpolate_method(
		self,'_raise',
		0.0,1.0,
		duration,
		Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	$Tween.start()
	show()

func _set_text(value):
	text = value
	$Label.text = value
	
func _raise(ratio):
	var temp_color = Color(color.a,color.g,color.b,1-ratio)
	modulate = temp_color
	global_position = _init_position + raise_offset * ratio

func _on_Tween_tween_all_completed():
	queue_free()
