extends Node2D

signal traverse_complete
signal killed

export var player_id = 1 setget _set_player_id
export var speed = 200.0

func _ready():
	reset()
	
func _set_player_id(value):
	player_id = value
	$Line.default_color = Globals.get_player_color(player_id)
	
func kill():
	emit_signal("killed",player_id)
	queue_free()
	
func reset():
	$A.rect_position.x = 0
	$B.rect_position.x = 0
	$Line.points[0].x = 0
	$Line.points[1].x = 0
	$Body/CollisionShape2D.shape.a.x = 0
	$Body/CollisionShape2D.shape.b.x = 0

func start(stop_point):
	reset()
	var start_point = global_position
	var disp = stop_point - start_point
	global_rotation = disp.angle()
	var dur = disp.length() / speed
	$Tween.interpolate_method(
		self,'update_end_point',
		0,disp.length(),dur,
		Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	$Tween.start()
	
func update_end_point(next_pos):
	$B.rect_position.x = next_pos
	$Line.points[1].x = next_pos
	$Body/CollisionShape2D.shape.b.x = next_pos

func _on_Tween_tween_all_completed():
	emit_signal('traverse_complete',$A.rect_global_position,$B.rect_global_position,$Line.default_color)
	queue_free()
