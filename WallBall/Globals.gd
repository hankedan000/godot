extends Node

var PLAYER_COLORS = [Color('0d7bd4'),Color('dc245d')]

func get_player_color(player_id):
	if player_id - 1 < PLAYER_COLORS.size():
		return PLAYER_COLORS[player_id - 1]
	else:
		return Color()
