extends PathFollow2D

signal shoot

export var player_id = 1 setget _set_player_id
export var speed = 200.0

var v = 0.0

func get_velocity(delta):
	if Input.is_action_pressed('p1_left'):
		return speed
	elif Input.is_action_pressed('p1_right'):
		return -speed
	else:
		return 0
	
func handle_shoot():
	if Input.is_action_just_pressed('p1_button'):
		emit_signal(
			'shoot',
			global_position,
			$Heading.get_collision_point(),
			player_id)
		
func _set_player_id(value):
	player_id = value
	
func _process(delta):
	$Heading.global_rotation_degrees = round(global_rotation_degrees/90.0)*90.0
	handle_shoot()
	v = lerp(v,get_velocity(delta),0.15)
	offset += v * delta
