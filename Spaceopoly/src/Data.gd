extends Node

const Missions = [
	"Invented a new force field. If someone rolls a 12 at you for a laser shot, your ship survives the blast and you win the game.",
	"Federation called off your mission. Upon your next visit to Earth draw a new mission card.",
	"Next time you roll a 7 advance directly to Mars to repair the Sojourner Land Rover. Then advance directly to earth for a new mission.",
	"Collect 12 lasers.",
	"Sojourner got stuck in a rut. Next Time you roll a 5 advance to Mars to free the Sojourner then advance directly back to Earth for a new mission.",
	"Collect 6 lasers, Mars, Triton, and Titan.",
	"Permanently disable another players ship with a laser (double sixes) and then upon landing back on Earth your mission will be complete. (applys when there are more than 2 players)",
	"Collect Mars, the Moon, Areil, and IO. Then upon landing upon mars your mission will be complete.",
	"Collect Nereid, Larrisa, Triton, and Proteus.",
	"Collect all four fuel docks.",
	"Collect Mars, Venus, and Mercury. Then upon landing back on Earth your mission will be complete.",
	"Collect Dione, Iapetus, Tethys, Titan, and Enceladus.",
	"Collect Titania, Europa, IO, Proteus, and Tethys.",
	"Collect Oberon, Umbriel, Ariel, and Titania.",
	"Collect Ganymede, Larrisa, Rhea, and Deimos. Then upon landing back on Earth your mission will be complete.",
	"Collect Iapetus, Nereid, Himalia, Oberon, and Miranda.",
	"Collect Pluto, Venus, Mars, and Mercury.",
	"Collect Mars, Phobos, and Deimos. Upon landing back on Mars your mission will be complete.",
	"Collect Ganymede, Calisto, IO, Himalia, and Europa.",
	"Collect Pluto, Charon, Mars, and the Moon."
]

const PropertyInfo = {
	"Moon":      {"price": 340, "group_id": 0, "rent": [1000],                                "type": "moon",      "diameter": [2159,3476],  "distance_from": [238715,384405],         "orbit_period": 27.322},
	"Mercury":   {"price": 450, "group_id": 1, "rent": [1050],                                "type": "planet",    "diameter": [3029,4878],  "distance_from": [35955900,57900000],     "orbit_period": 87.97},
	"IO":        {"price": 340, "group_id": 2, "rent": [130,220,370,620,1040,1740,2920,4910], "type": "moon",      "diameter": [2260,3640],  "distance_from": [26200,421800],          "orbit_period": 1.769},
	"Callisto":  {"price": 350, "group_id": 2, "rent": [140,230,380,640,1080,1800,3030,5100], "type": "moon",      "diameter": [3100,5000],  "distance_from": [1170000,1884000],       "orbit_period": 16.689},
	"Elara":     {"price": 170, "group_id": 2, "rent": [70,110,180,310,520,670,1460,2450],    "type": "moon",      "diameter": [47,57],      "distance_from": [7291000,11740000],      "orbit_period": 260.1},
	"Ganymede":  {"price": 360, "group_id": 2, "rent": [140,240,400,660,1120,1880,3150,5290], "type": "moon",      "diameter": [3270,5270],  "distance_from": [664500,1070000],        "orbit_period": 7.155},
	"Himalia":   {"price": 200, "group_id": 2, "rent": [80,130,210,350,600,1000,1700,2830],   "type": "moon",      "diameter": [106,170],    "distance_from": [7122900,11470000],      "orbit_period": 250.5},
	"Europa":    {"price": 330, "group_id": 2, "rent": [130,210,350,600,1000,1700,2810,4720], "type": "moon",      "diameter": [1950,3140],  "distance_from": [417000,671400],         "orbit_period": 3.551},
	"Amalthia":  {"price": 200, "group_id": 2, "rent": [80,130,210,3500,600,1000,1700,2830],  "type": "moon",      "diameter": [106,170],    "distance_from": [112100,180500],         "orbit_period": 498},
	"Thebe":     {"price": 170, "group_id": 2, "rent": [70,120,200,330,560,940,1600,2650],    "type": "moon",      "diameter": [50,80],      "distance_from": [138000,222000],         "orbit_period": 0.674},
	"Umbriel":   {"price": 290, "group_id": 3, "rent": [130,290,650,1470,3300],               "type": "moon",      "diameter": [740,1190],   "distance_from": [165500,266500],         "orbit_period": 4.144},
	"Oberon":    {"price": 300, "group_id": 3, "rent": [130,300,680,1520,3410],               "type": "moon",      "diameter": [965,1550],   "distance_from": [362600,583800],         "orbit_period": 13.463},
	"Ariel":     {"price": 290, "group_id": 3, "rent": [130,290,640,1440,3250],               "type": "moon",      "diameter": [725,1170],   "distance_from": [118700,191100],         "orbit_period": 2.52},
	"Titania":   {"price": 310, "group_id": 3, "rent": [140,310,700,1570,3530],               "type": "moon",      "diameter": [990,1590],   "distance_from": [270900,436150],         "orbit_period": 8.706},
	"Miranda":   {"price": 250, "group_id": 3, "rent": [110,250,560,1260,2840],               "type": "moon",      "diameter": [300,480],    "distance_from": [80400,129500],          "orbit_period": 1.414},
	"Pluto":     {"price": 430, "group_id": 4, "rent": [500,3470],                            "type": "planet",    "diameter": [1860,3000],  "distance_from": [3663000000,5898600000], "orbit_period": 90584},
	"Charon":    {"price": 300, "group_id": 4, "rent": [440,3000],                            "type": "moon",      "diameter": [800,1300],   "distance_from": [12420,20000],           "orbit_period": 6.387},
	"Phobos":    {"price": 120, "group_id": 5, "rent": [130,450,1670],                        "type": "moon",      "diameter": [17,27],      "distance_from": [5825,9380],             "orbit_period": 0.319},
	"Mars":      {"price": 470, "group_id": 5, "rent": [370,1340,4900],                       "type": "moon",      "diameter": [4219,6794],  "distance_from": [141463,227800000],      "orbit_period": 687},
	"Deimos":    {"price": 90,  "group_id": 5, "rent": [90,330,1220],                         "type": "moon",      "diameter": [7.5,12],     "distance_from": [14600,23500],           "orbit_period": 1.26},
	"Venus":     {"price": 490, "group_id": 6, "rent": [1200],                                "type": "planet",    "diameter": [7519,12108], "distance_from": [66943800,107800000],    "orbit_period": 224.7},
	"Mimas":     {"price": 240, "group_id": 7, "rent": [100,170,310,550,1000,1800,3230],      "type": "moon",      "diameter": [242,390],    "distance_from": [115200,185520],         "orbit_period": 0.942},
	"Dione":     {"price": 290, "group_id": 7, "rent": [110,200,350,640,1150,2080,3750],      "type": "moon",      "diameter": [696,1120],   "distance_from": [234700,377900],         "orbit_period": 2.737},
	"Iapetus":   {"price": 300, "group_id": 7, "rent": [120,210,370,670,1200,2170,3900],      "type": "moon",      "diameter": [907,1460],   "distance_from": [2212600,3562900],       "orbit_period": 79.331},
	"Rhea":      {"price": 300, "group_id": 7, "rent": [120,210,390,700,1250,2250,4100],      "type": "moon",      "diameter": [950,1530],   "distance_from": [320300,527000],         "orbit_period": 4.518},
	"Tethys":    {"price": 280, "group_id": 7, "rent": [110,200,350,630,1130,2030,3650],      "type": "moon",      "diameter": [660,1060],   "distance_from": [183100,294900],         "orbit_period": 1.888},
	"Enceladus": {"price": 250, "group_id": 7, "rent": [100,180,330,580,1050,1900,3400],      "type": "moon",      "diameter": [310,500],    "distance_from": [148000,238300],         "orbit_period": 1.37},
	"Titan":     {"price": 350, "group_id": 7, "rent": [140,250,450,810,1470,2640,4760],      "type": "moon",      "diameter": [3200,5150],  "distance_from": [758000,1221000],        "orbit_period": 15.945},
	"Nereid":    {"price": 230, "group_id": 8, "rent": [100,310,960,2980],                    "type": "moon",      "diameter": [210,340],    "distance_from": [3422400,5510000],       "orbit_period": 365.21},
	"Larissa":   {"price": 210, "group_id": 8, "rent": [90,280,860,2680],                     "type": "moon",      "diameter": [118,190],    "distance_from": [45700,73600],           "orbit_period": 0.55},
	"Triton":    {"price": 320, "group_id": 8, "rent": [140,430,1340,4170],                   "type": "moon",      "diameter": [1680,2700],  "distance_from": [219900,354000],         "orbit_period": 5.877},
	"Proteus":   {"price": 240, "group_id": 8, "rent": [110,320,1000,3130],                   "type": "moon",      "diameter": [260,420],    "distance_from": [73050,117600],          "orbit_period": 1.12},
	"JupiterFD": {"price": 350, "group_id": 9, "rent": [10,20,50,100],                        "type": "fuel_dock", "diameter": [0,0],        "distance_from": [0,0],                   "orbit_period": 0},
	"UranusFD":  {"price": 380, "group_id": 9, "rent": [10,30,100,200],                       "type": "fuel_dock", "diameter": [0,0],        "distance_from": [0,0],                   "orbit_period": 0},
	"SaturnFD":  {"price": 400, "group_id": 9, "rent": [20,40,120,250],                       "type": "fuel_dock", "diameter": [0,0],        "distance_from": [0,0],                   "orbit_period": 0},
	"NeptuneFD": {"price": 430, "group_id": 9, "rent": [20,50,150,300],                       "type": "fuel_dock", "diameter": [0,0],        "distance_from": [0,0],                   "orbit_period": 0}
}

var PropertyTextures = {}

func _ready():
	for prop in PropertyInfo.keys():
		var filepath = "res://imgs/property_cards/%s.jpg" % prop
		var prop_res = null
		# don't bother loading textures on server (reduces ram footprint)
		if not OS.has_feature('Server'):
			prop_res = load(filepath)
		PropertyTextures[prop] = prop_res
		
func get_init_money(is_bank):
	if is_bank:
		return 17 * 1000 + 18 * 500 + 24 * 100 + 18 * 50 + 18 * 10
	else:
		return 1 * 1000 + 1 * 500 + 1 * 100 + 1 * 50 + 1 * 10

func get_init_lasers(is_bank):
	if is_bank:
		return 50
	else:
		return 1

func get_init_properties(is_bank):
	var props = []
	if is_bank:
		for prop_str in Data.PropertyInfo:
			props.push_back(EnumUtils.to_enum(BaseSpot.Spots,prop_str))
	return props
		
func get_property_texture(spot_enum):
	var spot_str = EnumUtils.to_str(BaseSpot.Spots,spot_enum)
	if PropertyTextures.has(spot_str):
		return PropertyTextures[spot_str]
	return null
		
func get_property_info(spot_enum):
	var spot_str = EnumUtils.to_str(BaseSpot.Spots,spot_enum)
	if PropertyTextures.has(spot_str):
		return PropertyInfo[spot_str]
	return null
	
func determine_rent(spot,owned_proterties):
	var prop_info = get_property_info(spot)
	var owned_count = 0# number of properties owned in group
	for owned_prop in owned_proterties:
		var other_info = get_property_info(owned_prop)
		if prop_info.group_id == other_info.group_id:
			owned_count += 1
			
	if owned_count > 0 and owned_count <= prop_info.rent.length():
		# determine rent based on how many properties are owned
		return prop_info.rent[owned_count - 1]
	elif owned_count > 0:
		print("ERROR owned_count is greater than rent length! curr_spot = %s, owned_count = %d" % [EnumUtils.to_str(BaseSpot.Spots,spot),owned_count])
	
	return 0
		
