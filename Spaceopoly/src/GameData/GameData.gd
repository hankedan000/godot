extends Node
class_name GameData

signal connection_changed(connected)
signal connection_failed()
signal player_joined(pinfo)
signal player_color_set(pinfo)
signal player_color_changed(pinfo,old_color)
signal player_info_changed(net_id,old_pinfo,new_pinfo)
signal player_moved_to(net_id,old_pinfo,new_pinfo)
signal current_player_changed(pinfo)
signal start_game()

var SERVER_HOST = "localhost"
const SERVER_PORT = 5678

var has_profile = false
var profile = {
	"name" : "New Player"
}
var peer : NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
var connected = false
var game_started = false
var players = {}
var banker_info = {}
var current_player_id = -1# player's net_id who turn it is
var colors_available = []
# missions are only initialized and store on the server side
var missions_available = []

func _ready():
	# setup the banker
	banker_info = create_player_info()
	banker_info.net_id = -1
	banker_info.is_bank = true
	banker_info.name = "Banker"
	banker_info.money = Data.get_init_money(banker_info.is_bank)
	banker_info.lasers = Data.get_init_lasers(banker_info.is_bank)
	banker_info.properties = Data.get_init_properties(banker_info.is_bank)
	
	var localhost_specified = false
	for arg in OS.get_cmdline_args():
		if arg == "--localhost":
			localhost_specified = true
			break
			
	if localhost_specified:
		SERVER_HOST = "localhost"
	elif OS.has_feature('remote-server'):
		SERVER_HOST = "www.danielhankewycz.com"
	elif OS.has_feature('Android'):
		SERVER_HOST = "www.danielhankewycz.com"
	elif OS.has_feature('Server'):
		SERVER_HOST = "www.danielhankewycz.com"
#	SERVER_HOST = "www.danielhankewycz.com"
	
	for color in COLORS.ShipColor.values():
		colors_available.push_back(color)
	
	# restore any personal profile information
	restore()
	
	# Configure the network peer for multiplayer synchronization
	peer.connect("server_disconnected",self,"_server_disconnected")
	peer.connect("connection_failed",self,"_connection_failed")
	peer.connect("connection_succeeded",self,"_connection_succeeded")
	peer.connect("peer_connected",self,"_peer_connected")
	peer.connect("peer_disconnected",self,"_peer_disconnected")
	if OS.has_feature('Server'):
		print('Attempting to connect as server on %s:%d...' % [SERVER_HOST,SERVER_PORT])
		# TODO need to figure out a way to scale the server...
		peer.create_server(SERVER_PORT,4)
		
		# populate and shuffle the mission cards
		missions_available = Data.Missions.duplicate()
		randomize()# @Array.shuffle() uses global rng
		missions_available.shuffle()
		
		print('Server connected!')
	else:
		print('Attempting to connect as client on %s:%d...' % [SERVER_HOST,SERVER_PORT])
		peer.create_client(SERVER_HOST,SERVER_PORT)
	get_tree().network_peer = peer
		
###############################################################################
# PUBLIC METHODS
###############################################################################
	
func save():
	var data = {
		"profile": profile
	}
	
	var file = File.new()
	if file.open("user://game_data.json",File.WRITE) == OK:
		file.store_string(JSON.print(data,"  "))
		file.close()
	
func restore():
	var file = File.new()
	if file.open("user://game_data.json",File.READ) == OK:
		var data_str = file.get_as_text()
		var json_data := JSON.parse(data_str)
		if json_data.error == OK:
			var data = json_data.result
			if data.has("profile"):
				profile = data.profile
				has_profile = true
		file.close()
		
func create_player_info():
	return {
		"name" : profile.name,
		"net_id" : my_net_id(),
		"turn_order" : -1,
		"is_bank" : false,
		"pawn_spot" : BaseSpot.Spots.Earth,
		"pawn_color" : null,
		"lost_turn" : false,
		"mission" : "",
		"money" : 0,# dont care?
		"lasers" : 0,# dont care?
		"properties" : []# dont care?
	}
		
func is_color_available(color):
	return colors_available.has(color)
	
func get_property_owner(spot):
	for pinfo in players:
		if spot in pinfo.properties:
			return pinfo.net_id
	return null
		
func my_net_id():
	return peer.get_unique_id()
	
func set_profile(new_profile):
	profile = new_profile
	has_profile = true
	
func get_my_profile():
	return profile
	
func get_my_mission():
	if players.has(my_net_id()):
		return players[my_net_id()].mission
	return ""
	
func get_player_info(net_uid):
	if players.has(net_uid):
		return players[net_uid].duplicate()
	return null
	
func get_player_info_from_pawn_color(pawn_color):
	for pinfo in players.values():
		if pinfo.pawn_color == pawn_color:
			return pinfo.duplicate()
	return null
	
func has_profile():
	return has_profile
		
###############################################################################
# PRIVATE METHODS
###############################################################################
		
# Business logic used to set the turn order of each player.
#
# net_ids_in_turn_order -> A list of the player's network unique ids. List is
# order based on the player's turn order. The first player in the list will go
# first in the game, the second will go second, etc.
func _set_turn_order(net_ids_in_turn_order):
	var turn_order = 0
	for net_id in net_ids_in_turn_order:
		players[net_id].turn_order = turn_order
		turn_order += 1
		
###############################################################################
# REMOTE "PUBLIC" METHODS
###############################################################################

# Business logic used to start the game
#
# net_ids_in_turn_order -> A list of the player's network unique ids. List is
# order based on the player's turn order. The first player in the list will go
# first in the game, the second will go second, etc. By default, this list is
# empty, but will be populated by the server and the server will notify each
# peer of the generated list
remote func start_game(net_ids_in_turn_order=[]):
	if get_tree().is_network_server():
		# shuffle the list of player net_ids to get a random turn order
		net_ids_in_turn_order = players.keys()
		net_ids_in_turn_order.shuffle()
		
		# update server with the turn order
		_set_turn_order(net_ids_in_turn_order)
		
		# update all peers with the turn order
		for other_net_id in players:
			if other_net_id != 1:
				rpc_id(other_net_id,"start_game",net_ids_in_turn_order)
	else:
		# list is generated on server side, so just set it on the client side
		_set_turn_order(net_ids_in_turn_order)
	
	game_started = true
	emit_signal("start_game")
	
	# notify peers that it's the first player's turn
	for pinfo in players.values():
		if pinfo.turn_order == 0:
			rpc("set_current_player",pinfo.net_id)
			break
			
remote func buy_property(net_id,prop_spot):
	if get_tree().is_network_server():
		var pinfo = players[net_id]
		var prop_info = Data.get_property_info(prop_spot)
		var price = prop_info.price
		
		if pinfo.money >= price:
			# transfer money
			pinfo.money -= price
			banker_info.money += price
			# transfer properties
			banker_info.properties.erase(prop_spot)
			pinfo.properties.append(prop_spot)
			
		# update all peers with new player information
		for other_net_id in players:
			if other_net_id != 1:
				rpc_id(other_net_id,"update_player_info",pinfo)
				rpc_id(other_net_id,"update_player_info",banker_info)
			
remote func request_new_mission(net_id):
	if get_tree().is_network_server():
		var pinfo = players[net_id]
		
		# swap out mission card
		missions_available.push_back(pinfo.mission)
		pinfo.mission = missions_available.pop_front()
		
		# update all peers with new player information
		for other_net_id in players:
			if other_net_id != 1:
				rpc_id(other_net_id,"update_player_info",pinfo)
			
remote func next_turn():
	if get_tree().is_network_server():
		if not players.has(current_player_id):
			print("current player is not known. can't determine next turn")
			return
			
		var curr_pinfo = players[current_player_id]
		var next_turn_order = (curr_pinfo.turn_order + 1) % players.size()
		var next_pinfo = null
		for pinfo in players.values():
			if pinfo.turn_order == next_turn_order:
				next_pinfo = pinfo
				break
				
		if next_pinfo != null:
			rpc("set_current_player",next_pinfo.net_id)
		else:
			print("Can't find next player. curr_pinfo = %s" % curr_pinfo)
			
remote func set_player_color(net_id,color):
	if get_tree().is_network_server():
		if is_color_available(color):
			# update the server with the new color
			_set_player_color(net_id,color)
			
			# notify the other players of the new color
			for other_net_id in players:
				if other_net_id != 1:
					rpc_id(other_net_id,"_set_player_color",net_id,color)
			
remotesync func update_player_info(new_pinfo):
	var net_id = new_pinfo.net_id
	if net_id == -1:
		var old_pinfo = banker_info.duplicate()
		banker_info = new_pinfo
		emit_signal("player_info_changed",net_id,old_pinfo,new_pinfo)
	elif players.has(net_id):
		var old_pinfo = players[net_id].duplicate()
		players[net_id] = new_pinfo
		emit_signal("player_info_changed",net_id,old_pinfo,new_pinfo)

remotesync func set_current_player(net_id):
	current_player_id = net_id
	emit_signal("current_player_changed",get_player_info(net_id))

remote func add_player(pinfo):
	if get_tree().is_network_server():
		# initial the player with all of its game assets
		pinfo.mission = missions_available.pop_front()
		pinfo.money = Data.get_init_money(pinfo.is_bank)
		pinfo.lasers = Data.get_init_lasers(pinfo.is_bank)
		pinfo.properties = Data.get_init_properties(pinfo.is_bank)
		
		# tell the original client to add itself
		rpc_id(pinfo.net_id,"add_player",pinfo)
	
		# we're on the server. distribute currnt player list to all peers
		for net_id in players:
			print("Telling '%s' about '%s'" % [pinfo.name,players[net_id].name])
			# send new player the currently iterated player
			rpc_id(pinfo.net_id,"add_player",players[net_id])
			# send currently iterated player the new player info, skipping
			# server (it will get the info shortly)
			if net_id != 1:
				rpc_id(net_id,"add_player",pinfo)
	
	print("'%s' (%d) joined the game!" % [pinfo.name,pinfo.net_id])
	players[pinfo.net_id] = pinfo
	emit_signal("player_joined",pinfo)
		
###############################################################################
# REMOTE "PRIVATE" METHODS
###############################################################################

# Business logic used to change the player color on the client side.
# This method is supposed to be "private" and only called by the server when
# notifying clients for a players color change.
#
# net_id -> netowrk id of the player whos color is being set
# color -> the player's new selected color
remote func _set_player_color(net_id,color):
	var pinfo = players[net_id]
	if pinfo.pawn_color == null:
		# player is setting color for the first time
		colors_available.erase(color)
		pinfo.pawn_color = color
		emit_signal("player_color_set",pinfo)
	if pinfo.pawn_color != null and pinfo.pawn_color != color:
		# player is changing colors
		var old_color = pinfo.pawn_color
		colors_available.erase(color)
		colors_available.push_back(old_color)
		pinfo.pawn_color = color
		emit_signal("player_color_changed",pinfo,old_color)

###############################################################################
# SIGNAL HANDLERS
###############################################################################
	
func _server_disconnected():
	print("_server_disconnected")
	
func _connection_failed():
	print("_connection_failed")
	connected = false
	emit_signal("connection_changed",connected)
	emit_signal("connection_failed")
	
func _connection_succeeded():
	print("_connection_succeeded")
	connected = true
	emit_signal("connection_changed",connected)
	
func _peer_connected(peer_id):
	print("peer %d connected!" % peer_id)
	
func _peer_disconnected(peer_id):
	print("peer %d disconnected!" % peer_id)
	# remove the player if it exists
	if players.has(peer_id):
		var pinfo = players[peer_id]
		players.erase(peer_id)
		colors_available.push_back(pinfo.pawn_color)
		missions_available.push_back(pinfo.mission)
