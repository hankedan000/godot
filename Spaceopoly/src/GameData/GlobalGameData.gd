extends Node

signal instance_set()

var inst : GameData = null setget _set_inst,_get_inst

func _set_inst(instance: GameData):
	inst = instance
	if is_instance_valid(instance):
		emit_signal("instance_set")
	
func _get_inst() -> GameData:
	return inst
