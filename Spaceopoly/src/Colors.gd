extends Node
class_name Colors

enum ShipColor {
	Purple,
	Yellow,
	Blue,
	Black
}

var ShipPurple = preload("res://imgs/purple.png")
var ShipYellow = preload("res://imgs/yellow.png")
var ShipBlue = preload("res://imgs/blue.png")
var ShipBlack = preload("res://imgs/black.png")

func get_ship_texture(ship_color_enum):
	match ship_color_enum:
		ShipColor.Purple:
			return ShipPurple
		ShipColor.Yellow:
			return ShipYellow
		ShipColor.Blue:
			return ShipBlue
		ShipColor.Black:
			return ShipBlack
			
func get_color(ship_color_enum) -> Color:
	match ship_color_enum:
		ShipColor.Purple:
			return Color("c97ae3")
		ShipColor.Yellow:
			return Color("ffe680")
		ShipColor.Blue:
			return Color("617aff")
		ShipColor.Black:
			return Color("111111")
	return Color(1,1,1)
