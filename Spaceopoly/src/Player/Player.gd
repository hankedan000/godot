extends Node
class_name Player

signal became_current(player)
signal move_complete(player,spot)

const Pawn = preload("res://ui/Ship/Ship.tscn")

export var is_bank = false

var current = false setget _set_current
var pawn : Ship = null
var ready = false

func _ready():
	pawn = Pawn.instance()
	pawn.connect("move_complete",self,"_on_Pawn_move_complete")
	ready = true
	
###############################################################################
# SETTERS/GETTERS
###############################################################################
	
func _set_current(value):
	current = value
	if is_instance_valid(pawn):
		pawn.current = value
	
###############################################################################
# PUBLIC METHODS
###############################################################################

func set_color(color):
	pawn.color = color

func get_spot():
	return pawn.spot
	
func set_spot(spot):
	pawn.spot = spot
	
func get_player_info():
	var pinfo = null
	if GlobalGameData.inst:
		if is_bank:
			pinfo = GlobalGameData.inst.banker_info
		else:
			pinfo = GlobalGameData.inst.get_player_info(int(name))
	return pinfo
	
###############################################################################
# SIGNAL HANDLERS
###############################################################################

func _on_Pawn_move_complete(ship):
	emit_signal("move_complete",self,get_spot())
