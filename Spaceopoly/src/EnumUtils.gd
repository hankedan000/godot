extends Node

func to_str(enum_type, val):
	for key in enum_type.keys():
		if enum_type[key] == val:
			return key
	return "***INVALID***"
	
func to_enum(enum_type, string):
	for key in enum_type.keys():
		if key == string:
			return enum_type[key]
	return null

func next_enum(enum_type, val):
	var found = false
	for key in enum_type.keys():
		if found:
			return enum_type[key]
		elif enum_type[key] == val:
			found = true
	
	if found:
		# val was the last value in the enum, return first
		return enum_type[enum_type.keys()[0]]
		
	return null
		
