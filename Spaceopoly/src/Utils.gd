extends Node

enum Orbits {
	Jupiter,
	Uranus,
	Saturn,
	Neptune,
	None
}

func determine_orbit(spot):
	var ORBIT_MAP = {}
	ORBIT_MAP[Orbits.Jupiter] = [
		BaseSpot.Spots.IO,
		BaseSpot.Spots.JupiterFD,
		BaseSpot.Spots.Warp3,
		BaseSpot.Spots.Callisto,
		BaseSpot.Spots.Elara,
		BaseSpot.Spots.FS2,
		BaseSpot.Spots.Thebe,
		BaseSpot.Spots.OutOfFuel,
		BaseSpot.Spots.Ganymede,
		BaseSpot.Spots.Laser,
		BaseSpot.Spots.Himalia,
		BaseSpot.Spots.Europa,
		BaseSpot.Spots.WonDispute
	]
	ORBIT_MAP[Orbits.Uranus] = [
		BaseSpot.Spots.Miranda,
		BaseSpot.Spots.UranusFD,
		BaseSpot.Spots.Umbriel,
		BaseSpot.Spots.UranusNull,
		BaseSpot.Spots.Oberon,
		BaseSpot.Spots.AdvTriton,
		BaseSpot.Spots.Ariel,
		BaseSpot.Spots.FS4,
		BaseSpot.Spots.Titania
	]
	ORBIT_MAP[Orbits.Saturn] = [
		BaseSpot.Spots.Mimas,
		BaseSpot.Spots.SaturnFD,
		BaseSpot.Spots.Dione,
		BaseSpot.Spots.Iapetus,
		BaseSpot.Spots.Rhea,
		BaseSpot.Spots.Tethys,
		BaseSpot.Spots.FS6,
		BaseSpot.Spots.TurboLaser,
		BaseSpot.Spots.Enceladus,
		BaseSpot.Spots.MotionSickness,
		BaseSpot.Spots.AdvFS,
		BaseSpot.Spots.Titan,
		BaseSpot.Spots.FS7
	]
	ORBIT_MAP[Orbits.Neptune] = [
		BaseSpot.Spots.Nereid,
		BaseSpot.Spots.Larissa,
		BaseSpot.Spots.NeptuneFD,
		BaseSpot.Spots.AdvProp,
		BaseSpot.Spots.NeptuneNull,
		BaseSpot.Spots.OxygenLeak,
		BaseSpot.Spots.Triton,
		BaseSpot.Spots.FS9,
		BaseSpot.Spots.Proteus
	]
	
	for orbit in ORBIT_MAP.keys():
		if spot in ORBIT_MAP[orbit]:
			return orbit
	return Orbits.None

# curr_spot: the current spot that you'd like to know where to go next
# roll_count: the number of dice moves left in your roll. set to -1 if you
# 	wish to have "infinite" moves, this is in affect like advancing around the
#	board.
# accept_amalthia: if the roll_count == 1 and the curr_spot is on ganymede,
#	then this method will return Amalthia
func next_spot(curr_spot, roll_count = -1, accept_amalthia = false):
	match curr_spot:
		BaseSpot.Spots.WonDispute:
			return BaseSpot.Spots.IO
		BaseSpot.Spots.Ganymede:
			if roll_count == -1 or roll_count > 5:
				return BaseSpot.Spots.Amalthia
			elif roll_count == 1 and accept_amalthia:
				return BaseSpot.Spots.Amalthia
		BaseSpot.Spots.Titania:
			return BaseSpot.Spots.Miranda
		BaseSpot.Spots.Ariel:
			if roll_count == -1 or roll_count > 3:
				return BaseSpot.Spots.UranusGravity1
		BaseSpot.Spots.FS7:
			return BaseSpot.Spots.Mimas
		BaseSpot.Spots.AdvFS:
			if roll_count == -1 or roll_count > 4:
				return BaseSpot.Spots.SaturnGravity1
		BaseSpot.Spots.Proteus:
			if roll_count == -1 or roll_count > 3:
				return BaseSpot.Spots.NeptuneGravity1
			else:
				return BaseSpot.Spots.Nereid
	
	return EnumUtils.next_enum(BaseSpot.Spots,curr_spot)
	
func next_spot_advance_to(curr_spot,adv_to_spot):
	if curr_spot == adv_to_spot:
		return curr_spot
		
	var curr_orbit = determine_orbit(curr_spot)
	var dest_orbit = determine_orbit(adv_to_spot)
	if curr_orbit == dest_orbit:
		# spot we want is in current orbit. single step around til we get there
		return next_spot(curr_spot,1,false)
	else:
		# spot we want is out of current orbit. give -1 for roll_count to
		# kick us out of orbit when we can so we can move on around the board
		return next_spot(curr_spot,-1,false)
	
func is_a_property(spot_enum):
	var spot_str = EnumUtils.to_str(BaseSpot.Spots,spot_enum)
	return Data.PropertyInfo.has(spot_str)
	
func diff_pinfo(old_pinfo,new_pinfo):
	var delta = {
		"money" : new_pinfo.money - old_pinfo.money,
		"lasers" : new_pinfo.lasers - old_pinfo.lasers,
		"mission_changed" : new_pinfo.mission != old_pinfo.mission,
		"spot_changed" : new_pinfo.pawn_spot != old_pinfo.pawn_spot,
		"lost_turn" : new_pinfo.lost_turn,
		"props_gained" : [],
		"props_lost" : []
	}
	
	# build the list of properties that were gained
	for prop in new_pinfo.properties:
		if not old_pinfo.properties.has(prop):
			delta.props_gained.push_back(prop)
	
	# build the list of properties that were lost
	for prop in old_pinfo.properties:
		if not new_pinfo.properties.has(prop):
			delta.props_lost.push_back(prop)
	
	return delta
	
func spot_to_pretty_string(spot):
	var pretty_str = EnumUtils.to_str(BaseSpot.Spots,spot)
	if is_a_property(spot):
		return pretty_str
	elif "Gravity" in pretty_str:
		return null
	elif "FS" in pretty_str:
		return "a Federation Station"
		
	match spot:
		BaseSpot.Spots.JupiterFD:
			return "Jupiter Fuel Dock"
		BaseSpot.Spots.UranusFD:
			return "Uranus Fuel Dock"
		BaseSpot.Spots.SaturnFD:
			return "Saturn Fuel Dock"
		BaseSpot.Spots.NeptuneFD:
			return "Neptune Fuel Dock"
		BaseSpot.Spots.SolarFlare:
			return "Solar Flare"
		BaseSpot.Spots.Warp2:
			return "Warp 2"
		BaseSpot.Spots.Warp3:
			return "Warp 3"
		BaseSpot.Spots.OutOfFuel:
			return "Out of Fuel"
		BaseSpot.Spots.Laser:
			return "Laser"
		BaseSpot.Spots.WonDispute:
			return "Won A Dispute"
		BaseSpot.Spots.AdvProp:
			return "Advance To Property"
		BaseSpot.Spots.Adv1:
			return "Advance To Property"
		BaseSpot.Spots.Adv2:
			return "Advance To Property"
		BaseSpot.Spots.UranusNull:
			return null
		BaseSpot.Spots.AdvTriton:
			return "Adanvce To Triton"
		BaseSpot.Spots.Antigravity:
			return "Anti Gravity Field"
		BaseSpot.Spots.DiscComet:
			return "Discovered A Comet"
		BaseSpot.Spots.AsteroidBelt:
			return "Discovered An Astroid Belt"
		BaseSpot.Spots.MeteorShower:
			return "Meteor Shower"
		BaseSpot.Spots.TurboLaser:
			return "Turbo Laser"
		BaseSpot.Spots.MotionSickness:
			return "Motion Sickness"
		BaseSpot.Spots.AdvFS:
			return "Advance To Federation Statation"
		BaseSpot.Spots.NeptuneNull:
			return null
		BaseSpot.Spots.OxygenLeak:
			return "Oxygen Leak"
