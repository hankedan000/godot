extends MarginContainer

const PlayerRes = preload("res://src/Player/Player.tscn")
const SERVER_ID = 1

onready var boardview = $UI/Game/BoardView
onready var board = $UI/Game/BoardView/Viewport/Board
onready var game_data = $GameData
onready var lobby = $UI/Lobby/LobbyPopup
onready var ship_picker = $UI/Lobby/ShipPicker
onready var banker = $GameData/Banker
onready var banker_panel = $UI/Game/BottomPanel/HBox/ButtonBox/BankerPanel
onready var mission_card = $UI/GameDialogs/MissionCardView
onready var shade = $UI/Shade
onready var game_ui = $UI/Game
onready var game_dialogs = $UI/GameDialogs
onready var bottom_panel = $UI/Game/BottomPanel
onready var float_dialog = $UI/GameDialogs/FloatDialog
onready var property_collection = $UI/GameDialogs/PropertyCollectionDialog
onready var property_view = $UI/GameDialogs/PropertyView
onready var dice_button = $UI/Game/BottomPanel/HBox/ButtonBox/DiceButton
onready var next_turn_button = $UI/Game/NextTurnButton
onready var laser_button = $UI/Game/LaserButton
onready var debug_panel = $UI/Debug/DebugPanel
onready var debug_button = $UI/Debug/DebugButton

onready var doubles_sound = $sounds/RolledDoubles

enum RollState {
	Move,Warp,Laser,FuelDock,Dispute
}

var current_player : Player = null
var my_turn = false
var roll_state = RollState.Move
var rolls_left = 0
var rolled_doubles = false
# set to true once player has started their "lost turn" used to clear lost_turn
# flag once the "next turn" button is pressed
var clear_lost_turn = false
var moving = false
# Set to true when player lands on property and is require accept/reject view
# the property view dialog. This variable is used to "reshow" the PropertyView
# when the player hides the dialog due to them looking at other player
# properties maybe looking at their own mission card 
var needs_to_purchase_property = false
var debug_mode = false

func _ready():
	debug_mode = OS.has_feature("debug")
	debug_panel.visible = false
	debug_button.visible = debug_mode
	
	TheBoard.inst = board
	GlobalGameData.inst = game_data
	
	board.connect("pawn_begin_move_to",self,"_on_Board_pawn_begin_move")
	
	# bind players to their player panels
	banker_panel.bind_to_player(banker)
	
	lobby.popup_centered_ratio(0.4)
	
	for dialog in game_dialogs.get_children():
		if dialog is Control:
			dialog.connect(
				"visibility_changed",
				self,"_on_Dialog_visibility_changed")
	
func get_player(net_id):
	return get_node("Players/%d" % net_id)
	
func notify_turn_complete():
	game_data.rpc_id(SERVER_ID,"next_turn")
	
func update_ui():
	var lost_a_turn = false
	for player_panel in bottom_panel.get_player_panels():
		if player_panel is PlayerPanel and player_panel.player != null:
			# default buttons to not visible
			player_panel.set_property_button_visible(false)
			player_panel.set_laser_button_visible(false)
			
			var pinfo = player_panel.player.get_player_info()
			if pinfo.net_id == game_data.my_net_id():
				lost_a_turn = pinfo.lost_turn
				player_panel.set_property_button_visible(needs_to_purchase_property)
	
	if my_turn:
		if lost_a_turn:
			dice_button.enable_dice(false)
			next_turn_button.disabled = false
		elif needs_to_purchase_property:
			dice_button.enable_dice(false)
			next_turn_button.disabled = true
		elif rolls_left > 0 and not moving:
			dice_button.enable_dice(true)
			next_turn_button.disabled = true
		elif rolls_left == 0 and not moving:
			dice_button.enable_dice(false)
			next_turn_button.disabled = false
		else:
			print('unhandled case!')
	else:
		# disable buttons for all other players
		dice_button.enable_dice(false)
		next_turn_button.disabled = true
		
func handle_land(pinfo):
	var spot = pinfo.pawn_spot
	
	if pinfo.net_id == game_data.my_net_id():
		var spot_ctrl = board.get_spot_node(spot)
		if spot_ctrl is SpotControl:
			# only perform spot life cycle calls on pawn's client
			spot_ctrl.on_player_landed(pinfo)
	
	if Utils.is_a_property(spot):
		var prop_info = Data.get_property_info(spot)
		
		# determine if the player has enough to buy the property
		var can_buy = pinfo.money >= prop_info.price
		needs_to_purchase_property = can_buy
		
		if can_buy:
			# display the property view so player can purchase
			property_view.prop_spot = spot
			property_view.configure(
				can_buy,
				false)# disable sell button
			property_view.popup_centered_minsize()
			
	# update buttons and things based on current game state
	update_ui()
	
func _on_Board_pawn_begin_move(pawn,move_to):
	var pinfo = game_data.get_player_info_from_pawn_color(pawn.color)
	if pinfo != null:
		pinfo.pawn_spot = move_to
		if pinfo.net_id == game_data.my_net_id():
			# local player's pawn is moving
			game_data.rpc("update_player_info",pinfo)
		else:
			# a remote player's pawn is moving
			pass
	
func _on_BottomPanel_mission_button_down():
	mission_card.show()

func _on_BottomPanel_mission_button_up():
	mission_card.hide()

func _on_BottomPanel_roll_complete(roll_counts):
		
	# ----------------------
	# handle doubles rolling logic
	
	rolled_doubles = roll_counts.size() == 2 and roll_counts[0] == roll_counts[1]
	
	if rolled_doubles and current_player != null:
		var pinfo = current_player.get_player_info()
		pinfo.money += 100# player gets $100 for rolling doubles
		
		var binfo = game_data.banker_info
		binfo.money -= 100
		
		game_data.rpc("update_player_info",pinfo)
		game_data.rpc("update_player_info",binfo)
	
	if roll_state == RollState.Move and rolled_doubles:
		doubles_sound.play()
		# player gets an extra turn when rolling doubles
		rolls_left += 1
	
	# ----------------------
	# handle normal roll logic
	
	var roll_count = 0
	for count in roll_counts:
		roll_count += count
	
	print('Rolled a %d' % roll_count)
	moving = true
	rolls_left -= 1
	if is_instance_valid(current_player):
		current_player.pawn.move(roll_count)

func _on_BottomPanel_player_panel_pressed(pinfo):
	# disable buy button if we're viewing our own properties
	var disable_buy = pinfo.net_id == game_data.my_net_id()
	# disable sell button if we're viewing other people's properties
	var disable_sell = pinfo.net_id != game_data.my_net_id()
	
	property_collection.configure(pinfo.properties,disable_buy,disable_sell)
	property_collection.popup_centered_minsize()

func _on_BottomPanel_banker_panel_pressed(pinfo):
	# you can't just buy/sell properties from the banker at random! :)
	var disable_buy = true
	var disable_sell = true
	
	property_collection.configure(pinfo.properties,disable_buy,disable_sell)
	property_collection.popup_centered_minsize()

func _on_BottomPanel_purchasable_property_reveal(pinfo):
		property_view.configure(
			true,# enable but button
			false)# disable sell button
		property_view.popup_centered_minsize()

func _on_BottomPanel_laser_fired(at_pinfo):
	pass # Replace with function body.

func _on_LobbyPopup_game_create_requested():
	game_ui.show()
	lobby.hide()

func _on_LobbyPopup_game_join_requested():
	lobby.hide()
	
	# update the ship picker UI with existing player
	ship_picker.reset()
	for pinfo in game_data.players.values():
		ship_picker.mark_ship_selected(pinfo.pawn_color,true,pinfo.name)
	ship_picker.popup_centered_minsize()
	
	# create and add a new player to the game
	var pinfo = game_data.create_player_info()
	game_data.rpc_id(SERVER_ID,"add_player",pinfo)

func _on_ShipPicker_ship_selected(color):
	game_data.rpc_id(SERVER_ID,"set_player_color",game_data.my_net_id(),color)

func _on_ShipPicker_start_game_pressed():
	game_data.rpc_id(SERVER_ID,"start_game")

func _on_LobbyPopup_about_to_show():
	game_ui.hide()

func _on_GameData_connection_changed(connected):
	var ADD_DEBUG_PLAYER = false
	
	if connected and ADD_DEBUG_PLAYER:
		var debug_pinfo = game_data.create_player_info()
		debug_pinfo.name = "Debug"
		debug_pinfo.net_id = game_data.my_net_id() + 1
		debug_pinfo.pawn_color = Colors.ShipColor.Black
		game_data.rpc_id(SERVER_ID,"add_player",debug_pinfo)
	
func _on_GameData_current_player_changed(pinfo):
	print('%s is the current player!' % pinfo.name)
	
	# set the camera's target to the current player
	current_player = get_player(pinfo.net_id)
	boardview.camera_target = current_player.pawn
	
	# enable the dice button if current player is this client
	my_turn = pinfo.net_id == game_data.my_net_id()
	clear_lost_turn = false
	if my_turn:
		roll_state = RollState.Move
		rolled_doubles = false
		rolls_left = 1
		
		# handle the lost a turn logic
		if pinfo.lost_turn:
			clear_lost_turn = true
			rolls_left -= 1
		
	# update buttons and things based on current game state
	update_ui()

func _on_GameData_player_joined(pinfo):
	var new_player = PlayerRes.instance()
	new_player.name = str(pinfo.net_id)
	new_player.connect("move_complete",self,"_on_Player_move_complete")
	$Players.add_child(new_player)
	if pinfo.pawn_color != null:
		ship_picker.mark_ship_selected(pinfo.pawn_color,true,pinfo.name)

func _on_GameData_player_color_set(pinfo):
	var player = get_player(pinfo.net_id)
	player.pawn.color = pinfo.pawn_color
	ship_picker.mark_ship_selected(pinfo.pawn_color,true,pinfo.name)

func _on_GameData_player_color_changed(pinfo, old_color):
	var player = get_player(pinfo.net_id)
	player.pawn.color = pinfo.pawn_color
	ship_picker.mark_ship_selected(old_color,false,"")
	ship_picker.mark_ship_selected(pinfo.pawn_color,true,pinfo.name)

func _on_GameData_start_game():
	# initialize some things about the players and the UI
	for net_id in game_data.players:
		var player = get_player(net_id)
		var pinfo = game_data.players[net_id]
		player.set_color(pinfo.pawn_color)
		bottom_panel.bind_player(player)
		board.add_pawn(player.pawn)
	
	# show the game UI
	ship_picker.hide()
	game_ui.show()

func _on_GameData_player_info_changed(net_id, old_pinfo, new_pinfo):
	if net_id == -1:
		banker_panel.update_pinfo(old_pinfo,new_pinfo)
	else:
		# update player panel with changed information
		for panel in bottom_panel.get_player_panels():
			if panel is PlayerPanel and panel.player != null:
				var pinfo = panel.player.get_player_info()
				
				if pinfo.net_id == net_id:
					panel.update_pinfo(old_pinfo,new_pinfo)
					break
					
		var diff = Utils.diff_pinfo(old_pinfo,new_pinfo)
		if net_id != game_data.my_net_id() and diff.spot_changed:
			# remote pawn's position has changed; update on local board
			var player : Player = get_player(net_id)
			player.pawn.advance(new_pinfo.pawn_spot)
			
		if net_id == game_data.my_net_id() and diff.mission_changed:
			# update my mission card with new mission text
			mission_card._set_mission_text(new_pinfo.mission)
	
func _on_Player_move_complete(player: Player,spot):
	var pinfo = player.get_player_info()
	if pinfo.net_id != game_data.my_net_id():
		# don't care about updating anything when remote players are moving
		return
	
	moving = false
	print('%s landed on %s' % [pinfo.name,EnumUtils.to_str(BaseSpot.Spots,spot)])
	
	if roll_state == RollState.Move and rolled_doubles:
		var pretty_str = Utils.spot_to_pretty_string(pinfo.pawn_spot)
		if pretty_str != null:
			# allow player to chose if they want to land/float on spot
			float_dialog.set_spot_string(pretty_str)
			float_dialog.popup_centered_minsize()
		else:
			# spots that don't convert over to a pretty string are not worth
			# asking to land on landing on (ie. NeptuneNull, UranusNull, etc.)
			handle_land(pinfo)
	else:
		handle_land(pinfo)
		
	# update buttons and things based on current game state
	update_ui()
	
func _on_NextTurnButton_pressed():
	if clear_lost_turn:
		clear_lost_turn = false
		var pinfo = current_player.get_player_info()
		pinfo.lost_turn = false
		game_data.update_player_info(pinfo)
	
	notify_turn_complete()
	next_turn_button.disabled = true

func _on_LaserButton_pressed():
	pass # Replace with function body.

func _on_PropertyView_closed():
	needs_to_purchase_property = false
	property_view.hide()
	# update buttons and things based on current game state
	update_ui()

func _on_PropertyView_buy():
	needs_to_purchase_property = false
	property_view.hide()
	
	# notify server that player bought the property
	game_data.rpc_id(
		SERVER_ID,"buy_property",
		game_data.my_net_id(),property_view.prop_spot)
	
	# update buttons and things based on current game state
	update_ui()

func _on_PropertyView_sell():
	# update buttons and things based on current game state
	update_ui()

func _on_DebugButton_pressed():
	debug_panel.rect_size = debug_panel.rect_min_size
	debug_panel.visible = true

func _on_DebugPanel_change_spot(net_id, new_spot):
	var player : Player = get_player(net_id)
	player.pawn.advance(new_spot)

func _on_DebugPanel_modify_lasers(net_id, amount):
	var pinfo = game_data.get_player_info(net_id)
	pinfo.lasers += amount
	game_data.rpc("update_player_info",pinfo)

func _on_DebugPanel_modify_money(net_id, amount):
	var pinfo = game_data.get_player_info(net_id)
	pinfo.money += amount
	game_data.rpc("update_player_info",pinfo)

func _on_DebugPanel_fixed_roll(net_id, roll_count):
	var player : Player = get_player(net_id)
	player.pawn.move(roll_count)

func _on_DebugPanel_doubles_roll(net_id, roll_counts):
	if current_player != null and current_player.get_player_info().net_id == net_id:
		_on_BottomPanel_roll_complete(roll_counts)

func _on_FloatDialog_pressed_float():
	float_dialog.hide()

func _on_FloatDialog_pressed_land():
	float_dialog.hide()
	
	# get this player's information and handle the land event
	var pinfo = game_data.get_player_info(game_data.my_net_id())
	handle_land(pinfo)
	
func _on_Dialog_visibility_changed():
	var any_visible = false
	for dialog in game_dialogs.get_children():
		if dialog.visible:
			any_visible = true
			break
	shade.visible = any_visible
