extends Sprite

signal roll_complete()

const DiceTextures = [
	preload("res://imgs/d1.png"),
	preload("res://imgs/d2.png"),
	preload("res://imgs/d3.png"),
	preload("res://imgs/d4.png"),
	preload("res://imgs/d5.png"),
	preload("res://imgs/d6.png")
]

export var roll_time = 2
export var roll_count = 10

var value = 1 setget _set_value
var _count_left = 0
var _rng = RandomNumberGenerator.new()

func _ready():
	_rng.randomize()

func _set_value(dice_value):
	if dice_value > 0 and dice_value < 7:
		value = dice_value
		texture = DiceTextures[dice_value - 1]

func roll():
	_count_left = roll_count
	$Timer.start(float(roll_time) / roll_count)

func roll_complete():
	return _count_left == 0

func _on_Timer_timeout():
	_count_left -= 1
	self.value = _rng.randi_range(1,6)
	if roll_complete():
		$Timer.stop()
		emit_signal("roll_complete")
