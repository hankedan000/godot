tool
extends TextureButton
class_name PropertyCard

signal property_pressed(prop_spot)

export (BaseSpot.Spots) var prop_spot = BaseSpot.Spots.Moon setget _set_prop_spot

var ready = false

func _ready():
	ready = true

func _set_prop_spot(value):
	prop_spot = value
	if not ready:
		yield(self,"ready")
		
	texture_normal = Data.get_property_texture(value)

func _on_PropertyCard_pressed():
	emit_signal("property_pressed",prop_spot)
