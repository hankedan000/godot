extends PopupDialog

signal pressed_float()
signal pressed_land()

onready var rich_text = $MarginContainer/VBoxContainer/RichTextLabel
var fmt_str = ""

func _ready():
	fmt_str = rich_text.text
	if get_parent() is Viewport:
		popup_centered_minsize()

func set_spot_string(spot_str):
	rich_text.text = fmt_str.replace("<spot_string>",spot_str)

func _on_Float_pressed():
	emit_signal("pressed_float")

func _on_Land_pressed():
	emit_signal("pressed_land")
