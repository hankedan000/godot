extends Popup

export var text : String = "" setget _set_mission_text

var ready = false

func _ready():
	ready = true
	
func _set_mission_text(value):
	text = value.to_upper()
	if not ready:
		yield(self,"ready")
	
	$Label.text = text

func _on_MissionCardView_visibility_changed():
	if not ready:
		yield(self,"ready")
	
	if visible:
		var mission_text = ""
		if GlobalGameData.inst:
			mission_text = GlobalGameData.inst.get_my_mission()
		_set_mission_text(mission_text)
