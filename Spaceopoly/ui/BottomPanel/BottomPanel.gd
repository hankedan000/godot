extends PanelContainer

signal mission_button_pressed()
signal mission_button_down()
signal mission_button_up()
signal roll_complete(roll_counts)
signal player_panel_pressed(pinfo)
signal banker_panel_pressed(pinfo)
signal purchasable_property_reveal(pinfo)
signal laser_fired(at_pinfo)

var player_count = 0

func _ready():
	for child in $HBox/PlayerBox.get_children():
		if child is PlayerPanel:
			child.visible = false
			child.connect("purchase_property_pressed",self,"_on_PlayerPanel_purchase_property_pressed")
			child.connect("fire_laser_pressed",self,"_on_PlayerPanel_fire_laser_pressed")

func bind_player(player: Player):
	var pinfo = player.get_player_info()
	
	if pinfo == null:
		print("ERROR: 'pinfo' for Player '%s' is null" % player)
		return
	
	var turn_order = pinfo.turn_order
	if turn_order < 0 or turn_order > 3:
		print("ERROR: invalid turn_order of %d" % turn_order)
		return
	
	# position player in the bottom panel based on its turn_order
	var player_panel = get_node("HBox/PlayerBox/Player%dPanel" % (turn_order+1))
	if player_panel is PlayerPanel:
		player_panel.bind_to_player(player)
		player_panel.visible = true
		
func get_player_panels():
	return $HBox/PlayerBox.get_children()

func _on_DiceButton_roll_complete(roll_counts):
	emit_signal("roll_complete",roll_counts)

func _on_MissionButton_button_down():
	emit_signal("mission_button_down")

func _on_MissionButton_button_up():
	emit_signal("mission_button_up")

func _on_PlayerPanel_pressed(pinfo):
	emit_signal("player_panel_pressed",pinfo)

func _on_BankerPanel_pressed(pinfo):
	emit_signal("banker_panel_pressed",pinfo)
	
func _on_PlayerPanel_purchase_property_pressed(pinfo):
	emit_signal("purchasable_property_reveal",pinfo)
	
func _on_PlayerPanel_fire_laser_pressed(pinfo):
	emit_signal("laser_fired",pinfo)
