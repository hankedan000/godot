extends PopupDialog

signal buy()
signal sell()
signal closed()

onready var card = $MarginContainer/HBox/PropertyCard
onready var buy_button = $MarginContainer/HBox/Buttons/BuyButton
onready var sell_button = $MarginContainer/HBox/Buttons/SellButton

var prop_spot = BaseSpot.Spots.Moon setget _set_prop_spot
var ready = false

func _ready():
	ready = true

func configure(disable_buy,disable_sell):
	buy_button.disabled = not disable_buy
	sell_button.disabled = not disable_sell

func _set_prop_spot(value):
	prop_spot = value
	_wait_for_ready()
	var prop_info = Data.get_property_info(value)
	if prop_info != null:
		card.prop_spot = prop_spot
		buy_button.text = "Buy ($%d)" % prop_info["price"]
		
func _wait_for_ready():
	if not ready:
		yield(self,"ready")

func _on_CloseButton_pressed():
	hide()
	emit_signal("closed")

func _on_BuyButton_pressed():
	emit_signal("buy")

func _on_SellButton_pressed():
	emit_signal("sell")
