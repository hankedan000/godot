extends PopupDialog

signal ship_selected(color)
signal start_game_pressed()

onready var name1 = $Margin/VBoxContainer/Grid/Name1
onready var name2 = $Margin/VBoxContainer/Grid/Name2
onready var name3 = $Margin/VBoxContainer/Grid/Name3
onready var name4 = $Margin/VBoxContainer/Grid/Name4
onready var ship1 = $Margin/VBoxContainer/Grid/Ship1
onready var ship2 = $Margin/VBoxContainer/Grid/Ship2
onready var ship3 = $Margin/VBoxContainer/Grid/Ship3
onready var ship4 = $Margin/VBoxContainer/Grid/Ship4
onready var start_game = $Margin/VBoxContainer/StartGame

func _ready():
	if get_parent() is Viewport:
		popup_centered_minsize()
	
func reset():
	_set_button_state(COLORS.ShipColor.Purple,false,"")
	_set_button_state(COLORS.ShipColor.Yellow,false,"")
	_set_button_state(COLORS.ShipColor.Blue,false,"")
	_set_button_state(COLORS.ShipColor.Black,false,"")
	start_game.disabled = true
	
func mark_ship_selected(color,selected,player_name):
	_set_button_state(color,selected,player_name)
	
	var select_count = 0
	for child in $Margin/VBoxContainer/Grid.get_children():
		if child is TextureButton and child.disabled:
			select_count += 1
			
	if select_count >= 1:
		start_game.disabled = false
		
func _set_button_state(color,selected,player_name):
	match color:
		COLORS.ShipColor.Purple:
			ship1.disabled = selected
			name1.text = player_name
		COLORS.ShipColor.Yellow:
			ship2.disabled = selected
			name2.text = player_name
		COLORS.ShipColor.Blue:
			ship3.disabled = selected
			name3.text = player_name
		COLORS.ShipColor.Black:
			ship4.disabled = selected
			name4.text = player_name

func _on_Ship1_pressed():
	emit_signal("ship_selected",COLORS.ShipColor.Purple)

func _on_Ship2_pressed():
	emit_signal("ship_selected",COLORS.ShipColor.Yellow)

func _on_Ship3_pressed():
	emit_signal("ship_selected",COLORS.ShipColor.Blue)

func _on_Ship4_pressed():
	emit_signal("ship_selected",COLORS.ShipColor.Black)

func _on_StartGame_pressed():
	emit_signal("start_game_pressed")
