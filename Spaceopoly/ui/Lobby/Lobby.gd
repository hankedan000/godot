extends PopupDialog

signal game_create_requested()
signal game_join_requested()

onready var profile_name = $MarginContainer/VBox/ProfileName
onready var profile_editor = $ProfileEditor
onready var edit_profile = $MarginContainer/VBox/EditProfile
onready var create_game = $MarginContainer/VBox/CreateGame
onready var join_game = $MarginContainer/VBox/JoinGame
onready var connecting_label = $Overlay/ConnectingLabel

func _ready():
	GlobalGameData.connect(
		"instance_set",
		self,"_on_GlobalGameData_instance_set")
	if get_parent() is Viewport:
		popup_centered_minsize()

func update_ui_with_profile(profile):
	profile_name.text = "Welcome %s!" % profile.name
	edit_profile.text = "Edit Profile"
	if GlobalGameData.inst and GlobalGameData.inst.connected:
		enable_server_buttons(true)
	
func enable_server_buttons(enable):
	create_game.disabled = not enable
	join_game.disabled = not enable
	connecting_label.visible = not enable
	
func _on_GlobalGameData_instance_set():
	GlobalGameData.inst.connect(
		"connection_changed",
		self,"_on_game_data_connection_changed")
	GlobalGameData.inst.connect(
		"connection_failed",
		self,"_on_game_data_connection_failed")
	if GlobalGameData.inst.has_profile():
		update_ui_with_profile(GlobalGameData.inst.get_my_profile())
	
func _on_EditProfile_pressed():
	if GlobalGameData.inst and GlobalGameData.inst.has_profile():
		profile_editor.load_existing_profile(GlobalGameData.inst.get_my_profile())
	profile_editor.popup_centered_minsize()

func _on_ProfileEditor_profile_save_request(profile):
	update_ui_with_profile(profile)
	if GlobalGameData.inst:
		GlobalGameData.inst.set_profile(profile)
		GlobalGameData.inst.save()

func _on_CreateGame_pressed():
	emit_signal("game_create_requested")

func _on_JoinGame_pressed():
	emit_signal("game_join_requested")
	
func _on_game_data_connection_changed(connected):
	if connected and GlobalGameData.inst.has_profile():
		enable_server_buttons(true)
	else:
		enable_server_buttons(false)
	
func _on_game_data_connection_failed():
	connecting_label.text = "Connection failed! Please retry later."
	enable_server_buttons(false)
