extends Node2D
class_name RisingLabel

export var text = "<text>" setget _set_text
export var color = Color(1,1,1,1) setget _set_color

onready var label = $Label
onready var tween = $Tween

var ready = false

func _ready():
	if get_parent() is Viewport:
		start(Vector2(0,-50),2)
	ready = true

func _set_text(value):
	text = value
	if not ready:
		yield(self,"ready")
	label.text = value

func _set_color(value):
	color = value
	if not ready:
		yield(self,"ready")
	label.set("custom_colors/font_color",value)

func start(distance, duration):
	var start = global_position
	var stop = start + distance
	tween.interpolate_property(
		self,"global_position",
		start,stop,duration,
		Tween.TRANS_LINEAR,Tween.EASE_IN_OUT
	)
	tween.interpolate_property(
		self,"modulate",
		Color(1,1,1,1),Color(1,1,1,0),duration,
		Tween.TRANS_LINEAR,Tween.EASE_IN_OUT
	)
	tween.start()

func _on_Tween_tween_all_completed():
	queue_free()
