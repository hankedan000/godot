tool
extends PanelContainer
class_name PlayerPanel

signal pressed(pinfo)
signal purchase_property_pressed(pinfo)
signal fire_laser_pressed(pinfo)

const SpaceBoy = preload("res://imgs/space_boy.png")
const RisingLabelScene = preload("res://ui/PlayerPanel/RisingLabel.tscn")

onready var name_label = $VBoxContainer/Name
onready var pawn_sprite = $VBoxContainer/HBox/PawnSprite
onready var money_label = $VBoxContainer/HBox/VBox/MoneyLabel
onready var laser_label = $VBoxContainer/HBox/VBox/LaserLabel
onready var property_label = $VBoxContainer/HBox/VBox/PropertyLabel
onready var property_button = $CanvasLayer/PurchasePorpertyButton
onready var purchase_label = $CanvasLayer/PurchasePorpertyButton/PurchaseLabel
onready var laser_button = $CanvasLayer/LaserButton
onready var lost_turn = $CanvasLayer/LostTurn

var ready = false
var player : Player = null

func _ready():
	ready = true
	set_property_button_visible(false)
	set_laser_button_visible(false)
	set_lost_turn_visible(false)
	
func bind_to_player(player_instance: Player):
	player = player_instance
	var pinfo = player.get_player_info()
	
	if pinfo != null:
		name_label.text = pinfo.name
		if pinfo.is_bank:
			pawn_sprite.texture = SpaceBoy
		else:
			pawn_sprite.texture = player.pawn.get_pawn_texture()
		money_label.text = "$%d" % pinfo.money
		laser_label.text = "%d" % pinfo.lasers
		property_label.text = "%d" % pinfo.properties.size()
		
func update_pinfo(old_pinfo,new_pinfo):
	money_label.text = "$%d" % new_pinfo.money
	laser_label.text = "%d" % new_pinfo.lasers
	property_label.text = "%d" % new_pinfo.properties.size()
	set_lost_turn_visible(new_pinfo.lost_turn)
	
	var diff = Utils.diff_pinfo(old_pinfo,new_pinfo)
	if int(diff.money) != 0:
		var rise : RisingLabel = RisingLabelScene.instance()
		if diff.money > 0:
			rise.color = Color(0,1,0)
			rise.text = ("+%d" % diff.money)
		else:
			rise.color = Color(1,0,0)
			rise.text = ("%d" % diff.money)
		add_child(rise)
		# make sure we set the position after add_child() because parenting
		# container nodes can override what we are specifying here
		rise.global_position = money_label.rect_global_position + Vector2(0,-10)
		rise.start(Vector2(0,-50),2.0)
		
		
func set_property_button_visible(visible):
	if visible:
		property_button.show()
	else:
		property_button.hide()
	
func set_laser_button_visible(visible):
	if visible:
		laser_button.show()
	else:
		laser_button.hide()
	
func set_lost_turn_visible(visible):
	if visible:
		lost_turn.show()
	else:
		lost_turn.hide()

func _wait_till_ready():
	if not ready:
		yield(self,"ready")

func _on_TextureButton_pressed():
	var pinfo = null
	if is_instance_valid(player):
		pinfo = player.get_player_info()
		
	if pinfo != null:
		emit_signal("pressed",pinfo)
	else:
		print("ERROR: Player panel pressed, but pinfo is null")

func _on_PurchasePorpertyButton_pressed():
	emit_signal("purchase_property_pressed",player.get_player_info())

func _on_LaserButton_pressed():
	emit_signal("fire_laser_pressed",player.get_player_info())

func _on_Timer_timeout():
	purchase_label.visible = property_button.visible and not purchase_label.visible
