extends ViewportContainer

onready var viewport = $Viewport
onready var camera = $Viewport/Camera2D
onready var board = $Viewport/Board
onready var amalthia_dialog = $AmalthiaDialog
onready var view_button = $HUD/BoardViewButton

enum CameraMode {
	TrackPlayer,
	ShowBoard
}

var camera_mode = CameraMode.TrackPlayer setget _set_camera_mode
var camera_target = null
var camera_dislocated = false

# number of pixel before considering a mouse motion a drag
const DRAG_THRESHOLD = 20
var mouse_down = false
var mouse_down_position = Vector2()
var dragging = false

func _ready():
	camera.current = true
	_set_camera_mode(camera_mode)
	_update_camera_view_button()
	
func get_board() -> Board:
	return board

func _get_next_camera_mode(value):
	var options = CameraMode.values()
	var curr_idx = options.find(value)
	var next_idx = (curr_idx + 1) % options.size()
	return options[next_idx]
	
func _update_camera_view_button():
	if camera_dislocated:
		view_button.text = "Restore View"
	else:
		match camera_mode:
			CameraMode.ShowBoard:
				view_button.text = "Full Board"
			CameraMode.TrackPlayer:
				view_button.text = "Track Player"
			_:
				print("WARN: Unhandled camera mode!")

func _set_camera_mode(value):
	camera_mode = value
	_update_camera_view_button()
	match value:
		CameraMode.TrackPlayer:
			set_process(true)
			camera.zoom = Vector2(2,2)
		CameraMode.ShowBoard:
			set_process(false)
			camera.offset = board.sprite.position
			camera.zoom = Vector2(4,4)

func _process(delta):
	if is_instance_valid(camera_target) and not camera_dislocated:
		camera.offset = camera_target.global_position
		
func _drag_complete(drag_vector: Vector2):
	pass
		
func _on_ShowBoardButton_toggled(button_pressed):
	if button_pressed:
		_set_camera_mode(CameraMode.ShowBoard)
	else:
		_set_camera_mode(CameraMode.TrackPlayer)
		
func _input(event):
	if event is InputEventMouse:
		var board_event = event.duplicate()
		# inverse -> rotation + translation
		# affine_inverse -> scale + rotation + translation
		var transform = viewport.canvas_transform.affine_inverse()
		board_event.position = transform * event.position
		viewport.input(board_event)

func _on_Board_ask_for_amalthia():
	amalthia_dialog.popup_centered_minsize()

func _on_AmalthiaDialog_pressed_amalthia():
	board.accept_amalthia(true)

func _on_AmalthiaDialog_pressed_laser():
	board.accept_amalthia(false)

func _on_BoardView_gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			# pressed mouse button
			mouse_down = true
			mouse_down_position = event.position
		else:
			# released mouse button
			if dragging:
				_drag_complete(event.position - mouse_down_position)
				
			# clear flags for next time
			mouse_down = false
			dragging = false
		
	if event is InputEventMouseMotion:
		if mouse_down:
			var drag_vector = event.position - mouse_down_position
			
			# check to see if we've crossed the "drag" threshold
			if not dragging and drag_vector.length() > DRAG_THRESHOLD:
				dragging = true
				
		if dragging:
			camera_dislocated = true
			_update_camera_view_button()
			var camera_pan = event.relative
			camera_pan.x *= camera.zoom.x
			camera_pan.y *= camera.zoom.y
			
			camera.offset -= camera_pan

func _on_BoardViewButton_pressed():
	if camera_dislocated:
		camera_dislocated = false
		# trick it into restoring camera mode by reassigning current value
		_set_camera_mode(camera_mode)
	else:
		var next_camera_mode = _get_next_camera_mode(camera_mode)
		_set_camera_mode(next_camera_mode)
