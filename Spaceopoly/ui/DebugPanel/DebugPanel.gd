extends WindowDialog

signal change_spot(net_id,new_spot)
signal modify_money(net_id,amount)
signal modify_lasers(net_id,amount)
signal fixed_roll(net_id,roll_count)
signal doubles_roll(net_id,roll_counts)

onready var spot_options = $Margin/VBox/EditSpot/SpotOptions

func _ready():
	# configure the spot selector
	for enum_str in BaseSpot.Spots.keys():
		spot_options.add_item(enum_str)
		
	# show window when running in standalone scene
	if get_parent() is Viewport:
		popup_centered_minsize()
		
func _get_player_net_id():
	if GlobalGameData.inst != null:
		return GlobalGameData.inst.my_net_id()
	else:
		print_debug("WARN: gamedata is null returning net_id of -1")
		return -1
		
func _on_EditSpotApply_pressed():
	var id = spot_options.get_selected_id()
	var idx = spot_options.get_item_index(id)
	var spot_str = spot_options.get_item_text(idx)
	var spot = EnumUtils.to_enum(BaseSpot.Spots,spot_str)
	emit_signal("change_spot",_get_player_net_id(),spot)

func _on_EditMoney_applied(text):
	var amount = int(text)
	emit_signal("modify_money",_get_player_net_id(),amount)

func _on_EditLasers_applied(text):
	var amount = int(text)
	emit_signal("modify_lasers",_get_player_net_id(),amount)

func _on_FixedRoll_applied(text):
	var roll_count = int(text)
	emit_signal("fixed_roll",_get_player_net_id(),roll_count)

func _on_DoublesRoll_applied(text):
	var roll_count = int(text)
	emit_signal("doubles_roll",_get_player_net_id(),[roll_count,roll_count])
