tool
extends HBoxContainer

signal applied(text)

export var label_text = "<text>" setget _set_label_text
export var default_value = "100" setget _set_default_value
export var button_text = "Apply" setget _set_button_text
export var apply_on_enter = false

onready var label = $Label
onready var line_edit = $LineEdit
onready var button = $Button

var _ready = false

func _ready():
	_ready = true

func _set_label_text(value):
	label_text = value
	_wait_for_ready()
	label.text = value
	
func _set_default_value(value):
	default_value = value
	_wait_for_ready()
	line_edit.text = value
	
func _set_button_text(value):
	button_text = value
	_wait_for_ready()
	button.text = value
	
func _wait_for_ready():
	if not _ready():
		yield(self,"ready")

func _on_LineEdit_text_entered(new_text):
	if apply_on_enter:
		emit_signal("applied",new_text)

func _on_Button_pressed():
	emit_signal("applied",line_edit.text)
