extends PanelContainer
class_name PropertyList

signal property_pressed(prop_spot)

export var CardScene : PackedScene
export var card_min_y = 250 setget _set_card_min_y

onready var grid = $Scroll/Grid
var ready = false

func _ready():
	ready = true
	_fill_default_cards()
	_update_min_y(card_min_y)
	
func update_props_to_show(props_to_show):
	for child in grid.get_children():
		if child is PropertyCard:
			if child.prop_spot in props_to_show:
				child.show()
			else:
				child.hide()
	
func _set_card_min_y(value):
	card_min_y = value
	
	if not ready:
		yield(self,"ready")
		
	_update_min_y(card_min_y)
	
func _fill_default_cards():
	for child in grid.get_children():
		child.queue_free()
		
	for prop in Data.get_init_properties(true):
		var new_card = CardScene.instance()
		new_card.size_flags_horizontal = SIZE_EXPAND_FILL
		new_card.size_flags_vertical = SIZE_EXPAND_FILL
		new_card.prop_spot = prop
		new_card.connect(
			"property_pressed",
			self,"_on_PropertyCard_property_pressed")
		grid.add_child(new_card)
	
func _update_min_y(min_y):
	for child in grid.get_children():
		if child is Control:
			child.rect_min_size.y = min_y

func _on_PropertyCard_property_pressed(prop_spot):
	emit_signal("property_pressed",prop_spot)
