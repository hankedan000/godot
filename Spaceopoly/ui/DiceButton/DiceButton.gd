extends TextureButton

signal roll_complete(roll_counts)

onready var blink_timer = $BlinkTimer
onready var roll_label = $RollLabel
onready var dice_sound = $DiceSound

export var disable_after_roll = true
export var blink_rate_hz = 2

func _ready():
	enable_dice(not disabled)
	
func enable_dice(enabled):
	disabled = not enabled
	$RollLabel.visible = enabled
	if enabled:
		blink_timer.start(1.0 / blink_rate_hz)
	else:
		blink_timer.stop()
	
func roll():
	$Dice1.roll()
	$Dice2.roll()
	dice_sound.play(0.6)
	if disable_after_roll:
		enable_dice(false)
	
func get_roll_count():
	return $Dice1.value + $Dice2.value
	
func get_roll_counts():
	return [$Dice1.value,$Dice2.value]

func _on_Dice1_roll_complete():
	if $Dice2.roll_complete():
		emit_signal("roll_complete",get_roll_counts())

func _on_Dice2_roll_complete():
	if $Dice1.roll_complete():
		emit_signal("roll_complete",get_roll_counts())

func _on_DiceButton_pressed():
	roll()

func _on_BlinkTimer_timeout():
	roll_label.visible = not roll_label.visible
