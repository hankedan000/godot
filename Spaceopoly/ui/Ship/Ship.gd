tool
extends PathFollow2D
class_name Ship

signal became_current(ship)
signal changed_spots(ship,prev_spot,curr_spot)
signal slide_begin(ship,from_spot,to_spot)
signal slide_complete(ship)
signal move_complete(ship)
signal ask_for_amalthia(ship)
signal leave_orbit(ship,curr_spot)
signal enter_orbit(ship,curr_spot)

export (Colors.ShipColor) var color = Colors.ShipColor.Purple setget _set_color
export var move_time = 0.3

onready var roll_indicator = $RollIndicator
onready var roll_label = $RollIndicator/Label

var current = false setget _set_current
var spot = BaseSpot.Spots.Earth setget _set_spot
var board = null
var ready = false
var _advancing = false
var _advance_to_spot = null
var _roll_count_left = 0
var _roll_indicator_offset = Vector2()

func _ready():
	$Sprite.texture = COLORS.get_ship_texture(color)
	ready = true
	_roll_indicator_offset = roll_indicator.global_position - global_position
	roll_indicator.hide()
	
func _process(delta):
	if ready:
		roll_indicator.global_rotation_degrees = 0
		roll_indicator.global_position = global_position + _roll_indicator_offset
	
###############################################################################
# SETTERS/GETTERS
###############################################################################

func _set_color(value):
	color = value
	if ready:
		$Sprite.texture = get_pawn_texture()
			
func _set_current(value):
	current = value
	emit_signal("became_current",self)
	
func _set_spot(value):
	var prev_spot = spot
	spot = value
	emit_signal("changed_spots",self,prev_spot,spot)
	
###############################################################################
# PUBLIC METHODS
###############################################################################

func bind_to_board(board_instance):
	board = board_instance
	
func bound_to_board():
	return is_instance_valid(board)
	
func get_pawn_texture():
	return COLORS.get_ship_texture(color)

func advance(to_spot):
	_advancing = true
	_advance_to_spot = to_spot
	_slide_to_next_spot()

func move(roll_count):
	if roll_count > 0:
		roll_indicator.show()
		roll_label.text = str(roll_count)
		_roll_count_left = roll_count
		_slide_to_next_spot()
		
func accept_amalthia(accepted):
	if spot == BaseSpot.Spots.Ganymede:
		if accepted:
			emit_signal("leave_orbit",self,spot)
		else:
			emit_signal("enter_orbit",self,spot)
		_slide_to_next_spot(BaseSpot.Spots.Amalthia)
	else:
		print("Can't call accept_amalthia when on %s" % EnumUtils.to_str(BaseSpot.Spots,spot))
		
func offset_from_path(hv_offset):
	$Tween.interpolate_method(
		self,"_update_hv_offset",
		Vector2(h_offset,v_offset),hv_offset,
		move_time,
		Tween.TRANS_LINEAR,Tween.EASE_IN_OUT
	)
	$Tween.start()

func slide(next_offset):
	var parent = get_parent()
	if parent is Path2D:
		var max_length = parent.curve.get_baked_length()
		if next_offset < offset:
			# don't make pawn go backward when looping around path
			next_offset = max_length + next_offset
	
	$Tween.interpolate_property(
		self,"offset",
		offset,next_offset,
		move_time,
		Tween.TRANS_LINEAR,Tween.EASE_IN_OUT
	)
	$Tween.start()
	
func _update_hv_offset(hv_offset):
	h_offset = hv_offset.x
	v_offset = hv_offset.y
	
func _slide_to_next_spot(next_spot_override=null):
	var next_spot = next_spot_override
	if next_spot == null:
		if _advancing:
			next_spot = Utils.next_spot_advance_to(spot,_advance_to_spot)
		else:
			next_spot = Utils.next_spot(spot,_roll_count_left,false)
	
	if next_spot in [
		BaseSpot.Spots.Amalthia,
		BaseSpot.Spots.UranusGravity1,
		BaseSpot.Spots.SaturnGravity1,
		BaseSpot.Spots.NeptuneGravity1]:
		emit_signal("leave_orbit",self,spot)
	elif next_spot in [
		BaseSpot.Spots.Laser,
		BaseSpot.Spots.FS4,
		BaseSpot.Spots.Titan,
		BaseSpot.Spots.Nereid]:
		emit_signal("enter_orbit",self,spot)
	
	var from_spot = spot
	if bound_to_board() and (_roll_count_left > 0 or _advancing):
		_roll_count_left = max(0,_roll_count_left - 1)
		spot = next_spot
		var next_location = board.get_spot_location(next_spot)
		var parent = get_parent()
		if parent is Path2D:
			var next_offset = parent.curve.get_closest_offset(next_location)
			slide(next_offset)
	
	if not bound_to_board() and (_roll_count_left > 0 or _advancing):
		_roll_count_left = max(0,_roll_count_left - 1)
		spot = next_spot
		_slide_to_next_spot()
	
	if from_spot != spot:
		emit_signal("slide_begin",self,from_spot,next_spot)

func _on_Tween_tween_completed(object, key):
	if key == ":offset":
		emit_signal("slide_complete",self)
		if _advancing and spot != _advance_to_spot:
			_slide_to_next_spot()
		elif _roll_count_left > 0:
			print("%s %d" % [EnumUtils.to_str(BaseSpot.Spots,spot),_roll_count_left])
			roll_label.text = str(_roll_count_left)
			if spot == BaseSpot.Spots.Ganymede and _roll_count_left == 1:
				emit_signal("ask_for_amalthia",self)
			else:
				_slide_to_next_spot()
		else:
			print('move complete! roll count is %d' % _roll_count_left)
			_roll_count_left = 0
			_advancing = false
			roll_indicator.hide()
			emit_signal("move_complete",self)
	elif key == ":_update_hv_offset":
		pass
	else:
		print("WARN: unhandled tween completion for key '%s'" % key)
