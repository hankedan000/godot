extends TextureButton
class_name SpotControl

onready var spot = $BaseSpot
onready var own_indicator = $OwnIndicator
onready var hover_indicator = $HoverIndicator

func _ready():
	own_indicator.position = rect_size / 2
	hover_indicator.position = rect_size / 2
	own_indicator.hide()
	hover_indicator.hide()

func get_spot_enum():
	# attempt to convert name to Spots enum
	var spot_enum = EnumUtils.to_enum(BaseSpot.Spots,name)
	if spot_enum == null:
		return BaseSpot.Spots.NULL
	return spot_enum
	
func get_center():
	return rect_global_position + rect_size / 2
	
func enable_owned_indicator(enabled, color = Color(1,1,1)):
	own_indicator.modulate = color
	own_indicator.visible = enabled
	
# called when board is reset
func reset():
	pass
	
# called when the player's pawn touches the spot. note, this will be called
# right before the on_player_landed callback
func on_player_touched(pinfo):
	pass

# called when the player's pawn lands on the spot as a final destination
func on_player_landed(pinfo):
	pass

func _on_SpotControl_mouse_entered():
	if not disabled:
		hover_indicator.show()

func _on_SpotControl_mouse_exited():
	if not disabled:
		hover_indicator.hide()
