extends CanvasLayer
class_name Board

signal ask_for_amalthia()
signal pawn_begin_move_to(pawn,move_to)

# used to debug mouse position translation
export var show_mouse_position = false

onready var sprite = $BoardSprite
onready var mouse_sprite = $DebugMouseSprite
onready var path_main = $Paths/Main
onready var path_jupiter = $Paths/Jupiter
onready var path_uranus = $Paths/Uranus
onready var path_saturn = $Paths/Saturn
onready var path_neptune = $Paths/Neptune
var _spot_nodes = {}
var ready = false
var _curr_moving_pawn : Ship = null
var _pawns = []
var _pawn_clusters = {}

func _ready():
	# I keep these hidden because the paths slow down the editor
	for path in $Paths.get_children():
		path.visible = true
	
	for spot in $Spots.get_children():
		var spot_enum = EnumUtils.to_enum(BaseSpot.Spots,spot.name)
		_spot_nodes[spot_enum] = spot
		
	# reset the board's state
	reset_board()
	
	# only handle input events when show_mouse_posiiton is enabled
	mouse_sprite.visible = show_mouse_position
	set_process_input(show_mouse_position)
		
	ready = true
		
func _input(event):
	if event is InputEventMouseMotion:
		mouse_sprite.position = event.position
	
###############################################################################
# PUBLIC METHODS
###############################################################################
		
func get_spot_node(spot_enum) -> SpotControl:
	return _spot_nodes[spot_enum]
	
func add_pawn(pawn):
	# don't both adding the pawn again if it's already added to the board
	if _pawns.has(pawn):
		return
	
	_pawns.append(pawn)
	path_main.add_child(pawn)
	pawn.bind_to_board(self)
	if pawn.current:
		emit_signal("current_pawn_changed",pawn)
	pawn.connect("changed_spots",self,"_on_pawn_changed_spots")
	pawn.connect("slide_begin",self,"_on_pawn_slide_begin")
	pawn.connect("slide_complete",self,"_on_pawn_slide_complete")
	pawn.connect("ask_for_amalthia",self,"_on_pawn_ask_for_amalthia")
	pawn.connect("leave_orbit",self,"_on_pawn_leave_orbit")
	pawn.connect("enter_orbit",self,"_on_pawn_enter_orbit")
	_snap_pawn_to_nearest_offset(pawn,pawn.spot)
	_adjust_pawn_clustering()# spread out pawns on the same spot if necessary
	
func get_pawn(color) -> Ship:
	for pawn in _pawns:
		if pawn.color == color:
			return pawn
	return null
		
func get_spot_location(spot_enum):
	var spot = get_spot_node(spot_enum)
	return spot.get_center()
	
func accept_amalthia(accept):
	if is_instance_valid(_curr_moving_pawn):
		_curr_moving_pawn.accept_amalthia(accept)

func clear_property_indicators():
	for prop_spot in _spot_nodes:
		_spot_nodes[prop_spot].enable_owned_indicator(false)
	
func show_property_indicators(properties,color = Color(1,1,1)):
	for prop_spot in _spot_nodes:
		if prop_spot in properties:
			_spot_nodes[prop_spot].enable_owned_indicator(true,color)
		
func reset_board():
	_curr_moving_pawn = null
	for spot in _spot_nodes.values():
		spot.reset()
		
###############################################################################
# PRIVATE METHODS
###############################################################################
	
func _snap_pawn_to_nearest_offset(pawn,spot):
	var spot_node = get_spot_node(spot)
	var path = pawn.get_parent()
	if spot_node is SpotControl and path is Path2D:
		var offset = path.curve.get_closest_offset(spot_node.get_center())
		pawn.offset = offset
		
func _adjust_pawn_clustering():
	# regroup all pawns based on their current spots
	_pawn_clusters = {}
	for pawn in _pawns:
		if _pawn_clusters.has(pawn.spot):
			_pawn_clusters[pawn.spot].push_back(pawn)
		else:
			_pawn_clusters[pawn.spot] = [pawn]
	
	var CLUSTER_OFFSETS = {
		1: [Vector2(0,0)],
		2: [Vector2(0,-75),Vector2(0,75)],
		3: [Vector2(0,-75),Vector2(0,75),Vector2(75,0)],
		4: [Vector2(75,-75),Vector2(75,75),Vector2(-75,75),Vector2(-75,-75)]
	}	
	for spot in _pawn_clusters:
		var cluster = _pawn_clusters[spot]
		if not CLUSTER_OFFSETS.has(cluster.size()):
			print("ERROR: CLUSTER_OFFSETS doesn't have entry for cluster size of %d" % cluster.size())
			continue
			
		var offsets = CLUSTER_OFFSETS[cluster.size()]
		for i in range(cluster.size()):
			var pawn = cluster[i]
			if pawn is Ship:
				pawn.offset_from_path(offsets[i])
	
###############################################################################
# SIGNAL HANDLERS
###############################################################################

func _on_pawn_changed_spots(pawn,prev_spot,curr_spot):
	_curr_moving_pawn = pawn
	var spot = get_spot_node(curr_spot)
	var path = pawn.get_parent()
	if spot is SpotControl and path is Path2D:
		var offset = path.curve.get_closest_offset(spot.get_center())
		pawn.slide(offset)
	_adjust_pawn_clustering()# spread out pawns on the same spot if necessary

func _on_pawn_slide_begin(pawn,from_spot,to_spot):
	emit_signal("pawn_begin_move_to",pawn,to_spot)
	_adjust_pawn_clustering()# spread out pawns on the same spot if necessary

func _on_pawn_slide_complete(pawn):
	var spot = get_spot_node(pawn.spot)
	if spot is SpotControl:
		var gd := GlobalGameData.inst
		var pinfo = gd.get_player_info_from_pawn_color(pawn.color)
		if pinfo.net_id == gd.my_net_id():
			# only perform spot life cycle calls on pawn's client
			spot.on_player_touched(pinfo)
		
func _on_pawn_ask_for_amalthia(pawn):
	_curr_moving_pawn = pawn
	emit_signal("ask_for_amalthia")
	
func _on_pawn_leave_orbit(pawn,curr_spot):
	print("Left orbit!")
	pawn.get_parent().remove_child(pawn)
	path_main.add_child(pawn)
	_snap_pawn_to_nearest_offset(pawn,curr_spot)
	
func _on_pawn_enter_orbit(pawn,curr_spot):
	match curr_spot:
		BaseSpot.Spots.Ganymede:
			pawn.get_parent().remove_child(pawn)
			path_jupiter.add_child(pawn)
			print("Entered Jupiter orbit!")
		BaseSpot.Spots.Ariel:
			pawn.get_parent().remove_child(pawn)
			path_uranus.add_child(pawn)
			print("Entered Uranus orbit!")
		BaseSpot.Spots.AdvFS:
			pawn.get_parent().remove_child(pawn)
			path_saturn.add_child(pawn)
			print("Entered Saturn orbit!")
		BaseSpot.Spots.Proteus:
			pawn.get_parent().remove_child(pawn)
			path_neptune.add_child(pawn)
			print("Entered Neptune orbit!")
			
	_snap_pawn_to_nearest_offset(pawn,curr_spot)
