extends PopupDialog

signal pressed_amalthia()
signal pressed_laser()

func _on_Amalthia_pressed():
	emit_signal("pressed_amalthia")
	hide()

func _on_Laser_pressed():
	emit_signal("pressed_laser")
	hide()
