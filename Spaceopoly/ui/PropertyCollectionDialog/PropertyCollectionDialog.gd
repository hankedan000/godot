extends Popup

onready var prop_list = $PropertyList
onready var prop_view = $PropertyView

func _ready():
	if get_parent() is Viewport:
		popup_centered_minsize()
	
###############################################################################
# PUBLIC METHODS
###############################################################################

# properties_to_show -> A list of property spots that will be visible
# disable_buy -> set to true if you want the buy button to be disabled
# disable_sell -> set to true if you want the sell button to be disabled
func configure(properties_to_show,disable_buy,disable_sell):
	# display all the property cards that are being shown
	prop_list.update_props_to_show(properties_to_show)
			
	# configure the buy/sell buttons
	prop_view.configure(disable_buy,disable_sell)

###############################################################################
# SIGNAL HANDLERS
###############################################################################

func _on_PropertyList_property_pressed(prop_spot):
	prop_view.prop_spot = prop_spot
	prop_view.popup_centered_minsize()
