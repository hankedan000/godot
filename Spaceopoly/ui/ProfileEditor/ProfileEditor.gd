extends PopupDialog

signal profile_save_request(profile)

onready var name_edit = $Margin/VBox/FormGrid/NameEdit
onready var save_button = $Margin/VBox/ButtonRow/SaveButton

func _ready():
	load_existing_profile()
	name_edit.grab_focus()
	
	# popup when it's the only thing in the tree
	if get_parent() is Viewport:
		popup_centered_minsize()

func load_existing_profile(profile={}):
	name_edit.text = "New Player"
	if profile.has("name"):
		name_edit.text = profile.name

func _on_NameEdit_text_changed(new_text):
	save_button.disabled = new_text.length() <= 0

func _on_SaveButton_pressed():
	var profile = {
		"name": name_edit.text
	}
	hide()
	emit_signal("profile_save_request",profile)

func _on_CancelButton_pressed():
	hide()
